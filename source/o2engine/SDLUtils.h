// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

namespace SDLUtils {

// pass false if in error. prints an error message. returns true if in error
bool CheckError(bool condition);

// prints an error message
void Error();

}