// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "canvasUtils.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "gameModeAsciiRPG.h"
#include "level.h"
#include "scriptEnumsAsciiRPG.h"
#include "sentenceBuilder.h"
#include "sharedConstants.h"
#include "textureAsciiConverter.h"
#include "textureManager.h"
#include "utils.h"

#define REGISTER_BASIC_LEVEL(x, y) mLevelManager->RegisterLevel(y, [&](GameMode& gameMode) { return std::make_unique<x>(gameMode); });

GameModeAsciiRPG::GameModeAsciiRPG(const TextureManager& textureMan)
	: GameMode()
	, mTextureMan(textureMan) {

}

GameModeAsciiRPG::~GameModeAsciiRPG() {

}

Blackboard& GameModeAsciiRPG::GetBlackboard() {
	return mBlackboard;
}

StorylineManager& GameModeAsciiRPG::GetStoryMan() {
	return *mStorylineMan;
}

void GameModeAsciiRPG::InitImpl() {
	EventBroadcaster::get().broadcast(LoadTextureEvent("textAtlas", "data/textures/codepage_437.bmp"));
	CanvasUtils::initAtlas();

	Utils::Image atlasImage;
	if(mTextureMan.GetTexture("textAtlas", atlasImage)) {
		mTextureAsciiConverter.FillAsciiPalette(atlasImage, NUM_CHARS_WIDTH_ATLAS, NUM_CHARS_HEIGHT_ATLAS, CHAR_WIDTH, CHAR_HEIGHT);
	}

	// test image
	//EventBroadcaster::get().broadcast(LoadTextureEvent("test", "data/textures/test/character.bmp"));

	mStorylineMan = std::make_unique<StorylineManager>(mLevelScriptFactory, mBlackboard);

	RegisterEnemies();
	RegisterScripts();
	RegisterBlackboard();
	RegisterStoryline();

	REGISTER_BASIC_LEVEL(LevelConsole, "title");
	REGISTER_BASIC_LEVEL(LevelIntro, "intro");
	//REGISTER_BASIC_LEVEL(LevelVillage, "village");
	REGISTER_BASIC_LEVEL(LevelCombat, "combat");
	REGISTER_BASIC_LEVEL(LevelStatus, "status");

	// initial level
	mLevelManager->LoadLevel("title");

	WordGroupRegistry::Init();

	mPlayer.mCustomName = "Player";
	mPlayer.mAttack = 8;
	mPlayer.mSpeed = 10;
	mPlayer.SetMaxHealth(60, true);

	EventBroadcaster::get().addEventListener<ConvertTextureEvent>(this, [&](const ConvertTextureEvent& _event) {
		Utils::Image image;
		if(mTextureMan.GetTexture(_event.mName, image)) {
			std::unique_ptr<Utils::AsciiImage> asciiImage = std::make_unique<Utils::AsciiImage>();
			if(mTextureAsciiConverter.Convert(image, _event.mWidth, _event.mHeight, *asciiImage)) {
				mConvertedAsciiImages.emplace(_event.mName, std::move(asciiImage));
			}
			else {
				O2LOG("Failed to convert image '%s' to ascii with width: %i and height: %i", _event.mName.c_str(), _event.mWidth, _event.mHeight);
			}
		}
	});

	//EventBroadcaster::get().broadcast(ConvertTextureEvent("test", SCREEN_CHARS_WIDTH - 2, SCREEN_CHARS_HEIGHT - 11));
}

const Utils::AsciiImage* GameModeAsciiRPG::GetAsciiImage(const std::string& name) const {
	auto it = mConvertedAsciiImages.find(name);
	if(it != mConvertedAsciiImages.end()) {
		return it->second.get();
	}
	return nullptr;
}

void GameModeAsciiRPG::UpdateImpl(float dt) {
	mStorylineMan->Tick(dt);
}

void GameModeAsciiRPG::DestroyImpl() {

}

#define REGISTER_BRAIN(x) { std::unique_ptr<Brain> brain = std::make_unique<x>(); mBrainFactory.RegisterBrain(std::move(brain)); }
#define REGISTER_DIALOG_BRAIN(x) { std::unique_ptr<DialogBrain> brain = std::make_unique<x>(); mDialogBrainFactory.RegisterBrain(std::move(brain)); }

// todo: data-drive enemy stats
void GameModeAsciiRPG::RegisterEnemies() {
	// register brains
	REGISTER_BRAIN(DummyBrain);
	REGISTER_BRAIN(RandomBrain);
	REGISTER_BRAIN(AttackWeakestBrain);

	// register dialog brains
	REGISTER_DIALOG_BRAIN(MinDialogBrain);

	// basic slime
	{
		std::unique_ptr<Actor> slime = std::make_unique<Actor>();
		slime->mActorType = ActorType::Slime;
		slime->mAttack = 4;
		slime->mSpeed = 10;
		slime->SetMaxHealth(14, true);
		slime->mBrain = mBrainFactory.GetBrain(BrainType::Random);
		mActorFactory.RegisterActor(std::move(slime));
	}

	// min
	{
		std::unique_ptr<Actor> _min = std::make_unique<Actor>();
		_min->mActorType = ActorType::Min;
		_min->mCustomName = "??????";
		_min->mAttack = 5;
		_min->mSpeed = 10;
		_min->SetMaxHealth(50, true);
		_min->SetHealth(42);
		_min->mBrain = mBrainFactory.GetBrain(BrainType::AttackWeakest);
		_min->mDialogBrain = mDialogBrainFactory.GetBrain(DialogBrainType::Min);
		mActorFactory.RegisterActor(std::move(_min));
	}
}

void GameModeAsciiRPG::RegisterScripts() {
	mLevelScriptFactory.RegisterScript(LevelScriptName::Act0_MemoryOfGraduation, "data/scripts/act0_memory_of_graduation.txt");
}

void GameModeAsciiRPG::RegisterBlackboard() {
	mBlackboard.RegisterBooleanKey(BlackboardKeys::Act0_Complete, false);
}

void GameModeAsciiRPG::RegisterStoryline() {
	mStorylineMan->AddScript(LevelScriptName::Act0_MemoryOfGraduation, [](const Blackboard& blackboard) {
		return !blackboard.GetBoolean(BlackboardKeys::Act0_Complete);
	});
}
