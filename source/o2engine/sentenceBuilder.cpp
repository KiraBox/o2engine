// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "sentenceBuilder.h"
#include "utils.h"

std::vector<std::vector<std::string>> WordGroupRegistry::mWordGroups;
bool WordGroupRegistry::mInitialized = false;

SentenceBuilder::SentenceBuilder(const std::string& str)
	: mSentence(str) {

}

SentenceBuilder::SentenceBuilder(const std::string& str, WordGroup g1)
	: mSentence(str) {
	Add(g1);
}

SentenceBuilder::SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2)
	: mSentence(str) {
	Add(g1);
	Add(g2);
}

SentenceBuilder::SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2, WordGroup g3)
	: mSentence(str) {
	Add(g1);
	Add(g2);
	Add(g3);
}

SentenceBuilder::SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2, WordGroup g3, WordGroup g4)
	: mSentence(str) {
	Add(g1);
	Add(g2);
	Add(g3);
	Add(g4);
}

//SentenceBuilder& SentenceBuilder::operator=(const SentenceBuilder& rhs) {
//	mSentence = rhs.mSentence;
//	mWordGroups.assign(rhs.mWordGroups.begin(), rhs.mWordGroups.end());
//}

SentenceBuilder& SentenceBuilder::Add(WordGroup group) {
	const std::vector<std::string>& words = WordGroupRegistry::Get(group);
	mWordGroups.push_back(&words);
	return *this;
}

std::string SentenceBuilder::Build() const {
	size_t idx = mSentence.find('(');
	if (idx == std::string::npos) {
		return mSentence;
	}

	std::string str = mSentence.substr(0, idx);
	size_t idx2 = mSentence.find(')', idx);

	do {
		O2ALOG(idx2 - idx < 3, "Malformed sentence builder.");
		if (isdigit(mSentence[idx + 1])) {
			int num = static_cast<int>(mSentence[idx + 1]) - '0';
			O2ALOG(num < static_cast<int>(mWordGroups.size()), "Index too high for a word group!");
			if (num < static_cast<int>(mWordGroups.size())) {
				size_t randIdx = Utils::GLOBAL_RANDOM.Range(0, mWordGroups[num]->size() - 1);
				str += (*mWordGroups[num])[randIdx];
			}
		}

		idx = mSentence.find('(', idx2);
		str += mSentence.substr(idx2 + 1, idx - idx2 - 1);
		idx2 = mSentence.find(')', idx);

	} while (idx != std::string::npos);

	return str;
}

void SentencePicker::Add(const SentenceBuilder& sentenceBuilder) {
	mSentenceBuilders.push_back(sentenceBuilder);
}

std::string SentencePicker::Pick() const {
	if(mSentenceBuilders.empty()) {
		return "";
	}

	size_t randIdx = Utils::GLOBAL_RANDOM.Range(0, mSentenceBuilders.size() - 1);
	return mSentenceBuilders[randIdx].Build();
}

void WordGroupRegistry::AddWord(WordGroup group, const std::string& word) {
	if (!mInitialized) {
		mWordGroups.resize(WordGroup::NumTypes);
		mInitialized = true;
	}

	if (group < WordGroup::NumTypes) {
		mWordGroups[group].push_back(word);
	}
}

const std::vector<std::string>& WordGroupRegistry::Get(WordGroup group) {
	if (!mInitialized) {
		mWordGroups.resize(WordGroup::NumTypes);
		mInitialized = true;
	}

	O2ASSERT(group < WordGroup::NumTypes)
	return mWordGroups[group];
}

void WordGroupRegistry::Init() {
	AddWord(WordGroup::Vegetables, "artichoke");
	AddWord(WordGroup::Vegetables, "lentil");
	AddWord(WordGroup::Vegetables, "carrot");
	AddWord(WordGroup::Vegetables, "cauliflower");
	AddWord(WordGroup::Vegetables, "leek");
	AddWord(WordGroup::Vegetables, "eggplant");
	AddWord(WordGroup::Vegetables, "mushroom");
	AddWord(WordGroup::Vegetables, "turnip");

	AddWord(WordGroup::Beverages, "mead");
	AddWord(WordGroup::Beverages, "beer");
	AddWord(WordGroup::Beverages, "ale");
	AddWord(WordGroup::Beverages, "wine");
	AddWord(WordGroup::Beverages, "water");
	AddWord(WordGroup::Beverages, "tea");
	AddWord(WordGroup::Beverages, "kombucha");
}
