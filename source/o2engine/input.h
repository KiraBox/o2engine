// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class Input {
public:
	void Update();

private:
	void UpdateImpl();
};
