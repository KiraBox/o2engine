// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "canvasUtils.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "inputMapping.h"
#include "levelUtils.h"
#include <memory>
#include <set>
#include "sharedConstants.h"
#include "textAtlas.h"
#include "utils.h"

namespace {
	std::unique_ptr<TextAtlas> mIBMPCAtlas;
	std::unique_ptr<IBMPCRenderer> mTextRenderer;
}

namespace CanvasUtils {

void initAtlas() {
	mIBMPCAtlas = std::make_unique<TextAtlas>("textAtlas", CHAR_WIDTH * NUM_CHARS_WIDTH_ATLAS, CHAR_HEIGHT * NUM_CHARS_HEIGHT_ATLAS, CHAR_WIDTH, CHAR_HEIGHT);
	mTextRenderer = std::make_unique<IBMPCRenderer>(*mIBMPCAtlas);
}

void Text::Render() {
	std::vector<bool> isLineComplete;
	isLineComplete.resize(mText.size(), false);
	size_t incompleteCount = 0;
	bool searchComplete = false;

	size_t renderedCount = 0;

	for(size_t i = 0; i < mText.size(); ++i) {
		for(size_t j = 0; j < mText[i].size(); ++j) {
			++renderedCount;
			++incompleteCount;
			if(renderedCount >= mDelayIdx) {
				searchComplete = true;
				break;
			}
		}
		if(searchComplete) {
			break;
		}
		isLineComplete[i] = true;
		incompleteCount = 0;
	}

	renderedCount = 0;

	for(size_t i = 0; i < mText.size(); ++i) {
		for(size_t j = 0; j < mText[i].size(); ++j) {
			float posX = (mPosX * CHAR_WIDTH) + j * CHAR_WIDTH;
			float posY = (mPosY * CHAR_HEIGHT) - i * CHAR_HEIGHT;

			if(mCenterX) {
				if(isLineComplete[i]) {
					posX -= CHAR_WIDTH * (mText[i].size() / 2.0f);
				}
				else {
					posX -= CHAR_WIDTH * (incompleteCount / 2.0f);
				}
			}

			const TextEffectInfo& info = mText[i][j];
			mTextRenderer->SetColor(info.mColor[0], info.mColor[1], info.mColor[2]);
			mTextRenderer->DrawASCII(info.mCharacter, posX, posY);
			mTextRenderer->SetDefaultColor();

			++renderedCount;
			if(renderedCount >= mDelayIdx) {
				return;
			}
		}
	}
}

void Text::Update(float dt) {
	// typewriter text
	if(mDelayIdx >= mNumCharsInText) {
		mDelayCounter = 0.f;
		return;
	}

	mDelayCounter += dt;
	while(mDelayIdx < mNumCharsInText) {
		const float& delaySeconds = mText[mDelayLineIdx][mDelaySubLineIdx].mDelaySeconds;
		if(delaySeconds > 0.f && mDelayCounter >= delaySeconds) {
			mDelayCounter -= delaySeconds;
		}
		else if(delaySeconds == 0.f && mDelayCounter >= mDefaultDelaySeconds) {
			mDelayCounter -= mDefaultDelaySeconds;
		}
		else {
			break;
		}

		++mDelayIdx;

		++mDelaySubLineIdx;
		if(mDelaySubLineIdx >= mText[mDelayLineIdx].size()) {
			mDelaySubLineIdx = 0;
			++mDelayLineIdx;
		}
	}
}

void Text::UpdateAndRender(float dt) {
	Update(dt);
	Render();
}

const std::string& Text::GetText() const {
	return mOriginalText;
}

void Text::SetText(const std::string& text) {
	mOriginalText = text;

	mDelayIdx = 0;
	mDelayLineIdx = 0;
	mDelaySubLineIdx = 0;
	mDelayCounter = 0.f;

	mText.clear();
	mText.push_back({});

	size_t idxBegin = mOriginalText.find('{');
	bool containsSpecialCharacters = mFormattingEnabled && (idxBegin != std::string::npos);

	if (containsSpecialCharacters) {
		size_t idxWalk = 0;
		size_t idxSeparator = 0;
		size_t idxEnd = 0;

		while (idxWalk < mOriginalText.size()) {
			// characters with no special properties
			for (size_t i = idxWalk; i < idxBegin; ++i) {
				mText[0].push_back({ static_cast<unsigned char>(mOriginalText[i]) });
			}

			idxSeparator = mOriginalText.find('|', idxBegin);
			O2ASSERT(idxSeparator != std::string::npos);
			idxEnd = mOriginalText.find('}', idxSeparator);
			O2ASSERT(idxEnd != std::string::npos);

			std::vector<std::string> properties;
			{
				std::string subStr = mOriginalText.substr(idxBegin + 1, idxSeparator - idxBegin - 1);
				size_t propIdx[2] = { 0, subStr.find(';') };
				properties.push_back(subStr.substr(propIdx[0], propIdx[1] - propIdx[0]));

				while (propIdx[1] != std::string::npos) {
					propIdx[0] = propIdx[1] + 1;
					propIdx[1] = subStr.find(';', propIdx[0]);
					properties.push_back(subStr.substr(propIdx[0], propIdx[1] - propIdx[0]));
				}
			}

			float color[3] = { 255.f, 255.f, 255.f };
			float delaySeconds = 0.f;

			for (const std::string& property : properties) {
				if (property.empty()) {
					continue;
				}
				// color
				if (property[0] == 'c') {
					size_t colorIdx = property.find(',');
					color[0] = static_cast<float>(atof(property.substr(1, colorIdx).c_str()));

					size_t colorIdxPrev = colorIdx;
					colorIdx = property.find(',', colorIdx + 1);

					color[1] = static_cast<float>(atof(property.substr(colorIdxPrev + 1, colorIdx - colorIdxPrev - 1).c_str()));
					color[2] = static_cast<float>(atof(property.substr(colorIdx + 1).c_str()));
				}
				// delay speed
				else if (property[0] == 'd') {
					delaySeconds = static_cast<float>(atof(property.substr(1).c_str()));
				}
			}

			for (size_t j = idxSeparator + 1; j < idxEnd; ++j) {
				TextEffectInfo info;
				info.mCharacter = static_cast<unsigned char>(mOriginalText[j]);
				info.mColor[0] = color[0];
				info.mColor[1] = color[1];
				info.mColor[2] = color[2];
				info.mDelaySeconds = delaySeconds;

				mText[0].push_back(info);
			}

			idxWalk = idxEnd + 1;
			idxBegin = mOriginalText.find("{", idxWalk);

			if (idxBegin == std::string::npos) {
				break;
			}
		}

		// any remaining characters
		for (size_t i = idxWalk; i < mOriginalText.size(); ++i) {
			mText[0].push_back({ static_cast<unsigned char>(mOriginalText[i]) });
		}
	}
	else {
		for (size_t i = 0; i < mOriginalText.size(); ++i) {
			mText[0].push_back({ static_cast<unsigned char>(mOriginalText[i]) });
		}
	}

	mNumCharsInText = 0;
	for(size_t i = 0; i < mText.size(); ++i) {
		for(size_t j = 0; j < mText[i].size(); ++j) {
			++mNumCharsInText;
		}
	}

	// split the text into multiple lines
	if(mShouldCarry) {
		for(size_t i = 0; i < mText.size(); ++i) {
			if(mMaxLength > -1 && static_cast<int>(mText[i].size()) > mMaxLength) {
				size_t idxWalk = mMaxLength;
				while(!isspace(mText[i][idxWalk].mCharacter)) {
					--idxWalk;
				}
				++idxWalk;

				mText.push_back({});
				for(size_t j = idxWalk; j < mText[i].size(); ++j) {
					mText[i+1].push_back(mText[i][j]);
				}
				mText[i].resize(idxWalk);
			}
		}
	}
	else {
		mText.resize(mMaxLength);
	}
}

void Text::Complete() {
	Update(999999.f);
}

bool Text::IsDone() const {
	return (mDelayIdx >= mNumCharsInText);
}

void Text::SetDefaultDelay(float delaySeconds) {
	mDefaultDelaySeconds = delaySeconds;
}

void Text::EnableFormatting() {
	mFormattingEnabled = true;
	SetText(mOriginalText);
}

void Text::DisableFormatting() {
	mFormattingEnabled = false;
	SetText(mOriginalText);
}

void Text::SetMaxLength(int maxLength, bool carry) {
	mMaxLength = maxLength;
	mShouldCarry = carry;
}

int Text::GetMaxLength() const {
	return mMaxLength;
}

void Text::CenterX(bool state) {
	mCenterX = state;
}

void Text::CenterY(bool state) {
	mCenterY = state;
}

CanvasRect::CanvasRect(float posX, float posY, unsigned int charsWidth, unsigned int charsHeight)
	: mPosX(posX)
	, mPosY(posY)
	, mWidth(charsWidth)
	, mHeight(charsHeight) {

	size_t code = 0;

	mCharInfo.resize(mWidth);
	for (size_t i = 0; i < mCharInfo.size(); ++i) {
		mCharInfo[i].resize(mHeight);
	}
}

void CanvasRect::Render() {
	for (size_t x = 0; x < mWidth; ++x) {
		for (size_t y = 0; y < mHeight; ++y) {
			size_t code = mCharInfo[x][y].mCharacter;
			if (code != 0) {
				TextEffectInfo& info = mCharInfo[x][y];
				mTextRenderer->SetColor(info.mColor[0], info.mColor[1], info.mColor[2]);
				mTextRenderer->Draw(info.mCharacter, mPosX + (x * CHAR_WIDTH), mPosY + (y * CHAR_HEIGHT));
			}
		}
	}
}

void CanvasRect::SetChar(int idxX, int idxY, unsigned char c) {
	O2ALOG(idxX >= 0 && (unsigned int)idxX < mWidth && idxY >= 0 && (unsigned int)idxY < mHeight, "Index out of range.");
	mCharInfo[idxX][idxY].mCharacter = c;
}

void CanvasRect::SetStringLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, const std::string& str) {
	size_t idx = 0;
	_SetCharLine(startIdxX, startIdxY, endIdxX, endIdxY, [&]() { return idx < str.size() ? str[idx++] : ' '; });
}

void CanvasRect::SetCharLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, unsigned char c) {
	_SetCharLine(startIdxX, startIdxY, endIdxX, endIdxY, [&](){ return c; });
}

void CanvasRect::SetImageCentered(const Utils::AsciiImage& image) {
	if(image.mWidth > mWidth || image.mHeight > mHeight) {
		O2ALOG(false, "Image cannot be set, the dimensions are too large for the canvas.");
		return;
	}

	int offsetX = (mWidth - image.mWidth) / 2;
	int offsetY = (mHeight - image.mHeight) / 2;

	for(unsigned int x = 0; x < image.mWidth; ++x) {
		for(unsigned int y = 0; y < image.mHeight; ++y) {
			TextEffectInfo& info = mCharInfo[x + offsetX][mHeight - y - offsetY - 1];
			const Utils::AsciiImage::CharInfo& asciiInfo = image.mCharInfo[x + image.mWidth * y];
			info.mCharacter = asciiInfo.mCode;
			info.mColor[0] = asciiInfo.mColor.mR;
			info.mColor[1] = asciiInfo.mColor.mG;
			info.mColor[2] = asciiInfo.mColor.mB;
		}
	}
}

void CanvasRect::_SetCharLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, std::function<unsigned char()> getCharFunc) {
	// single character
	if (startIdxX == endIdxX && startIdxY == endIdxY) {
		SetChar(startIdxX, startIdxY, getCharFunc());
		return;
	}

	// horizontal line
	if (startIdxX == endIdxX) {
		int dirY = (endIdxY - startIdxY) > 0 ? 1 : -1;
		for (int y = startIdxY; y != endIdxY;) {
			SetChar(startIdxX, y, getCharFunc());
			y += dirY;
		}
		SetChar(startIdxX, endIdxY, getCharFunc());
		return;
	}

	// vertical line
	if (startIdxY == endIdxY) {
		int dirX = (endIdxX - startIdxX) > 0 ? 1 : -1;
		for (int x = startIdxX; x != endIdxX;) {
			SetChar(x, startIdxY, getCharFunc());
			x += dirX;
		}
		SetChar(endIdxX, startIdxY, getCharFunc());
		return;
	}

	// Bresenham's line algorithm
	if (abs(endIdxY - startIdxY) < abs(endIdxX - startIdxX)) {
		if (endIdxX < startIdxX) {
			std::swap(startIdxX, endIdxX);
			std::swap(startIdxY, endIdxY);
		}

		int deltaX = endIdxX - startIdxX;
		int deltaY = endIdxY - startIdxY;
		float deltaError = std::fabs(static_cast<float>(deltaY) / static_cast<float>(deltaX));
		float error = 0.f;

		int x = startIdxX;
		int y = startIdxY;

		while (x <= endIdxX) {
			SetChar(x, y, getCharFunc());
			error += deltaError;
			if (error >= 0.5f) {
				error -= 1.0f;
				y += (deltaY > 0.f) ? 1 : -1;
			}
			x += 1;
		};
	}
	else {
		if (endIdxY < startIdxY) {
			std::swap(startIdxX, endIdxX);
			std::swap(startIdxY, endIdxY);
		}

		int deltaX = endIdxX - startIdxX;
		int deltaY = endIdxY - startIdxY;
		float deltaError = std::fabs(static_cast<float>(deltaX) / static_cast<float>(deltaY));
		float error = 0.f;

		int x = startIdxX;
		int y = startIdxY;

		while (y <= endIdxY) {
			SetChar(x, y, getCharFunc());
			error += deltaError;
			if (error >= 0.5f) {
				error -= 1.0f;
				x += (deltaX > 0.f) ? 1 : -1;
			}
			y += 1;
		};
	}
}

PlayerTextInput::PlayerTextInput(Text& text, unsigned int width)
	: mWidth(width)
	, mText(text)
	, mCursor('_')
	, mDelaySeconds(0.f)
	, mDelayAccum(0.f)
	, mCursorToggle(false)
	, mIsBackspaceHeld(false) {

	mDelaySeconds = 0.5f;
	mBackspaceDelaySeconds[0] = 0.5f;
	mBackspaceDelaySeconds[1] = 0.02f;
	mIsShiftHeld[0] = false;
	mIsShiftHeld[1] = false;

	mText.DisableFormatting();
}

void PlayerTextInput::Update(float dt) {
	mDelayAccum += dt;
	if (mDelayAccum >= mDelaySeconds) {
		mDelayAccum -= mDelaySeconds;

		const std::string& text = mText.GetText();

		if (mCursorToggle) {
			mText.SetText(text.substr(0, text.size() - 1));
		}
		else {
			std::string textCopy(text);
			textCopy.push_back(mCursor);
			mText.SetText(textCopy);
		}

		mCursorToggle = !mCursorToggle;
	}

	if (mIsBackspaceHeld) {
		mBackspaceDelayAccum[0] += dt;
		if (mBackspaceDelayAccum[0] >= mBackspaceDelaySeconds[0]) {
			mBackspaceDelayAccum[1] += dt;
			if (mBackspaceDelayAccum[1] > mBackspaceDelaySeconds[1]) {
				mBackspaceDelayAccum[1] -= mBackspaceDelaySeconds[1];
				Remove();
			}
		}
	}
	else {
		mBackspaceDelayAccum[0] = 0.f;
		mBackspaceDelayAccum[1] = 0.f;
	}
}

void PlayerTextInput::Append(unsigned char c) {
	std::string text = mText.GetText();
	if (text.size() >= mWidth - 1) {
		return;
	}

	if (mCursorToggle) {
		text.pop_back();
		text.push_back(c);
		text.push_back(mCursor);
	}
	else {
		text.push_back(c);
	}

	mText.SetText(text);
}

void PlayerTextInput::Remove() {
	std::string text = mText.GetText();

	if (mCursorToggle) {
		text.pop_back();
	}

	if (!text.empty()) {
		text.pop_back();
	}

	if (mCursorToggle) {
		text.push_back(mCursor);
	}

	mText.SetText(text);
}

void PlayerTextInput::Clear() {
	mText.SetText("");
	mCursorToggle = false;
}

void PlayerTextInput::OnKeyPressed(EKeyboard key) {
	// todo: when commands are added to this function, make sure they are case-insensitive please. thanks
	if (key == EKeyboard::RETURN || key == EKeyboard::RETURN2) {
		if (mOnReturnCallback != nullptr) {
			std::string text = mText.GetText();
			if (mCursorToggle) {
				text.pop_back();
			}
			mOnReturnCallback(text);
		}
		Clear();
		return;
	}

	if (!mModificationEnabled) {
		return;
	}
	
	if (key == EKeyboard::LSHIFT) {
		mIsShiftHeld[0] = true;
		return;
	}

	if (key == EKeyboard::RSHIFT) {
		mIsShiftHeld[1] = true;
		return;
	}

	if (key >= EKeyboard::A && key <= EKeyboard::Z) {
		unsigned char difference = static_cast<unsigned char>(key) - static_cast<unsigned char>(EKeyboard::A);
		if (IsShiftHeld()) {
			Append('A' + difference);
		}
		else {
			Append('a' + difference);
		}
	}
	else if (key >= EKeyboard::ZERO && key <= EKeyboard::NINE) {
		unsigned char difference = static_cast<unsigned char>(key) - static_cast<unsigned char>(EKeyboard::ZERO);
		if (IsShiftHeld()) {
			switch (key) {
				case EKeyboard::ONE:
					Append('!');
					break;
				case EKeyboard::TWO:
					Append('@');
					break;
				case EKeyboard::THREE:
					Append('#');
					break;
				case EKeyboard::FOUR:
					Append('$');
					break;
				case EKeyboard::FIVE:
					Append('%');
					break;
				case EKeyboard::SIX:
					Append('^');
					break;
				case EKeyboard::SEVEN:
					Append('&');
					break;
				case EKeyboard::EIGHT:
					Append('*');
					break;
				case EKeyboard::NINE:
					Append('(');
					break;
				case EKeyboard::ZERO:
					Append(')');
					break;
			};
		}
		else {
			Append('0' + difference);
		}
	}
	else if (key == EKeyboard::MINUS) {
		Append(IsShiftHeld() ? '_' : '-');
	}
	else if (key == EKeyboard::EQUALS) {
		Append(IsShiftHeld() ? '+' : '=');
	}
	else if (key == EKeyboard::LEFTBRACKET) {
		Append(IsShiftHeld() ? '{' : '[');
	}
	else if (key == EKeyboard::RIGHTBRACKET) {
		Append(IsShiftHeld() ? '}' : ']');
	}
	else if (key == EKeyboard::BACKQUOTE) {
		Append(IsShiftHeld() ? '~' : '`');
	}
	else if (key == EKeyboard::BACKSLASH) {
		Append(IsShiftHeld() ? '|' : '\\');
	}
	else if (key == EKeyboard::SEMICOLON) {
		Append(IsShiftHeld() ? ':' : ';');
	}
	else if (key == EKeyboard::QUOTE) {
		Append(IsShiftHeld() ? '"' : '\'');
	}
	else if (key == EKeyboard::COMMA) {
		Append(IsShiftHeld() ? '<' : ',');
	}
	else if (key == EKeyboard::PERIOD) {
		Append(IsShiftHeld() ? '>' : '.');
	}
	else if (key == EKeyboard::SLASH) {
		Append(IsShiftHeld() ? '?' : '/');
	}
	else if (key == EKeyboard::SPACE) {
		Append(' ');
	}
	else if (key == EKeyboard::BACKSPACE) {
		Remove();
		mIsBackspaceHeld = true;
	}
	else if (key == EKeyboard::ESCAPE) {
		Clear();
	}
}

void PlayerTextInput::OnKeyReleased(EKeyboard key) {
	if (!mModificationEnabled) {
		return;
	}

	if (key == EKeyboard::LSHIFT) {
		mIsShiftHeld[0] = false;
	}
	else if (key == EKeyboard::RSHIFT) {
		mIsShiftHeld[1] = false;
	}
	else if (key == EKeyboard::BACKSPACE) {
		mIsBackspaceHeld = false;
	}
}

bool PlayerTextInput::IsShiftHeld() const {
	return mIsShiftHeld[0] || mIsShiftHeld[1];
}

void PlayerTextInput::RegisterOnReturn(std::function<void(std::string)> callback) {
	mOnReturnCallback = callback;
}

void PlayerTextInput::SetText(const std::string& str) {
	mText.SetText(str);
	mCursorToggle = false;
}

bool PlayerTextInput::IsModificationEnabled() const {
	return mModificationEnabled;
}

void PlayerTextInput::EnableModification(bool state) {
	if (mModificationEnabled != state) {
		mModificationEnabled = state;
		mIsShiftHeld[0] = false;
		mIsShiftHeld[1] = false;
		mIsBackspaceHeld = false;
	}
}

}
