// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "actor.h"
#include "brain.h"
#include "utils.h"

BrainType Brain::GetBrainType() const {
	return mBrainType;
}

void BrainFactory::RegisterBrain(std::unique_ptr<Brain> brain) {
	if (mRegisteredBrains.empty()) {
		mRegisteredBrains.resize(static_cast<size_t>(BrainType::NumTypes));
	}
	if (brain) {
		mRegisteredBrains[static_cast<size_t>(brain->GetBrainType())] = std::move(brain);
	}
}

Brain* BrainFactory::GetBrain(BrainType brainType) const {
	if (!mRegisteredBrains.empty()) {
		if (const auto& brain = mRegisteredBrains[static_cast<size_t>(brainType)]) {
			return brain.get();
		}
	}
	return nullptr;
}

ActorActionInfo Brain::ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies) {
	ActorActionInfo action;
	action.mActor = actor;
	action.mActionType = ActorActionType::None;

	return action;
}

ActorActionInfo RandomBrain::ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies) {
	ActorActionInfo action;
	action.mActor = actor;
	action.mTargetActor = enemies[Utils::GLOBAL_RANDOM.GetInt() % enemies.size()];

	action.mActionType = ActorActionType::Attack;
	action.mData.mAmount = actor->GetBasicAttackAmount(Utils::GLOBAL_RANDOM);

	return action;
}

ActorActionInfo AttackWeakestBrain::ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies) {
	ActorActionInfo action;
	action.mActor = actor;

	std::vector<size_t> identicalHealths;
	identicalHealths.push_back(0);

	for(size_t i = 1; i < enemies.size(); ++i) {
		if(enemies[i]->GetHealth() == enemies[identicalHealths[0]]->GetHealth()) {
			identicalHealths.push_back(i);
		}
		else if(enemies[i]->GetHealth() < enemies[identicalHealths[0]]->GetHealth()) {
			identicalHealths.clear();
			identicalHealths.push_back(i);
		}
	}

	if(identicalHealths.size() > 1) {
		action.mTargetActor = enemies[identicalHealths[Utils::GLOBAL_RANDOM.GetInt() % identicalHealths.size()]];
	}
	else {
		action.mTargetActor = enemies[identicalHealths[0]];
	}

	action.mActionType = ActorActionType::Attack;
	action.mData.mAmount = actor->GetBasicAttackAmount(Utils::GLOBAL_RANDOM);

	return action;
}
