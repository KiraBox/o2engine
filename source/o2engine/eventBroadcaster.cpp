// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "eventBroadcaster.h"

std::unique_ptr<EventBroadcaster> EventBroadcaster::mInstance = nullptr;
