// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "actor.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "levelUtils.h"
#include "sharedConstants.h"
#include "utils.h"

namespace {
	// level borders
	static CanvasUtils::CanvasRect mLevelBounds = CanvasUtils::CanvasRect(0.0f, 0.0f, SCREEN_CHARS_WIDTH, SCREEN_CHARS_HEIGHT);
	static CanvasUtils::CanvasRect mImageBounds = CanvasUtils::CanvasRect(CHAR_WIDTH, CHAR_HEIGHT * 10, SCREEN_CHARS_WIDTH - 2, SCREEN_CHARS_HEIGHT - 11);

	// player input
	static CanvasUtils::Text mPreConsoleText;
	static CanvasUtils::Text mConsoleText;
	static std::shared_ptr<CanvasUtils::PlayerTextInput> mPlayerInput;

	// player status
	static int mPrevHealth = 0;
	static CanvasUtils::Text mHealthText;

	// dialogue queue
	static LevelUtils::TextQueue mDialogueQueue;
}

namespace LevelUtils {

void InitLevelBorders() {
	const unsigned edgeChar = '=';

	mLevelBounds.SetCharLine(0, SCREEN_CHARS_HEIGHT - 2, 0, 1, edgeChar);
	mLevelBounds.SetCharLine(SCREEN_CHARS_WIDTH - 1, SCREEN_CHARS_HEIGHT - 2, SCREEN_CHARS_WIDTH - 1, 1, edgeChar);
	mLevelBounds.SetCharLine(0, SCREEN_CHARS_HEIGHT - 1, SCREEN_CHARS_WIDTH - 1, SCREEN_CHARS_HEIGHT - 1, edgeChar);
	mLevelBounds.SetCharLine(0, 0, SCREEN_CHARS_WIDTH - 1, 0, edgeChar);

	mLevelBounds.SetCharLine(0, 2, SCREEN_CHARS_WIDTH - 1, 2, edgeChar);
}

void UpdateLevelBorders() {
	mLevelBounds.Render();
}

void UpdateImage() {
	mImageBounds.Render();
}

void InitPlayerInput(void* _this, std::function<void(std::string)> onReturnCallback) {
	mPreConsoleText.mPosX = 2;
	mPreConsoleText.mPosY = 1;
	mPreConsoleText.SetText("O2:\\>");

	mConsoleText.mPosX = 7;
	mConsoleText.mPosY = 1;

	mPlayerInput = std::make_shared<CanvasUtils::PlayerTextInput>(mConsoleText, 71);
	EventBroadcaster::get().addEventListener<KeyPressedEvent>(_this, [&](const KeyPressedEvent& _event) {
		mPlayerInput->OnKeyPressed(_event.mKey);
	});
	EventBroadcaster::get().addEventListener<KeyReleasedEvent>(_this, [&](const KeyReleasedEvent& _event) {
		mPlayerInput->OnKeyReleased(_event.mKey);
	});
	mPlayerInput->RegisterOnReturn(onReturnCallback);
}

void UpdatePlayerInput(float dt) {
	mPreConsoleText.UpdateAndRender(dt);
	mConsoleText.UpdateAndRender(dt);

	mPlayerInput->Update(dt);
}

void DestroyPlayerInput(void* _this) {
	EventBroadcaster::get().removeEventListener<KeyPressedEvent>(_this);
	EventBroadcaster::get().removeEventListener<KeyReleasedEvent>(_this);
	mPlayerInput = nullptr;
}

CanvasUtils::PlayerTextInput& GetPlayerInputText() {
	return *mPlayerInput;
}

void LevelUtils::DisplayImage(const Utils::AsciiImage& image) {
	mImageBounds.SetImageCentered(image);
}

void LevelUtils::TextQueue::PushText(const std::string& text) {
	if(mText.empty()) {
		mText.push(text);
		//Advance();
	}
	else {
		mText.push(text);
	}
}

bool LevelUtils::TextQueue::PopText(std::string& outStr) {
	if(mText.empty()) {
		return false;
	}
	else {
		outStr = mText.front();
		mText.pop();
		return true;
	}
}

void LevelUtils::TextQueue::Clear() {
	while(!mText.empty()) {
		mText.pop();
	}
}

void LevelUtils::TextQueue::Reset() {
	mIsDone = false;
	mAdvance = false;
	Clear();
}

void LevelUtils::TextQueue::Advance() {
	mAdvance = true;
}

void LevelUtils::TextQueue::UpdateAndRender(CanvasUtils::Text& text, float dt, std::function<void()> postUpdate) {
	if(mAdvance) {
		if(text.IsDone()) {
			std::string str;
			PopText(str);
			text.SetText(str);
		}
		else {
			text.Complete();
		}

		mAdvance = false;
	}

	text.Update(dt);

	if(postUpdate) {
		postUpdate();
	}

	text.Render();

	mIsDone = mText.empty() && text.IsDone();
}

bool LevelUtils::TextQueue::IsDone() const {
	return mIsDone;
}

}
