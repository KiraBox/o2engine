// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <functional>
#include <memory>
#include <unordered_map>

class EventListener;

class EventBroadcaster {
private:
	static std::unique_ptr<EventBroadcaster> mInstance;

public:
	static EventBroadcaster& get() {
		if (mInstance == nullptr) {
			mInstance = std::make_unique<EventBroadcaster>();
		}
		return *mInstance.get();
	}

	static void destroy() {
		if (mInstance) {
			mInstance.reset();
		}
	}

	template <typename T>
	void addEventListener(void* owner, std::function<void(const T&)> onEventReceived) {
		if (onEventReceived != nullptr) {
			ListenerData<T>::insert(owner, onEventReceived);
		}
	}

	template <typename T>
	void removeEventListener(void* owner) {
		ListenerData<T>::remove(owner);
	}

	template <typename T>
	void broadcast(const T& _event) {
		ListenerData<T>::broadcastEvent(_event);
	}

private:
	template <typename T>
	class ListenerData {
	private:
		static std::unordered_map<void*, std::function<void(const T&)>> mListenerCallbacks;

	public:
		static void insert(void* owner, std::function<void(const T&)> callback) {
			if (owner != nullptr && callback != nullptr) {
				mListenerCallbacks[owner] = callback;
			}
		}

		static void remove(void* owner) {
			if (owner != nullptr) {
				auto elem = mListenerCallbacks.find(owner);
				if (elem != mListenerCallbacks.end()) {
					mListenerCallbacks.erase(elem);
				}
			}
		}

		static void broadcastEvent(const T& _event) {
			for (auto& listener : mListenerCallbacks) {
				listener.second(_event);
			}
		}
	};
};

template <typename T>
std::unordered_map<void*, std::function<void(const T&)>> EventBroadcaster::ListenerData<T>::mListenerCallbacks;
