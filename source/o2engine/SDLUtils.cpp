// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "SDL.h"
#include "SDLUtils.h"
#include <stdio.h>

namespace SDLUtils {

bool CheckError(bool condition) {
	if (condition == false) {
		O2LOG("SDL Error: %s\n", SDL_GetError());
		return true;
	}
	return false;
}

void Error() {
	CheckError(false);
}

}
