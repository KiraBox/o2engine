// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include <cstdarg>
#include <cstdlib>
#include "utils.h"

#include <time.h>

namespace Utils {

Random GLOBAL_RANDOM(static_cast<unsigned int>(time(NULL)));

std::string Format(const char* format, ...) {
	va_list args;
	va_start(args, format);

	static char buffer[1000];
	vsprintf_s(buffer, format, args);

	va_end(args);
	return buffer;
}

Random::Random(unsigned int seed, unsigned int cacheSize /* = 2048 */) 
	: mCacheSize(cacheSize)
	, mSeed(seed) {

	srand(mSeed);
	mCache.resize(mCacheSize);
	for (unsigned int i = 0; i < mCacheSize; ++i) {
		mCache[i] = rand();
	}
}

unsigned int Random::GetInt() {
	unsigned int num = mCache[mIdx];
	mIdx = (mIdx + 1) % mCacheSize;
	return num;
}

float Random::GetFloat() {
	unsigned int num = GetInt();
	return static_cast<float>(num) / static_cast<float>(RAND_MAX);
}

int Random::Range(int _min, int _max) {
	if (_min == _max) {
		return _min;
	}
	else {
		return _min + GetInt() % (_max - _min + 1);
	}
}

float Random::Range(float _min, float _max) {
	return _min + GetFloat() * (_max - _min);
}

unsigned int Random::GetSeed() const {
	return mSeed;
}

Timer::Timer(float duration)
	: mDuration(duration) {

}

void Timer::Reset() {
	mCounter = 0.f;
}

bool Timer::Done(float dt) {
	mCounter += dt;
	if (mCounter > mDuration) {
		return true;
	}
	return false;
}

float Timer::GetProgress() const {
	if (mDuration > 0.f) {
		return mCounter / mDuration;
	}
	return 0.f;
}

std::string ToLower(const std::string& str) {
	std::string newStr;
	newStr.resize(str.size());
	for (size_t i = 0; i < newStr.size(); ++i) {
		newStr[i] = tolower(str[i]);
	}
	return newStr;
}

std::string ToUpper(const std::string& str) {
	std::string newStr;
	newStr.resize(str.size());
	for (size_t i = 0; i < newStr.size(); ++i) {
		newStr[i] = toupper(str[i]);
	}
	return newStr;
}

bool ToNumber(const std::string& str, int& outNum) {
	if (str.empty()) {
		return false;
	}

	outNum = 0;

	int tens = 1;
	for (int i = str.size() - 1; i >= 0; --i) {
		if (!isdigit(str[i])) {
			return false;
		}
		outNum += tens * (str[i] - '0');
		++tens;
	}

	return true;
}

std::string Capitalize(const std::string& str) {
	std::string newStr = str;
	if (!newStr.empty()) {
		newStr[0] = toupper(newStr[0]);
	}
	return newStr;
}

bool IsNotBlack(const Color& color) {
	return color.mR > 0.f || color.mG > 0.f || color.mB > 0.f;
}

}
