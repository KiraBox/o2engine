// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "canvas.h"
#include <functional>
#include <map>
#include <memory>

class UserInterface {
public:
	void Init();
	void Update(float dt);
	void Destroy();

	void RegisterCanvas(const std::string& name, std::function<std::unique_ptr<Canvas>()> creator);

	void LoadCanvas(const std::string& name);
	void UnloadCanvas(const std::string& name);

	size_t NumActiveCanvas() const;

private:
	std::map<std::string, std::function<std::unique_ptr<Canvas>()>> mCanvasRegistry;
	std::map<std::string, std::unique_ptr<Canvas>> mActiveCanvas;
};
