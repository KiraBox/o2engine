// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "tokenizer.h"
#include <set>

void Tokenizer::Tokenize(const std::string& str, const std::string& delimeters) {
	mTokens.clear();

	if(str.empty()) {
		return;
	}

	std::set<char> delims;
	for(const char& c : delimeters) {
		delims.insert(c);
	}

	std::string token;
	for(size_t i = 0; i < str.size(); ++i) {
		const char& c = str[i];
		if(delims.find(c) != delims.end()) {
			if(!token.empty()) {
				mTokens.push_back(token);
				token.clear();
			}
		}
		else {
			token.push_back(c);
		}
	}

	if(!token.empty()) {
		mTokens.push_back(token);
	}
}

const std::vector<std::string>& Tokenizer::GetTokens() const {
	return mTokens;
}
