// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actor.h"
#include "blackboard.h"
#include "gameMode.h"
#include "storylineManager.h"
#include "textureAsciiConverter.h"

class TextureManager;

class GameModeAsciiRPG : public GameMode {
public:
	GameModeAsciiRPG(const TextureManager& textureMan);
	virtual ~GameModeAsciiRPG();

	Blackboard& GetBlackboard();
	StorylineManager& GetStoryMan();

	const Utils::AsciiImage* GetAsciiImage(const std::string& name) const;

private:
	virtual void InitImpl() override;
	virtual void UpdateImpl(float dt) override;
	virtual void DestroyImpl() override;

	void RegisterEnemies();
	void RegisterScripts();
	void RegisterBlackboard();
	void RegisterStoryline();

	LevelScriptFactory mLevelScriptFactory;
	Blackboard mBlackboard;

	std::unique_ptr<StorylineManager> mStorylineMan;

	const TextureManager& mTextureMan;

	TextureAsciiConverter mTextureAsciiConverter;
	std::unordered_map<std::string, std::unique_ptr<Utils::AsciiImage> > mConvertedAsciiImages;
};
