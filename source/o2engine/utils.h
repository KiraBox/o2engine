// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <string>
#include <vector>

namespace Utils {

class Random;

extern Random GLOBAL_RANDOM;

std::string Format(const char* format, ...);

class Timer {
public:
	Timer(float duration);
	void Reset();
	bool Done(float dt);
	float GetProgress() const;

private:
	float mCounter = 0.f;
	const float mDuration;
};

std::string ToLower(const std::string& str);
std::string ToUpper(const std::string& str);
bool ToNumber(const std::string& str, int& outNum);
std::string Capitalize(const std::string& str);

class Random {
public:
	Random(unsigned int seed, unsigned int cacheSize = 2048);

	unsigned int GetInt();
	float GetFloat();
	int Range(int _min, int _max);
	float Range(float _min, float _max);

	unsigned int GetSeed() const;

private:
	const unsigned int mCacheSize;
	std::vector<unsigned int> mCache;
	size_t mIdx = 0;

	unsigned int mSeed;
};

struct Color {
	float mR = 0.0f;
	float mG = 0.0f;
	float mB = 0.0f;
};

struct Image {
	unsigned int mWidth;
	unsigned int mHeight;

	struct PixelInfo {
		Color mColor;
	};

	std::vector<PixelInfo> mPixelInfo;
};

struct AsciiImage {
	unsigned int mWidth;
	unsigned int mHeight;

	struct CharInfo {
		int mCode;
		Color mColor;
	};

	std::vector<CharInfo> mCharInfo;
};

bool IsNotBlack(const Color& color);

}
