// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "event.h"
#include "eventBroadcaster.h"
#include "gameModeAsciiRPG.h"
#include "level.h"

Level::Level(GameMode& gameMode) : mGameMode(gameMode) {

}

void Level::Init() {
	EventBroadcaster::get().addEventListener<DialogueScriptEvent>(this, [&](const DialogueScriptEvent& _event) {
		return OnScriptDialogueEvent(_event);
	});
	EventBroadcaster::get().addEventListener<BattleScriptEvent>(this, [&](const BattleScriptEvent& _event) {
		return OnScriptBattleEvent(_event);
	});
	EventBroadcaster::get().addEventListener<BlackboardScriptEvent>(this, [&](const BlackboardScriptEvent& _event) {
		return OnBlackboardScriptEvent(_event);
	});
	EventBroadcaster::get().addEventListener<AddRemovePartyMemberScriptEvent>(this, [&](const AddRemovePartyMemberScriptEvent& _event) {
		return OnAddRemovePartyMemberScriptEvent(_event);
	});

	InitImpl();
}

void Level::Update(float dt) {
	UpdateImpl(dt);
}

void Level::Destroy() {
	DestroyImpl();

	EventBroadcaster::get().removeEventListener<DialogueScriptEvent>(this);
	EventBroadcaster::get().removeEventListener<BattleScriptEvent>(this);
	EventBroadcaster::get().removeEventListener<BlackboardScriptEvent>(this);
	EventBroadcaster::get().removeEventListener<AddRemovePartyMemberScriptEvent>(this);
}

void Level::SetName(const std::string& name) {
	mName = name;
}

const std::string& Level::GetName() const {
	return mName;
}

void Level::OnScriptDialogueEvent(const DialogueScriptEvent& _event) {
	mTextQueue.PushText(_event.mDialogue);
	AcknowledgeScriptEvent(true);
}

void Level::OnScriptBattleEvent(const BattleScriptEvent& _event) {
	if(mTextQueue.IsDone() && mReturnKeyPressed) {
		AcknowledgeScriptEvent(true);
		// todo: serialize previous level information for restoring state?

		std::unique_ptr<LevelTransitionInfo> transitionInfo = std::make_unique<LevelTransitionInfo>();
		transitionInfo->mPrevLevel = GetName();
		transitionInfo->mEnemies.assign(_event.mEnemies.begin(), _event.mEnemies.end());

		EventBroadcaster::get().broadcast(LoadLevelEvent("combat", std::move(transitionInfo)));

		static_cast<GameModeAsciiRPG&>(mGameMode).GetStoryMan().Pause(true);
	}
	else {
		AcknowledgeScriptEvent(false);
	}
}

void Level::OnBlackboardScriptEvent(const BlackboardScriptEvent& _event) {
	if(mTextQueue.IsDone() && mReturnKeyPressed) {
		AcknowledgeScriptEvent(true);
	}
	else {
		AcknowledgeScriptEvent(false);
	}
}

void Level::OnAddRemovePartyMemberScriptEvent(const AddRemovePartyMemberScriptEvent& _event) {
	if(mTextQueue.IsDone() && mReturnKeyPressed) {
		if(_event.mIsAdd) {
			mGameMode.AddPartyMember(_event.mActorType);
		}
		else {
			mGameMode.RemovePartyMember(_event.mActorType);
		}
		AcknowledgeScriptEvent(true);
	}
	else {
		AcknowledgeScriptEvent(false);
	}
}

void Level::AcknowledgeScriptEvent(bool accepted) const {
	EventBroadcaster::get().broadcast(ScriptEventAck(accepted));
}
