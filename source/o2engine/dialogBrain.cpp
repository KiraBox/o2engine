// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "dialogBrain.h"

DialogBrainType DialogBrain::GetBrainType() const {
	return mBrainType;
}

void DialogBrainFactory::RegisterBrain(std::unique_ptr<DialogBrain> brain) {
	if(mRegisteredBrains.empty()) {
		mRegisteredBrains.resize(static_cast<size_t>(DialogBrainType::NumTypes));
	}
	if(brain) {
		mRegisteredBrains[static_cast<size_t>(brain->GetBrainType())] = std::move(brain);
	}
}

DialogBrain* DialogBrainFactory::GetBrain(DialogBrainType brainType) const {
	if(!mRegisteredBrains.empty()) {
		if(const auto& brain = mRegisteredBrains[static_cast<size_t>(brainType)]) {
			return brain.get();
		}
	}
	return nullptr;
}

std::vector<std::string> MinDialogBrain::GetHurtText(bool isFriendlyFire) {
	std::vector<std::string> dialog;
	if(isFriendlyFire) {
		dialog.push_back("Ouch! What are you doing?");
	}
	return dialog;
}

std::vector<std::string> MinDialogBrain::GetDefeatedText(bool isFriendlyFire) {
	std::vector<std::string> dialog;
	if(isFriendlyFire) {
		dialog.push_back("How ... could you ...");
	}
	return dialog;
}
