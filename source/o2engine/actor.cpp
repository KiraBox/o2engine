// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "actor.h"
#include "actorAction.h"
#include "common.h"
#include "utils.h"

int Actor::mUniqueActorId = 0;

Actor::Actor()
	: mActorId(mUniqueActorId++) {

}

void Actor::SetHealth(int health) {
	mHealth = (health >= 0) ? health : 0;
}

int Actor::GetHealth() const {
	return mHealth;
}

void Actor::SetMaxHealth(int health, bool resetHealth) {
	mMaxHealth = (health >= 0) ? health : 0;
	if (mHealth > mMaxHealth || resetHealth) {
		mHealth = mMaxHealth;
	}
}

int Actor::GetMaxHealth() const {
	return mMaxHealth;
}

void Actor::Damage(int damage, DamageType type /* = DamageType::None */) {
	if (mHealth > 0) {
		mHealth -= damage;
		if (mHealth < 0) {
			mHealth = 0;
		}
	}
}

void Actor::Heal(int damage) {
	if (mHealth < mMaxHealth) {
		mHealth += mMaxHealth;
		if (mHealth > mMaxHealth) {
			mHealth = mMaxHealth;
		}
	}
}

int Actor::GetBasicAttackAmount(Utils::Random& random) const {
	float deviation = mAttack * mBasicAttackDeviation;
	float attack = mAttack - (0.5f * deviation) + random.GetFloat() * deviation;
	return static_cast<int>(attack);
}

std::string Actor::GetCombatText(const ActorActionInfo& info) const {
	O2ASSERT(info.mActor != nullptr);

	switch(info.mActionType) {
		case ActorActionType::None:
			return Utils::Format("%s did nothing.", Utils::Capitalize(info.mActor->GetName()).c_str());
			break;
		case ActorActionType::Attack:
			return Utils::Format("%s attacked %s for %i damage!", Utils::Capitalize(info.mActor->GetName()).c_str(), Utils::Capitalize(info.mTargetActor->GetName()).c_str(), info.mData.mAmount);
			break;
		default:
			O2FAIL();
			break;
	};

	return "UNIMPLEMENTED";
}

std::string Actor::GetName() const {
	if(mCustomName.empty()) {
		return ACTOR_TYPE_TO_NAME_MAP[mActorType].c_str();
	}
	else {
		return mCustomName;
	}
}

std::string Actor::GetNameAbbrv() const {
	std::string name = GetName();
	
	std::string nameAbbrv;
	for(size_t i = 0; i < name.size(); ++i) {
		const char& c = name[i];
		switch(c) {
			case ' ':
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				continue;
			default:
				nameAbbrv.push_back(c);
		};

		if(nameAbbrv.size() >= 4) {
			break;
		}
	}

	return nameAbbrv;
}

std::unique_ptr<Actor> Actor::Clone() const {
	std::unique_ptr<Actor> clone = std::make_unique<Actor>();

	clone->mActorType = mActorType;
	clone->mCustomName = mCustomName;

	//clone->mMagicPoints = mMagicPoints;
	//clone->mSkillPoints = mSkillPoints;
	clone->mAttack = mAttack;
	//clone->mDefense = mDefense;
	//clone->mEvasion = mEvasion;
	clone->mSpeed = mSpeed;
	//clone->mLuck = mLuck;

	clone->mBrain = mBrain;
	clone->mDialogBrain = mDialogBrain;

	clone->mHealth = mHealth;
	clone->mMaxHealth = mMaxHealth;

	return std::move(clone);
}

void ActorFactory::RegisterActor(std::unique_ptr<Actor> actor) {
	if (mRegisteredActors.empty()) {
		mRegisteredActors.resize(static_cast<size_t>(ActorType::NumTypes));
	}
	if (actor) {
		mRegisteredActors[static_cast<size_t>(actor->mActorType)] = std::move(actor);
	}
}

std::unique_ptr<Actor> ActorFactory::CreateActor(ActorType actorType) const {
	if (!mRegisteredActors.empty()) {
		if (const auto& actor = mRegisteredActors[static_cast<size_t>(actorType)]) {
			return actor->Clone();
		}
	}
	return nullptr;
}
