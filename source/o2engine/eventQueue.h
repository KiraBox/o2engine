// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <functional>
#include <mutex>
#include <vector>

template <typename EventType>
class EventQueue {
private:
	static std::unique_ptr<EventQueue<EventType>> mInstance;

public:
	static EventQueue<EventType>& get() {
		if (mInstance == nullptr) {
			mInstance = std::make_unique<EventQueue<EventType>>();
		}
		return *mInstance.get();
	}

	static void destroy() {
		if (mInstance) {
			mInstance.reset();
		}
	}

	template <typename ... Args>
	void pushEvent(Args&& ... params) {
		mMutex.lock();
		mEvents.emplace_back(std::make_unique<EventType>(params ...));
		mMutex.unlock();
	}

	void clear() {
		mMutex.lock();
		mEvents.clear();
		mMutex.unlock();
	}

	// callback returns true to consume the processed event
	void forEachEvent(std::function<bool(const EventType&)> callback) {
		if (callback != nullptr && !mEvents.empty()) {
			mMutex.lock();
			for (size_t i = 0; i < mEvents.size();) {
				if (callback(*mEvents[i].get())) {
					mEvents[i] = std::move(mEvents.back());
					mEvents.pop_back();
				}
				else {
					++i;
				}
			}
			mMutex.unlock();
		}
	}

private:
	std::vector<std::unique_ptr<EventType>> mEvents;
	std::mutex mMutex;
};

template <typename EventType>
std::unique_ptr<EventQueue<EventType>> EventQueue<EventType>::mInstance = nullptr;
