// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <string>
#include <vector>

enum WordGroup {
	Vegetables,
	Beverages,
	NumTypes
};

class SentenceBuilder {
public:
	SentenceBuilder(const std::string& str);
	SentenceBuilder(const std::string& str, WordGroup g1);
	SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2);
	SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2, WordGroup g3);
	SentenceBuilder(const std::string& str, WordGroup g1, WordGroup g2, WordGroup g3, WordGroup g4);

	//SentenceBuilder& operator=(const SentenceBuilder& rhs);
	std::string Build() const;

private:
	SentenceBuilder& Add(WordGroup group);

	std::string mSentence;
	std::vector<const std::vector<std::string>*> mWordGroups;
};

class SentencePicker {
public:
	void Add(const SentenceBuilder& sentenceBuilder);
	std::string Pick() const;

private:
	std::vector<SentenceBuilder> mSentenceBuilders;
};

class WordGroupRegistry {
public:
	// todo: data-drive this function
	static void Init();

	static void AddWord(WordGroup group, const std::string& word);
	static const std::vector<std::string>& Get(WordGroup group);

private:
	static std::vector<std::vector<std::string>> mWordGroups;
	static bool mInitialized;
};
