// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "audio.h"
#include "common.h"
#include "event.h"
#include "eventQueue.h"
#include "SDL.h"
#include "SDL_audio.h"
#include "SDLUtils.h"
#include <set>
#include <stdio.h>

struct SoundData {
	uint32_t mWavLength = 0;
	uint8_t* mWavBuffer = nullptr;
	SDL_AudioSpec mAudioSpec;

	uint32_t mId = 0;
	bool mLoop = false;
	int mVolume = 0;
	SoundType mType;

	uint32_t mStreamLength = 0;
	uint8_t* mStreamBuffer = nullptr;

	struct SoundData* mPrev = nullptr;
	struct SoundData* mNext = nullptr;
};

namespace {
	const int AUDIO_FREQUENCY = 44100;
	uint32_t AUDIO_UNIQUE_ID = 0;

	SDL_AudioSpec* mAudioSpec = nullptr;
	SDL_AudioDeviceID mAudioDevice = 0;

	// head of a linked list of sounds
	SoundData mSoundList;

	void Add(SoundData* data) {
		if (data != nullptr) {
			if (mSoundList.mNext == nullptr) {
				mSoundList.mNext = data;
			}
			else {
				SoundData* head = mSoundList.mNext;
				data->mNext = head;
				head->mPrev = data;
				mSoundList.mNext = data;
			}
		}
	}

	void Remove(SoundData* data) {
		if (data != nullptr) {
			SoundData* node = mSoundList.mNext;
			while (node != nullptr) {
				if (node == data) {
					if (node == mSoundList.mNext) {
						mSoundList.mNext = node->mNext;
					}
					if (node->mPrev != nullptr) {
						node->mPrev->mNext = node->mNext;
					}
					if (node->mNext != nullptr) {
						node->mNext->mPrev = node->mPrev;
					}

					delete node;
					return;
				}

				node = node->mNext;
			}
		}
	}

	void RemoveAll(uint32_t id) {
		SoundData* node = mSoundList.mNext;
		while (node != nullptr) {
			if (node->mId == id) {
				if (node == mSoundList.mNext) {
					mSoundList.mNext = node->mNext;
				}
				if (node->mPrev != nullptr) {
					node->mPrev->mNext = node->mNext;
				}
				if (node->mNext != nullptr) {
					node->mNext->mPrev = node->mPrev;
				}

				SoundData* temp = node->mNext;
				delete node;
				node = temp;
			}
			else {
				node = node->mNext;
			}
		}
	}
}

void audioCallbackFunc(void* userData, Uint8* stream, int len) {
	// silence the main buffer
	SDL_memset(stream, 0, len);

	SoundData* soundList = static_cast<SoundData*>(userData);
	SoundData* node = soundList ? soundList->mNext : nullptr;

	while (node != nullptr) {
		if (node->mStreamLength > 0) {
			uint32_t tempLen = (static_cast<uint32_t>(len) > node->mStreamLength) ? node->mStreamLength : static_cast<uint32_t>(len);

			SDL_MixAudioFormat(stream, node->mStreamBuffer, mAudioSpec->format, tempLen, node->mVolume);

			node->mStreamBuffer += tempLen;
			node->mStreamLength -= tempLen;

			node = node->mNext;
		}
		else if (node->mLoop) {
			node->mStreamBuffer = node->mWavBuffer;
			node->mStreamLength = node->mWavLength;
		}
		else {
			SoundData* temp = node->mNext;
			::Remove(node);
			node = temp;
		}
	}
}

void Audio::InitImpl() {
	mAudioSpec = new SDL_AudioSpec();

	SDL_AudioSpec* maxSpec = new SDL_AudioSpec();
	maxSpec->freq = AUDIO_FREQUENCY;
	maxSpec->format = AUDIO_S16LSB;
	maxSpec->channels = 2;			// stereo
	maxSpec->samples = 4096;
	maxSpec->callback = audioCallbackFunc;
	maxSpec->userdata = &::mSoundList;

	SDL_AudioSpec* minSpec = new SDL_AudioSpec();
	minSpec->freq = AUDIO_FREQUENCY;
	minSpec->format = AUDIO_S16LSB;
	minSpec->channels = 1;			// mono
	minSpec->samples = 512;
	minSpec->callback = audioCallbackFunc;
	minSpec->userdata = &::mSoundList;

	mAudioDevice = SDL_OpenAudioDevice(nullptr, 0, maxSpec, mAudioSpec, 0);
	if (SDLUtils::CheckError(mAudioDevice >= 0)) {
		O2LOG("Failed to initialize max spec audio.");
		if (mAudioSpec == nullptr) {
			std::swap(mAudioSpec, maxSpec);
		}

		mAudioDevice = SDL_OpenAudioDevice(nullptr, 0, minSpec, mAudioSpec, 0);
		if (SDLUtils::CheckError(mAudioDevice >= 0)) {
			O2LOG("Failed to initialize min spec audio.");
			if (mAudioSpec == nullptr) {
				std::swap(mAudioSpec, minSpec);
			}
		}
	}

	delete maxSpec;
	maxSpec = nullptr;

	delete minSpec;
	minSpec = nullptr;

	SDL_PauseAudioDevice(mAudioDevice, 0);
}

void Audio::UpdateImpl(float dt) {

}

void Audio::DestroyImpl() {
	SDL_CloseAudio();

	if (mAudioSpec != nullptr) {
		delete mAudioSpec;
		mAudioSpec = nullptr;
	}
}

Audio::Audio()
	: mGlobalVolume(SDL_MIX_MAXVOLUME) {
	for (size_t i = 0; i < static_cast<size_t>(SoundType::NumTypes); ++i) {
		mVolume[i] = 1.0f;
	}
}

void Audio::Init() {
	InitImpl();
}

void Audio::Update(float dt) {
	std::map<std::string, std::tuple<std::string, SoundType>> soundsToLoad;
	EventQueue<LoadSound>::get().forEachEvent([&](const LoadSound& _event) {
		soundsToLoad[_event.mName] = { _event.mFilename, _event.mType };
		return true;
	});

	std::set<std::string> soundsToUnload;
	EventQueue<UnloadSound>::get().forEachEvent([&](const UnloadSound& _event) {
		soundsToUnload.insert(_event.mName);
		return true;
	});

	for (auto&& name : soundsToUnload) {
		auto elem = mLoadedSounds.find(name);
		if (elem != mLoadedSounds.end()) {
			::RemoveAll(elem->second->mId);
			SDL_FreeWAV(elem->second->mWavBuffer);
			mLoadedSounds.erase(elem);
			//O2LOG("Unloaded sound '%s'", name.c_str());
		}
	}

	for (auto&& it : soundsToLoad) {
		SoundData* data = new SoundData();
		const std::string filename = std::get<0>(it.second);
		const SoundType type = std::get<1>(it.second);
		if (SDLUtils::CheckError(SDL_LoadWAV(filename.c_str(), &(data->mAudioSpec), &(data->mWavBuffer), &(data->mWavLength)) != nullptr)) {
			O2LOG("Failed to load sound '%s' from file: '%s'", it.first.c_str(), filename.c_str());
		}
		else {
			data->mStreamBuffer = data->mWavBuffer;
			data->mStreamLength = data->mWavLength;

			data->mId = AUDIO_UNIQUE_ID++;
			data->mType = type;
			
			mLoadedSounds[it.first] = data;
			//O2LOG("Loaded sound '%s' from file: '%s'", it.first.c_str(), it.second.c_str());
		}
	}

	EventQueue<PauseAudio>::get().forEachEvent([&](const PauseAudio& _event) {
		mPaused = _event.mState;
		SDL_PauseAudioDevice(mAudioDevice, mPaused ? 1 : 0);
		return true;
	});

	if (mPaused) {
		return;
	}

	EventQueue<PlaySoundEvent>::get().forEachEvent([&](const PlaySoundEvent& _event) {
		PlaySound(_event.mName, _event.mLoop);
		return true;
	});

	UpdateImpl(dt);
}

void Audio::Destroy() {
	DestroyImpl();
}

void Audio::PlaySound(const std::string& name, bool loop) {
	auto elem = mLoadedSounds.find(name);
	if (elem != mLoadedSounds.end()) {
		SoundData* data = new SoundData();
		SDL_memcpy(data, elem->second, sizeof(SoundData));

		data->mLoop = loop;
		data->mVolume = static_cast<int>(mGlobalVolume * mVolume[static_cast<size_t>(data->mType)]);

		SDL_LockAudioDevice(mAudioDevice);

		::Add(data);

		SDL_UnlockAudioDevice(mAudioDevice);
	}
}
