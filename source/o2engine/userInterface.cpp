// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "CanvasConsole.h"
#include "common.h"
#include "userInterface.h"

void UserInterface::Init() {
	RegisterCanvas("title", [&]() { return std::make_unique<CanvasConsole>(); });
}

void UserInterface::Update(float dt) {
	for(auto& canvas : mActiveCanvas) {
		if (canvas.second != nullptr) {
			canvas.second->Update(dt);
		}
	}
}

void UserInterface::Destroy() {
	for (auto& canvas : mActiveCanvas) {
		if (canvas.second != nullptr) {
			canvas.second->Destroy();
		}
	}
	mActiveCanvas.clear();
}

void UserInterface::RegisterCanvas(const std::string& name, std::function<std::unique_ptr<Canvas>()> creator) {
	mCanvasRegistry[name] = creator;
}

void UserInterface::LoadCanvas(const std::string& name) {
	O2ALOG(mActiveCanvas.find(name) == mActiveCanvas.end(), "Canvas '%s' already loaded!", name.c_str());

	auto it = mCanvasRegistry.find(name);
	if(it != mCanvasRegistry.end() && it->second != nullptr) {
		std::unique_ptr<Canvas> canvas = it->second();
		canvas->Init();

		mActiveCanvas[name] = std::move(canvas);
	}
	else {
		O2LOG("Canvas '%s' has not been registered or has a null creator function.", name.c_str());
	}
}

void UserInterface::UnloadCanvas(const std::string& name) {
	auto it = mActiveCanvas.find(name);
	if (it != mActiveCanvas.end()) {
		if (it->second != nullptr) {
			it->second->Destroy();
		}
		mActiveCanvas.erase(it);
	}
}

size_t UserInterface::NumActiveCanvas() const {
	return mActiveCanvas.size();
}
