// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class Window;

class Engine {
public:
	void Run();
	void Shutdown();

private:
	void InitSDL();
	void DestroySDL();

	void UpdatePlatform();

private:
	bool mIsRunning = true;
};
