// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include <algorithm>
#include "CanvasConsole.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "inputMapping.h"
#include "sharedConstants.h"
#include <string>
#include "utils.h"
#include <vector>

namespace {
	static Console mConsole("O2:\\>", SCREEN_CHARS_HEIGHT, SCREEN_CHARS_WIDTH);
}

void CanvasConsole::Init() {
	mConsole.Init();

	mConsole.Append("O2 Console [Version 0.1]");
	mConsole.PushLine("Copyright (c) 2019 O2Studios. All rights reserved.");
	mConsole.PushLine("");
	mConsole.PushLine(mConsole.GetLineHeader());

	EventBroadcaster::get().addEventListener<KeyPressedEvent>(this, [&](const KeyPressedEvent& _event) {
		mConsole.OnKeyPressed(_event.mKey);
	});
	EventBroadcaster::get().addEventListener<KeyReleasedEvent>(this, [&](const KeyReleasedEvent& _event) {
		mConsole.OnKeyReleased(_event.mKey);
	});

	mConsole.RegisterCommandNoParams("credits.exe", [&]() {
		mConsole.PushLine("R Tyler Rae - {d0.1|e v e r y t h i n g}");
		mConsole.PushLine("");
		mConsole.PushLine(mConsole.GetLineHeader());
		return true;
	});

	mConsole.RegisterCommandNoParams("credits", [&]() {
		mConsole.PushLine("R Tyler Rae - {d0.1|e v e r y t h i n g}");
		mConsole.PushLine("");
		mConsole.PushLine(mConsole.GetLineHeader());
		return true;
	});

	mConsole.RegisterCommandNoParams("exit", [&]() {
		EventBroadcaster::get().broadcast<ExitGameEvent>(ExitGameEvent());
		return true;
	});

	mConsole.RegisterCommandNoParams("options.exe", [&]() {
		mConsole.PushLine("Options level placeholder.");
		mConsole.PushLine("");
		mConsole.PushLine(mConsole.GetLineHeader());
		return true;
	});

	mConsole.RegisterCommandNoParams("options", [&]() {
		mConsole.PushLine("Options level placeholder.");
		mConsole.PushLine("");
		mConsole.PushLine(mConsole.GetLineHeader());
		return true;
	});

	mConsole.RegisterCommandNoParams("play.exe", [&]() {
		EventBroadcaster::get().broadcast<LoadLevelEvent>(LoadLevelEvent("intro"));
		return true;
	});

	mConsole.RegisterCommandNoParams("play", [&]() {
		EventBroadcaster::get().broadcast<LoadLevelEvent>(LoadLevelEvent("intro"));
		return true;
	});
}

void CanvasConsole::Destroy() {
	mConsole.Destroy();

	EventBroadcaster::get().removeEventListener<KeyPressedEvent>(this);
}

void CanvasConsole::Update(float dt) {
	mConsole.Update(dt);
	mConsole.Render();
}

Console::Console(const std::string& lineHeader, unsigned int height, unsigned width)
	: mRowIdx(0)
	, mLineHeader(lineHeader)
	, mWidth(width)
	, mHeight(height)
	, mCursor('_')
	, mDelaySeconds(0.f)
	, mDelayAccum(0.f)
	, mCursorToggle(false) {

	mIsShiftHeld[0] = false;
	mIsShiftHeld[1] = false;
	mIsBackspaceHeld = false;

	mBackspaceDelaySeconds[0] = 0.f;
	mBackspaceDelaySeconds[1] = 0.f;
	mBackspaceDelayAccum[0] = 0.f;
	mBackspaceDelayAccum[1] = 0.f;
}

void Console::Init() {
	mDelaySeconds = 0.5f;
	mBackspaceDelaySeconds[0] = 0.5f;
	mBackspaceDelaySeconds[1] = 0.02f;

	mRows.resize(mHeight);
	for (size_t i = 0; i < mRows.size(); ++i) {
		mRows[i].mPosY = static_cast<float>(SCREEN_CHARS_HEIGHT - i - 1);
	}

	mConsole.RegisterCommandNoParams("clear", [&]() {
		Clear();
		return true;
	});

	mConsole.RegisterCommandNoParams("dir", [&]() {
		mConsole.PushLine("       credits.exe");
		mConsole.PushLine("       options.exe");
		mConsole.PushLine("       play.exe");
		mConsole.PushLine("");
		mConsole.PushLine(mLineHeader);
		return true;
	});

	mConsole.RegisterCommandNoParams("help", [&]() {
		mConsole.PushLine("CLEAR   Clears the screen.");
		mConsole.PushLine("DIR     Displays a list of files and subdirectories.");
		mConsole.PushLine("HELP    Provides help information for O2 Console commands.");
		mConsole.PushLine("EXIT    Closes the application.");
		mConsole.PushLine("");
		mConsole.PushLine(mLineHeader);
		return true;
	});
}

void Console::Destroy() {
	mRows.clear();
	mRowIdx = 0;

	mDelayAccum = 0.f;
	mCursorToggle = false;

	mIsShiftHeld[0] = false;
	mIsShiftHeld[1] = false;
	mIsBackspaceHeld = false;

	mBackspaceDelayAccum[0] = false;
	mBackspaceDelayAccum[1] = false;

	mCommandNames.clear();
	mCommandsNoParams.clear();
}

void Console::Update(float dt) {
	mDelayAccum += dt;
	if (mDelayAccum >= mDelaySeconds) {
		mDelayAccum -= mDelaySeconds;

		const std::string& text = mRows[mRowIdx].GetText();

		if (mCursorToggle) {
			mRows[mRowIdx].SetText(text.substr(0, text.size() - 1));
		}
		else {
			std::string textCopy(text);
			textCopy.push_back(mCursor);
			mRows[mRowIdx].SetText(textCopy);
		}

		mCursorToggle = !mCursorToggle;
	}

	if (mIsBackspaceHeld) {
		mBackspaceDelayAccum[0] += dt;
		if (mBackspaceDelayAccum[0] >= mBackspaceDelaySeconds[0]) {
			mBackspaceDelayAccum[1] += dt;
			if (mBackspaceDelayAccum[1] > mBackspaceDelaySeconds[1]) {
				mBackspaceDelayAccum[1] -= mBackspaceDelaySeconds[1];
				Remove();
			}
		}
	}
	else {
		mBackspaceDelayAccum[0] = 0.f;
		mBackspaceDelayAccum[1] = 0.f;
	}

	for (size_t i = 0; i < mRows.size(); ++i) {
		mRows[i].Update(dt);
	}
}

void Console::Render() {
	for (size_t i = 0; i < mRows.size(); ++i) {
		mRows[i].Render();
	}
}

void Console::PushLine(const std::string& str) {
	if (mCursorToggle) {
		const std::string& text = mRows[mRowIdx].GetText();
		mRows[mRowIdx].SetText(text.substr(0, text.size() - 1));
		mCursorToggle = false;
	}

	if (mRowIdx == mRows.size() - 1) {
		for (size_t i = 1; i < mRows.size(); ++i) {
			mRows[i - 1].SetText(mRows[i].GetText());
		}
	}

	mRowIdx = std::min(mRowIdx + 1, mRows.size() - 1);
	//mRows[mRowIdx].SetText(str.substr(0, mWidth - 2 /*cursor*/));
	mRows[mRowIdx].SetText(str);
}

void Console::Append(unsigned char c) {
	std::string text = mRows[mRowIdx].GetText();
	if (text.size() >= mWidth - 1) {
		return;
	}

	if (mCursorToggle) {
		text.pop_back();
		text.push_back(c);
		text.push_back(mCursor);
	}
	else {
		text.push_back(c);
	}

	mRows[mRowIdx].SetText(text);
}

void Console::Append(const std::string& str) {
	for (size_t i = 0; i < str.size(); ++i) {
		Append(str[i]);
	}
}

void Console::Remove() {
	std::string text = mRows[mRowIdx].GetText();

	if (mCursorToggle) {
		text.pop_back();
	}

	size_t idx = text.find_first_of(mLineHeader);
	if (idx != std::string::npos) {
		text = text.substr(mLineHeader.size());
	}

	if (!text.empty()) {
		text.pop_back();
	}

	if (idx != std::string::npos) {
		text = mLineHeader + text;
	}

	if (mCursorToggle) {
		text.push_back(mCursor);
	}

	mRows[mRowIdx].SetText(text);
}

void Console::Clear() {
	for (size_t i = 0; i < mRows.size(); ++i) {
		mRows[i].SetText("");
	}

	mRowIdx = 0;
	mCursorToggle = false;

	PushLine(mLineHeader);
}

void Console::ClearLine() {
	std::string text = mRows[mRowIdx].GetText();

	if (mCursorToggle) {
		text.pop_back();
	}

	size_t idx = text.find_first_of(mLineHeader);
	if (idx != std::string::npos) {
		text = text.substr(0, mLineHeader.size());
	}

	if (mCursorToggle) {
		text.push_back(mCursor);
	}

	mRows[mRowIdx].SetText(text);
}

std::string Console::GetLine() const {
	std::string text = mRows[mRowIdx].GetText();

	if (mCursorToggle) {
		text.pop_back();
	}

	size_t idx = text.find_first_of(mLineHeader);
	if (idx != std::string::npos) {
		text = text.substr(mLineHeader.size());
	}

	return text;
}

void Console::OnKeyPressed(EKeyboard key) {
	if (key == EKeyboard::RETURN || key == EKeyboard::RETURN2) {
		const std::string command = Utils::ToLower(GetLine());
		auto it = mCommandsNoParams.find(command);
		if (it != mCommandsNoParams.end()) {
			it->second();
		}
		else if(command.empty()) {
			mConsole.PushLine(mLineHeader);
		}
		else {
			mConsole.PushLine("Command not found, use HELP to print a list of available commands.");
			mConsole.PushLine("");
			mConsole.PushLine(mLineHeader);
		}
	}
	else if (key == EKeyboard::LSHIFT) {
		mIsShiftHeld[0] = true;
	}
	else if (key == EKeyboard::RSHIFT) {
		mIsShiftHeld[1] = true;
	}

	if (key >= EKeyboard::A && key <= EKeyboard::Z) {
		unsigned char difference = static_cast<unsigned char>(key) - static_cast<unsigned char>(EKeyboard::A);

		if (IsShiftHeld()) {
			Append('A' + difference);
		}
		else {
			Append('a' + difference);
		}
	}
	else if (key >= EKeyboard::ZERO && key <= EKeyboard::NINE) {
		unsigned char difference = static_cast<unsigned char>(key) - static_cast<unsigned char>(EKeyboard::ZERO);
		Append('0' + difference);
	}
	else if (key == EKeyboard::SPACE) {
		Append(' ');
	}
	else if (key == EKeyboard::BACKSPACE) {
		Remove();
		mIsBackspaceHeld = true;
	}
	else if (key == EKeyboard::ESCAPE) {
		ClearLine();
	}
	else if (key == EKeyboard::PERIOD) {
		Append('.');
	}
	else if (key == EKeyboard::TAB) {
		AutoComplete();
	}
}

void Console::OnKeyReleased(EKeyboard key) {
	if (key == EKeyboard::LSHIFT) {
		mIsShiftHeld[0] = false;
	}
	else if (key == EKeyboard::RSHIFT) {
		mIsShiftHeld[1] = false;
	}
	else if (key == EKeyboard::BACKSPACE) {
		mIsBackspaceHeld = false;
	}
}

void Console::RegisterCommandNoParams(const std::string& name, std::function<bool()> command) {
	if (command != nullptr) {
		mCommandNames.push_back(name);
		mCommandsNoParams[name] = command;
	}
}

void Console::AutoComplete() {
	if (mCommandNames.empty()) {
		return;
	}

	std::string line = GetLine();
	if (line.empty()) {
		return;
	}

	size_t offset = line.find_last_of(' ');
	if (offset != std::string::npos) {
		line = line.substr(offset + 1);
	}

	std::vector<size_t> numSimilarChars;
	numSimilarChars.resize(mCommandNames.size());

	for (size_t i = 0; i < mCommandNames.size(); ++i) {
		const std::string& name = mCommandNames[i];
		if (line.size() < name.size()) {
			for (size_t j = 0; j < line.size(); ++j) {
				if (line[j] != name[j]) {
					break;
				}
				++numSimilarChars[i];
			}
		}
	}

	size_t idx = 0;
	size_t maxValue = numSimilarChars[0];

	for (size_t i = 1; i < numSimilarChars.size(); ++i) {
		if (numSimilarChars[i] > maxValue || (numSimilarChars[i] == maxValue && mCommandNames[i].size() < mCommandNames[idx].size())) {
			maxValue = numSimilarChars[i];
			idx = i;
		}
	}

	// no command matches the full line
	if (maxValue < line.size()) {
		return;
	}

	line = mCommandNames[idx];

	if (offset != std::string::npos) {
		line = GetLine().substr(0, offset + 1) + line;
	}

	ClearLine();
	Append(line);
}
