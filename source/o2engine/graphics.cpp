// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "event.h"
#include "eventQueue.h"
#include "GL/glew.h"
#include "GL/wglew.h"
#include "graphics.h"
#include "SDL.h"
#include "SDLUtils.h"
#include "sharedConstants.h"
#include "textAtlas.h"
#include "textureManager.h"
#include "window.h"

namespace {
	SDL_Window* mSdlWindow = nullptr;
	SDL_GLContext mContext;
	SDL_PixelFormat* mPixelFromat = nullptr;
	GLuint mProgramId = 0;
}

void LoadShader() {
	GLuint shaderId[2] = { 0, 0 }; 

	auto makeShader = [&](const std::string& filename, GLenum shaderType, GLuint& shaderId) -> bool {
		bool shaderCreated = false;
		SDL_RWops* infile = SDL_RWFromFile(filename.c_str(), "r");
		if (SDLUtils::CheckError(infile != nullptr) == false) {
			int size = static_cast<int>(SDL_RWsize(infile));
			if (SDLUtils::CheckError(size > 0) == false) {
				char* buffer = new char[size + 1];
				memset(buffer, 0, size + 1);

				if (SDLUtils::CheckError(SDL_RWread(infile, buffer, size, 1) != 0) == false) {
					shaderId = glCreateShader(shaderType);
					glShaderSource(shaderId, 1, &buffer, &size);
					glCompileShader(shaderId);

					GLint status = 0;
					glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
					if (status == 0) {
						O2LOG("Failed to compile shader '%s' with errors: ", filename.c_str());

						GLint len = 0;
						glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &len);

						char* logBuffer = new char[len + 1];
						memset(logBuffer, 0, len + 1);
						glGetShaderInfoLog(shaderId, len, nullptr, logBuffer);
						O2LOG("%s", logBuffer);

						delete[] logBuffer;
						logBuffer = nullptr;
					}

					shaderCreated = (status != 0);
				}

				delete[] buffer;
				buffer = nullptr;
			}

			SDL_RWclose(infile);
		}

		return shaderCreated;
	};

	if (makeShader("data/shaders/shader.vert", GL_VERTEX_SHADER, shaderId[0]) && 
		makeShader("data/shaders/shader.frag", GL_FRAGMENT_SHADER, shaderId[1])) 
	{
		mProgramId = glCreateProgram();
		glAttachShader(mProgramId, shaderId[0]);
		glAttachShader(mProgramId, shaderId[1]);
		glLinkProgram(mProgramId);

		glUseProgram(mProgramId);
	}
}

void Graphics::InitImpl() {
	mSdlWindow = static_cast<SDL_Window*>(mWindow->GetWindow());
	mContext = SDL_GL_CreateContext(mSdlWindow);

	glewInit();

	LoadShader();
}

void Graphics::UpdateImpl() {
	const int windowWidth = mWindow->GetWidth();
	const int windowHeight = mWindow->GetHeight();

	const float invWindowWidth = 1.0f / windowWidth;
	const float invWindowHeight = 1.0f / windowHeight;

	glViewport(0, 0, windowWidth, windowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	EventQueue<Render2DQuadEvent>::get().forEachEvent([&](const Render2DQuadEvent& _event) {
		glPushMatrix();
		glLoadIdentity();

		glEnable(GL_TEXTURE_2D);

		glActiveTexture(GL_TEXTURE0);

		TextureManager::TextureInfo textureInfo;
		if (mTextureMan.UseTexture(_event.mTextureName, textureInfo)) {
			float topLeftU = _event.mTopLeftU;
			float topLeftV = _event.mTopLeftV;
			float botRightU = _event.mBotRightU;
			float botRightV = _event.mBotRightV;

			float textureWidth = _event.mAtlassed ? (botRightU - topLeftU) * textureInfo.mWidth : textureInfo.mWidth;
			float textureHeight = _event.mAtlassed ? (botRightV - topLeftV) * textureInfo.mHeight : textureInfo.mHeight;

			float invTextureWidth = 1.0f / textureWidth;
			float invTextureHeight = 1.0f / textureHeight;

			float posX = _event.mCentered ? _event.mPosX - (textureWidth * 0.5f) : _event.mPosX;
			float posY = _event.mCentered ? _event.mPosY - (textureHeight * 0.5f) : _event.mPosY;

			glScalef(invWindowWidth * 2.0f, invWindowHeight * 2.0f, 1.0f);
			glScalef(textureWidth, textureHeight, 1.0f);
			glScalef(static_cast<float>(_event.mScaleX), static_cast<float>(_event.mScaleY), 1.0f);

			glTranslatef(posX * invTextureWidth, posY * invTextureHeight, 0.0f);

			GLint textureLocation = glGetUniformLocation(mProgramId, "texture");
			glUniform1i(textureLocation, 0);

			GLint colorRGBLocation = glGetUniformLocation(mProgramId, "colorRGB");
			glUniform3f(colorRGBLocation, _event.mRGB[0], _event.mRGB[1], _event.mRGB[2]);

			glBegin(GL_QUADS);

			glTexCoord2f(topLeftU, topLeftV);
			glVertex2f(0.0f, 1.0f);

			glTexCoord2f(topLeftU, botRightV);
			glVertex2f(0.0f, 0.0f);

			glTexCoord2f(botRightU, botRightV);
			glVertex2f(1.0f, 0.0f);

			glTexCoord2f(botRightU, topLeftV);
			glVertex2f(1.0f, 1.0f);

			glEnd();
		}

		glDisable(GL_TEXTURE_2D);

		glPopMatrix();

		// consume event
		return true;
	});

	SDL_GL_SwapWindow(mSdlWindow);
}

void Graphics::DestroyImpl() {
	SDL_FreeFormat(mPixelFromat);
}

Graphics::Graphics(const TextureManager& textureMan)
	: mTextureMan(textureMan) {
}

void Graphics::Init(const Window& window) {
	mWindow = &window;



	InitImpl();
}

void Graphics::Update() {
	UpdateImpl();
}

void Graphics::Destroy() {
	DestroyImpl();
}
