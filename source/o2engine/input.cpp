// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "event.h"
#include "eventBroadcaster.h"
#include "eventQueue.h"
#include "input.h"
#include "inputMapping.h"
#include <set>

namespace {
	std::set<int> mKeyPressMap;
}

void Input::UpdateImpl() {
	EventQueue<PlatformInputEvent>::get().forEachEvent([](const PlatformInputEvent& _event) {
		auto keyElem = mKeyPressMap.find(_event.mKeycode);
		const bool prevDown = (keyElem != mKeyPressMap.end());
		if (prevDown && !_event.mKeyDown) {
			mKeyPressMap.erase(keyElem);

			auto elem = SDL_INPUT_TO_KEYBOARD_MAP.find(_event.mKeycode);
			if (elem != SDL_INPUT_TO_KEYBOARD_MAP.end()) {
				KeyReleasedEvent keyReleased(elem->second);
				EventBroadcaster::get().broadcast(keyReleased);
			}
		}
		if (!prevDown && _event.mKeyDown) {
			mKeyPressMap.insert(_event.mKeycode);

			auto elem = SDL_INPUT_TO_KEYBOARD_MAP.find(_event.mKeycode);
			if (elem != SDL_INPUT_TO_KEYBOARD_MAP.end()) {
				KeyPressedEvent keyPressed(elem->second);
				EventBroadcaster::get().broadcast(keyPressed);
			}
		}
		// consume event
		return true;
	});
}

void Input::Update() {
	UpdateImpl();
}
