// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actor.h"
#include <string>

class Blackboard;

enum class ScriptNodeType {
	Dialogue = 0,
	Battle,
	Blackboard,
	AddRemovePartyMember
};

class ScriptNode {
public:
	ScriptNode(ScriptNodeType nodeType) : mType(nodeType) {}
	virtual ~ScriptNode() {}

	bool IsComplete() const;

	virtual void Tick(float dt, Blackboard& blackboard) {}

protected:
	ScriptNodeType mType;
	bool mIsComplete = false;
};

class DialogueScriptNode : public ScriptNode {
public:
	DialogueScriptNode(const std::string& str)
		: ScriptNode(ScriptNodeType::Dialogue)
		, mDialogue(str) {
	}
	virtual ~DialogueScriptNode() {}
	virtual void Tick(float dt, Blackboard& blackboard) override;

	std::string mDialogue;
};

class BattleScriptNode : public ScriptNode {
public:
	BattleScriptNode()
		: ScriptNode(ScriptNodeType::Battle) {
	}
	virtual ~BattleScriptNode() {}
	virtual void Tick(float dt, Blackboard& blackboard) override;

	std::vector<ActorType> mEnemies;
};

class BlackboardScriptNode : public ScriptNode {
public:
	enum OperationType {
		Assign = 0
	};

	enum ValueType {
		Boolean = 0
	};

	BlackboardScriptNode()
		: ScriptNode(ScriptNodeType::Blackboard) {
	}
	virtual ~BlackboardScriptNode() {}
	virtual void Tick(float dt, Blackboard& blackboard) override;

	std::string mTargetKey;
	OperationType mOpType;
	int mValue;
};

class AddRemovePartyMemberScriptNode : public ScriptNode {
public:
	AddRemovePartyMemberScriptNode()
		: ScriptNode(ScriptNodeType::AddRemovePartyMember) {
	}
	virtual ~AddRemovePartyMemberScriptNode() {}
	virtual void Tick(float dt, Blackboard& blackboard) override;

	bool mIsAdd = true;
	ActorType mActorType;
};
