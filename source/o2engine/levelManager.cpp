// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "levelManager.h"

void LevelManager::RegisterLevel(const std::string& name, std::function<std::unique_ptr<Level>(GameMode&)> createFunc) {
	O2ASSERT(mLevelRegistry.find(name) == mLevelRegistry.end());
	mLevelRegistry[name] = createFunc;
}

void LevelManager::LoadLevel(const std::string& name) {
	auto elem = mLevelRegistry.find(name);
	if (elem != mLevelRegistry.end()) {
		if (mLevel != nullptr) {
			mLevel->Destroy();
			mLevel.reset();
		}

		mLevel = elem->second(mGameMode);
		mLevel->SetName(name);

		mLevel->Init();

		O2LOG("Loaded level '%s'", name.c_str());
	}
	else {
		O2LOG("Level '%s' not found", name.c_str());
	}
}

void LevelManager::Init() {
	EventBroadcaster::get().addEventListener<LoadLevelEvent>(this, [&](const LoadLevelEvent& _event) {
		NextLevel(_event.mName);
		if (_event.mLevelTransitionInfo) {
			mLevelTransitionInfo = std::make_unique<LevelTransitionInfo>(*_event.mLevelTransitionInfo);
		}
		else {
			mLevelTransitionInfo = nullptr;
		}
	});
}

void LevelManager::Update(float dt) {
	if (!mNextLevel.empty()) {
		LoadLevel(mNextLevel);
		mNextLevel.clear();
	}

	if (mLevel != nullptr) {
		mLevel->Update(dt);
	}
}

void LevelManager::Destroy() {
	if (mLevel != nullptr) {
		O2LOG("Unloaded level '%s'", mLevel->GetName().c_str());

		mLevel->Destroy();
		mLevel.reset();
	}

	EventBroadcaster::get().removeEventListener<LoadLevelEvent>(this);
}

const LevelTransitionInfo* LevelManager::GetLevelTransitionInfo() const {
	if (mLevelTransitionInfo) {
		return &*mLevelTransitionInfo;
	}
	else {
		return nullptr;
	}
}

void LevelManager::NextLevel(const std::string& name) {
	O2ALOG(mNextLevel.empty(), "Already transitioning to a new level! '%s' will be overwritten by '%s'", mNextLevel.c_str(), name.c_str());
	mNextLevel = name;
}
