// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "event.h"
#include "eventQueue.h"
#include "sharedConstants.h"
#include "textAtlas.h"

TextAtlas::TextAtlas(const std::string& textureName, unsigned int atlasWidth, unsigned int atlasHeight, unsigned int width, unsigned int height)
	: mTextureName(textureName)
	, mAtlasWidth(atlasWidth)
	, mAtlasHeight(atlasHeight)
	, mWidth(width)
	, mHeight(height) {

	O2ALOG(mAtlasWidth % mWidth == 0, "Invalid atlas width.");
	O2ALOG(mAtlasHeight % mHeight == 0, "Invalid atlas height.");

	mNumWidth = mAtlasWidth / mWidth;
	mNumHeight = mAtlasHeight / mHeight;

	float incWidth = 1.0f / mNumWidth;
	float incHeight = 1.0f / mNumHeight;

	mCells.resize(mNumWidth * mNumHeight);
	for (size_t h = 0; h < mNumHeight; ++h) {
		for (size_t w = 0; w < mNumWidth; ++w) {
			Cell& cell = mCells[h * mNumWidth + w];
			cell.mTopLeftU = w * incWidth;
			cell.mTopLeftV = h * incHeight;
			cell.mBotRightU = (w + 1) * incWidth;
			cell.mBotRightV = (h + 1) * incHeight;
		}
	}
}

size_t TextAtlas::getNumCells() const {
	return mCells.size();
}

const TextAtlas::Cell& TextAtlas::getCell(size_t idx) const {
	O2ASSERT(idx < getNumCells());
	return mCells[idx];
}

void IBMPCRenderer::Draw(size_t code, float x, float y) const {
	const auto& cell = mAtlas.getCell(code);

	int posX = -1 * (SCREEN_WIDTH / 2) + static_cast<int>(x);
	int posY = -1 * (SCREEN_HEIGHT / 2) + static_cast<int>(y);

	Render2DQuadEvent render(mAtlas.getTextureName());
	render.atlassed()
		.topLeft(cell.mTopLeftU, cell.mTopLeftV)
		.botRight(cell.mBotRightU, cell.mBotRightV)
		.position(posX, posY)
		.color(mColor[0], mColor[1], mColor[2]);

	EventQueue<Render2DQuadEvent>::get().pushEvent(render);
}

void IBMPCRenderer::DrawASCII(unsigned char c, float x, float y) const {
	// ascii characters
	if (c > ' ' && c <= '~') {
		Draw(static_cast<size_t>(c), x, y);
	}
}

void IBMPCRenderer::SetColor(float r, float g, float b) {
	mColor[0] = r;
	mColor[1] = g;
	mColor[2] = b;
}

void IBMPCRenderer::SetDefaultColor() {
	mColor[0] = mDefaultColor[0];
	mColor[1] = mDefaultColor[1];
	mColor[2] = mDefaultColor[2];
}
