// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "event.h"
#include <functional>
#include "level.h"
#include <memory>
#include <string>
#include <unordered_map>

class LevelManager {
public:
	LevelManager(GameMode& gameMode)
		: mGameMode(gameMode) {}

	void RegisterLevel(const std::string& name, std::function<std::unique_ptr<Level>(GameMode&)> createFunc);
	void LoadLevel(const std::string& name);

	void Init();
	void Update(float dt);
	void Destroy();

	const LevelTransitionInfo* GetLevelTransitionInfo() const;

private:
	void NextLevel(const std::string& name);

	std::unordered_map<std::string, std::function<std::unique_ptr<Level>(GameMode&)> > mLevelRegistry;
	std::unique_ptr<Level> mLevel;

	GameMode& mGameMode;
	std::unique_ptr<LevelTransitionInfo> mLevelTransitionInfo;

	std::string mNextLevel;
};
