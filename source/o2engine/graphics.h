// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class TextureManager;
class Window;

class Graphics {
public:
	Graphics(const TextureManager& textureMan);

	void Init(const Window& window);
	void Update();
	void Destroy();

private:
	void InitImpl();
	void UpdateImpl();
	void DestroyImpl();

private:
	const Window* mWindow = nullptr;
	const TextureManager& mTextureMan;
};
