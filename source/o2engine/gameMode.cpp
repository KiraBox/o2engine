// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "gameMode.h"

void GameMode::Init() {
	mLevelManager = std::make_unique<LevelManager>(*this);
	mUI = std::make_unique<UserInterface>();

	mLevelManager->Init();
	mUI->Init();

	InitImpl();
}

void GameMode::Update(float dt) {
	mLevelManager->Update(dt);
	mUI->Update(dt);

	UpdateImpl(dt);
}

void GameMode::Destroy() {
	DestroyImpl();

	mUI->Destroy();
	mLevelManager->Destroy();
}

UserInterface& GameMode::GetUI_DEPRECATED() {
	return *mUI;
}

Actor& GameMode::GetPlayer() {
	return mPlayer;
}

std::vector<Actor*> GameMode::GetParty() {
	std::vector<Actor*> party;
	party.resize(mParty.size() + 1);

	party[0] = &mPlayer;
	for(size_t i = 0; i < mParty.size(); ++i) {
		party[i + 1] = mParty[i].get();
	}

	return party;
}

void GameMode::AddPartyMember(ActorType type) {
	std::unique_ptr<Actor> newActor = mActorFactory.CreateActor(type);
	// todo: deserialize party member data from stream
	mParty.push_back(std::move(newActor));
}

void GameMode::RemovePartyMember(ActorType type) {
	int idx = -1;
	for(size_t i = 0; i < mParty.size(); ++i) {
		if(mParty[i]->mActorType == type) {
			idx = i;
			break;
		}
	}

	if(idx > -1) {
		for(size_t i = idx; i < mParty.size() - 1; ++i) {
			mParty[i] = std::move(mParty[i + 1]);
		}
		mParty.pop_back();
	}

	// todo: serialize party member data from stream
}

const ActorFactory& GameMode::GetActorFactory() const {
	return mActorFactory;
}

const LevelManager& GameMode::GetLevelMan() const {
	return *mLevelManager;
}
