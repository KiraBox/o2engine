// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "blackboard.h"
#include "canvasUtils.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "fsm.h"
#include "gameModeAsciiRPG.h"
#include "level.h"
#include "levelUtils.h"
#include "utils.h"
#include "sharedConstants.h"

namespace {
	enum IntroStates {
		SkipIntroPrompt,
		IntroStory,
		Done,
		NumTypes
	};

	enum SkipIntroResult {
		Unknown,
		Yes,
		No
	};

	static FSM<IntroStates> mIntroFSM;
	static SkipIntroResult mSkipIntroResult;
	static bool mIsStateComplete[IntroStates::NumTypes];
	static bool mIntroReturnKeyPressed;

	// display test for skip intro/dialogue
	static CanvasUtils::Text mDisplayedText;
}

void SetUpStoryFSM(GameModeAsciiRPG& gameMode, LevelUtils::TextQueue& textQueue) {
	mSkipIntroResult = SkipIntroResult::Unknown;
	for (size_t i = 0; i < IntroStates::NumTypes; ++i) {
		mIsStateComplete[i] = false;
	}

	mIntroFSM.RegisterState(IntroStates::SkipIntroPrompt,
		[&textQueue](void*) {
			textQueue.PushText("Skip Intro? [{c0,1,0|YES}] or [{c1,0,0|NO}]");
			textQueue.Advance();
		},
		[](void*) {
			
		}, 
		nullptr
	);

	mIntroFSM.RegisterState(IntroStates::IntroStory, 
		[&textQueue](void*) {
			textQueue.PushText(Utils::Format("Long ago, {%s|HUMANS} and {%s|MONSTERS} shared the world", COLOR_GREEN.c_str(), COLOR_RED.c_str()));
			textQueue.PushText(Utils::Format("{%s|MAGIC} was a powerful tool, and they used it to make life better for everyone", COLOR_TURQUOISE.c_str()));
			textQueue.PushText(Utils::Format("One day a {%s|MAGE} appeared by the name of {%s|ZARREF}", COLOR_TURQUOISE.c_str(), COLOR_BLUE.c_str()));
			textQueue.PushText("He used his power to deceive everyone, and brought many allies to his side");
			textQueue.PushText(Utils::Format("{%s|ZARREF} took control of the kingdom of {%s|HUMANS} and enslaved all other races", COLOR_BLUE.c_str(), COLOR_GREEN.c_str()));
			textQueue.PushText(Utils::Format("Stealing the {%s|SOULS} and {%s|MANA} of living things, he sought greater power", COLOR_BLUE.c_str(), COLOR_BLUE.c_str()));
			textQueue.PushText(Utils::Format("But a resistance formed and a great {%s|WAR} was fought", COLOR_RED.c_str()));
			textQueue.PushText(Utils::Format("After years of conflict, {%s|ZARREF} was finally defeated", COLOR_RED_DARK.c_str()));
			textQueue.PushText(Utils::Format("But with his final breath, {%s|ZARREF} cursed himself, splintering his {%s|SOUL} and scattering it", COLOR_RED_DARK.c_str(), COLOR_BLUE.c_str()));
			textQueue.PushText("And as he faded away, he vowed to return");
			textQueue.PushText("Now, ages have passed, and much has been forgotten");
			textQueue.PushText(Utils::Format("Rumors of {%s|EVIL} once again spread across the land", COLOR_RED_DARK.c_str()));
			textQueue.PushText(Utils::Format("A new story begins"));
			textQueue.Advance();

			textQueue.UpdateAndRender(mDisplayedText, 0.0f);
			LevelUtils::GetPlayerInputText().EnableModification(false);
		},
		[](void*) {
			LevelUtils::GetPlayerInputText().EnableModification(true);
		},
		[&textQueue](void*, float) {
			if(mIntroReturnKeyPressed) {
				mIntroReturnKeyPressed = false;
				if(!textQueue.IsDone()) {
					textQueue.Advance();
				}
				else {
					mIsStateComplete[IntroStates::IntroStory] = textQueue.IsDone();
				}
			}
			else if(mDisplayedText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
			}
		}
	);

	mIntroFSM.RegisterState(IntroStates::Done,
		[&gameMode](void*) {
			EventBroadcaster::get().broadcast(LoadLevelEvent("status"));
		},
		nullptr, nullptr
	);

	mIntroFSM.AddEdge(IntroStates::SkipIntroPrompt, IntroStates::IntroStory, [&](void*) {
		return mSkipIntroResult == SkipIntroResult::No;
	});
	mIntroFSM.AddEdge(IntroStates::SkipIntroPrompt, IntroStates::Done, [&](void*) {
		return mSkipIntroResult == SkipIntroResult::Yes;
	});

	mIntroFSM.AddEdge(IntroStates::IntroStory, IntroStates::Done, [&](void*) {
		return mIsStateComplete[IntroStates::IntroStory];
	});

	mIntroFSM.Init(IntroStates::SkipIntroPrompt, nullptr);
}

void LevelIntro::InitImpl() {
	LevelUtils::InitLevelBorders();
	LevelUtils::InitPlayerInput(this, [&](std::string str) {
		const IntroStates state = mIntroFSM.GetState();
		if (state == IntroStates::SkipIntroPrompt) {
			str = Utils::ToLower(str);
			if (str == "yes") {
				mSkipIntroResult = SkipIntroResult::Yes;
			}
			else if (str == "no") {
				mSkipIntroResult = SkipIntroResult::No;
			}
		}
		else if (state == IntroStates::IntroStory) {
			mIntroReturnKeyPressed = true;
		}
	});

	SetUpStoryFSM(static_cast<GameModeAsciiRPG&>(mGameMode), mTextQueue);

	mDisplayedText.mPosX = SCREEN_CHARS_WIDTH / 2;
	mDisplayedText.mPosY = 5;
	mDisplayedText.SetDefaultDelay(0.05f);
	mDisplayedText.CenterX(true);
	mDisplayedText.SetMaxLength(DIALGOUE_MAX_CHARS_WIDTH);

	mIntroReturnKeyPressed = false;
}

void LevelIntro::UpdateImpl(float dt) {
	LevelUtils::UpdateLevelBorders();
	LevelUtils::UpdatePlayerInput(dt);

	mIntroFSM.Update(dt);

	mTextQueue.UpdateAndRender(mDisplayedText, dt);
}

void LevelIntro::DestroyImpl() {
	LevelUtils::DestroyPlayerInput(this);
}
