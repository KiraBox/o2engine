// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actorAction.h"
#include <memory>
#include <vector>

enum class BrainType {
	Dummy = 0,
	Random,
	AttackWeakest,
	NumTypes
};

class Brain {
public:
	explicit Brain(BrainType brainType) : mBrainType(brainType) {}
	virtual ~Brain() {}

	BrainType GetBrainType() const;
	virtual ActorActionInfo ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies);

private:
	BrainType mBrainType;
};

class DummyBrain : public Brain {
public:
	DummyBrain() : Brain(BrainType::Dummy) {}
	virtual ~DummyBrain() {}
};

class RandomBrain : public Brain {
public:
	RandomBrain() : Brain(BrainType::Random) {}
	virtual ~RandomBrain() {}

	virtual ActorActionInfo ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies) override;
};

class AttackWeakestBrain : public Brain {
public:
	AttackWeakestBrain() : Brain(BrainType::AttackWeakest) {}
	virtual ~AttackWeakestBrain() {}

	virtual ActorActionInfo ChooseAction(Actor* actor, const std::vector<Actor*>& enemies, const std::vector<Actor*>& allies) override;
};

class BrainFactory {
public:
	void RegisterBrain(std::unique_ptr<Brain> brain);
	Brain* GetBrain(BrainType brainType) const;

private:
	std::vector<std::unique_ptr<Brain>> mRegisteredBrains;
};
