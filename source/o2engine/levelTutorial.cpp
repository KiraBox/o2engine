// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "event.h"
#include "eventBroadcaster.h"
#include "level.h"

void LevelTutorial::Init() {

}

void LevelTutorial::Update(float dt) {
	EventBroadcaster::get().broadcast<LoadLevelEvent>(LoadLevelEvent("village"));
}

void LevelTutorial::Destroy() {

}
