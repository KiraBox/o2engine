// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

struct ActorActionInfo;
class Brain;
class DialogBrain;
namespace Utils { class Random; }

enum DamageType {
	None
};

enum class ActorType {
	// player and npcs
	Player,
	Min,

	// enemies
	Slime,

	NumTypes
};

static std::unordered_map<ActorType, std::string> ACTOR_TYPE_TO_NAME_MAP {
	{ ActorType::Player, "player" },
	{ ActorType::Min, "min" },
	{ ActorType::Slime, "slime" }
};

static std::unordered_map<std::string, ActorType> NAME_TO_ACTOR_TYPE_MAP {
	{ "player", ActorType::Player },
	{ "min", ActorType::Min },
	{ "slime", ActorType::Slime }
};

class Actor {
public:
	Actor();

	void SetHealth(int health);
	int GetHealth() const;

	void SetMaxHealth(int health, bool resetHealth = false);
	int GetMaxHealth() const;

	void Damage(int damage, DamageType type = DamageType::None);
	void Heal(int damage);

	int GetBasicAttackAmount(Utils::Random& random) const;

	std::string GetCombatText(const ActorActionInfo& info) const;

	std::string GetName() const;
	std::string GetNameAbbrv() const;

	std::unique_ptr<Actor> Clone() const;

	std::string mCustomName;
	ActorType mActorType;

	const int mActorId;

	//int mMagicPoints = 0;
	//int mSkillPoints = 0;
	int mAttack = 0;
	//int mDefense = 0;
	//int mEvasion = 0;
	int mSpeed = 0;
	//int mLuck = 0;

	Brain* mBrain = nullptr;
	DialogBrain* mDialogBrain = nullptr;

private:
	int mHealth = 0;
	int mMaxHealth = 0;

	// sometimes we do more, sometimes we do less
	float mBasicAttackDeviation = 0.6f;

	static int mUniqueActorId;
};

class ActorFactory {
public:
	void RegisterActor(std::unique_ptr<Actor> actor);
	std::unique_ptr<Actor> CreateActor(ActorType actorType) const;

private:
	std::vector<std::unique_ptr<Actor>> mRegisteredActors;
};
