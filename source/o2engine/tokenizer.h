// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <string>
#include <vector>

class Tokenizer {
public:
	void Tokenize(const std::string& str, const std::string& delimeters);
	const std::vector<std::string>& GetTokens() const;

private:
	std::vector<std::string> mTokens;
};
