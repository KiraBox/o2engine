// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "blackboard.h"
#include "common.h"
#include "utils.h"

void Blackboard::RegisterBooleanKey(unsigned int key, bool value) {
	O2ASSERT(mBooleanValues.find(key) == mBooleanValues.end());
	mBooleanValues[key] = value;
}

void Blackboard::SetBoolean(unsigned int key, bool value) {
	auto elem = mBooleanValues.find(key);
	if(elem != mBooleanValues.end()) {
		elem->second = value;
	}
	else {
		O2LOG(Utils::Format("Boolean key for '%i' not found", key).c_str());
	}
}

bool Blackboard::GetBoolean(unsigned int key) const {
	auto elem = mBooleanValues.find(key);
	if(elem != mBooleanValues.end()) {
		return elem->second;
	}
	else {
		O2LOG(Utils::Format("Boolean key for '%i' not found", key).c_str());
		return false;
	}
}
