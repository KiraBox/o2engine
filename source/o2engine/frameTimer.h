// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class FrameTimer {
public:
	void Update();

	float GetDeltaTime() const;
	float GetDeltaTimeSinceStart() const;
	float GetFrameRate() const;

private:
	void UpdateImpl();

	float mDeltaTime = 0.f;
	float mDeltaTimeSinceStart = 0.f;
	float mFrameRate = 0.f;
};
