// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "blackboard.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "scriptEnumsAsciiRPG.h"
#include "scriptNode.h"
#include "utils.h"

namespace {

static bool mAckReceived = false;
static bool mScriptEventAccepted = false;

struct ScriptAckResponseScope {
	ScriptAckResponseScope(void* owner)
		: mOwner(owner) {
		mAckReceived = false;
		mScriptEventAccepted = false;

		if(mOwner) {
			EventBroadcaster::get().addEventListener<ScriptEventAck>(mOwner, [&](const ScriptEventAck& _event) {
				mAckReceived = true;
				mScriptEventAccepted = _event.mAccepted;
			});
		}
	}

	~ScriptAckResponseScope() {
		if(mOwner) {
			EventBroadcaster::get().removeEventListener<ScriptEventAck>(mOwner);
		}
	}

	void* mOwner = nullptr;
};

}

bool ScriptNode::IsComplete() const {
	return mIsComplete;
}

void DialogueScriptNode::Tick(float dt, Blackboard& blackboard) {
	ScriptAckResponseScope scope(this);

	EventBroadcaster::get().broadcast(DialogueScriptEvent(mDialogue));
	O2ALOG(mAckReceived, "Level not registering to receive dialogue script events.");

	if(mAckReceived && mScriptEventAccepted) {
		mIsComplete = true;
	}
}

void BattleScriptNode::Tick(float dt, Blackboard& blackboard) {
	ScriptAckResponseScope scope(this);

	EventBroadcaster::get().broadcast(BattleScriptEvent(mEnemies));
	O2ALOG(mAckReceived, "Level not registering to receive battle script events.");

	if(mAckReceived && mScriptEventAccepted) {
		mIsComplete = true;
	}
}

void BlackboardScriptNode::Tick(float dt, Blackboard& blackboard) {
	ScriptAckResponseScope scope(this);

	EventBroadcaster::get().broadcast(BlackboardScriptEvent());
	O2ALOG(mAckReceived, "Level not registering to receive blackboard script events.");

	if(mAckReceived && mScriptEventAccepted) {
		auto it = NAME_TO_BB_BOOLEAN_KEY.find(mTargetKey);
		if(it != NAME_TO_BB_BOOLEAN_KEY.end()) {
			blackboard.SetBoolean(it->second, mValue);
			mIsComplete = true;
		}

		O2ALOG(mIsComplete, "Blackboard script operation did nothing.");
		mIsComplete = true;
	}
}

void AddRemovePartyMemberScriptNode::Tick(float dt, Blackboard& blackboard) {
	ScriptAckResponseScope scope(this);

	EventBroadcaster::get().broadcast(AddRemovePartyMemberScriptEvent(mIsAdd, mActorType));
	O2ALOG(mAckReceived, "Level not registering to receive blackboard script events.");

	if(mAckReceived && mScriptEventAccepted) {
		mIsComplete = true;
	}
}
