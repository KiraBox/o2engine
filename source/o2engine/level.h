// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actor.h"
#include "encounter.h"
#include "levelUtils.h"
#include <string>
#include "userInterface.h"

struct AddRemovePartyMemberScriptEvent;
struct BattleScriptEvent;
struct BlackboardScriptEvent;
struct DialogueScriptEvent;
class GameMode;

#define BASIC_LEVEL(x)							\
class x : public Level {						\
public:											\
	x(GameMode& gameMode)						\
		: Level(gameMode) {}					\
	virtual ~x() {}								\
	virtual void InitImpl() override;			\
	virtual void UpdateImpl(float dt) override;	\
	virtual void DestroyImpl() override;		\
};												\


class Level {
public:
	Level(GameMode& gameMode);
	virtual ~Level() {}

	void Init();
	void Update(float dt);
	void Destroy();

	virtual void InitImpl() = 0;
	virtual void UpdateImpl(float dt) = 0;
	virtual void DestroyImpl() = 0;

	void SetName(const std::string& name);
	const std::string& GetName() const;

private:
	void OnScriptDialogueEvent(const DialogueScriptEvent& _event);
	void OnScriptBattleEvent(const BattleScriptEvent& _event);
	void OnBlackboardScriptEvent(const BlackboardScriptEvent& _event);
	void OnAddRemovePartyMemberScriptEvent(const AddRemovePartyMemberScriptEvent& _event);
	void AcknowledgeScriptEvent(bool accepted) const;

protected:
	std::string mName;
	GameMode& mGameMode;

	LevelUtils::TextQueue mTextQueue;
	bool mReturnKeyPressed = false;
};

BASIC_LEVEL(LevelConsole);
BASIC_LEVEL(LevelIntro);
BASIC_LEVEL(LevelVillage);
BASIC_LEVEL(LevelStatus);

class LevelCombat : public Level {
public:
	LevelCombat(GameMode& gameMode);

	virtual ~LevelCombat() {

	}

	virtual void InitImpl() override;
	virtual void UpdateImpl(float dt) override;
	virtual void DestroyImpl() override;

private:
	std::unique_ptr<Encounter> mEncounter;
	std::vector<std::unique_ptr<Actor>> mCreatedActors;
};
