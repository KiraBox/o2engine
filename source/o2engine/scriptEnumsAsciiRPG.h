// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <unordered_map>
#include <string>

enum LevelScriptName {
	// story arc scripts
	Act0_MemoryOfGraduation = 0,

	// one-off scripts

	Count
};

enum BlackboardKeys {
	Act0_Complete = 0
};

static std::unordered_map<std::string, int> NAME_TO_BB_BOOLEAN_KEY {
	{ "act_0_complete", BlackboardKeys::Act0_Complete }
};
