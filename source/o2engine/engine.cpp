// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "audio.h"
#include "engine.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "eventQueue.h"
#include "frameTimer.h"
#include "gameModeAsciiRPG.h"
#include "graphics.h"
#include "input.h"
#include "SDL.h"
#include "SDL_syswm.h"
#include "SDLUtils.h"
#include "sharedConstants.h"
#include "textureManager.h"
#include "window.h"

void Engine::Shutdown() {
	mIsRunning = false;
}

void Engine::Run() {
	InitSDL();

	FrameTimer frameTimer;
	Window window;
	Input input;
	TextureManager textureMan;
	Graphics graphics(textureMan);
	Audio audio;

	// custom game mode
	std::unique_ptr<GameMode> gameMode = std::make_unique<GameModeAsciiRPG>(textureMan);

	// engine events
	EventBroadcaster::get().addEventListener<ExitGameEvent>(this, [&](const ExitGameEvent&) {
		Shutdown();
	});

	window.SetName("Rune Song");
	window.SetDimensions(SCREEN_WIDTH, SCREEN_HEIGHT);
	window.Init();

	textureMan.Init();
	graphics.Init(window);
	audio.Init();

	gameMode->Init();

	while (mIsRunning) {
		frameTimer.Update();
		const float dt = frameTimer.GetDeltaTime();

		UpdatePlatform();
		input.Update();

		gameMode->Update(dt);

		window.Update();
		graphics.Update();
		audio.Update(dt);
	}

	gameMode->Destroy();
	gameMode.reset();

	graphics.Destroy();
	textureMan.Destroy();
	window.Destroy();
	audio.Destroy();

	DestroySDL();

	EventQueue<PlatformInputEvent>::destroy();

	EventBroadcaster::destroy();
}

void Engine::UpdatePlatform() {
	SDL_Event _event;
	while (SDL_PollEvent(&_event) > 0) {
		switch (_event.type) {
		case SDL_QUIT:
			Shutdown();
			break;
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			EventQueue<PlatformInputEvent>::get().pushEvent(
				(_event.key.state == SDL_PRESSED),
				_event.key.keysym.sym
			);
			break;
		};
	}
}

void Engine::InitSDL() {
	SDLUtils::CheckError(SDL_InitSubSystem(SDL_INIT_VIDEO | SDL_INIT_AUDIO) >= 0);
}

void Engine::DestroySDL() {
	SDL_Quit();
}
