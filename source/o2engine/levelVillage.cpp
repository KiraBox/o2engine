// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "canvasUtils.h"
#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "fsm.h"
#include "gameMode.h"
#include "level.h"
#include "levelUtils.h"
#include "sentenceBuilder.h"
#include "sharedConstants.h"
#include "utils.h"

namespace {
	// borders
	static CanvasUtils::CanvasRect mTextBorders(1, 3 * CHAR_HEIGHT, SCREEN_CHARS_WIDTH - 1, 5);

	// locations
	static CanvasUtils::Text mInnText;
	static CanvasUtils::Text mCombatText;

	// location introduction text
	static CanvasUtils::Text mLocationIntroText[3];
	static Utils::Timer mLocationIntroTextDelayTimer(0.5f);
	static size_t mLocationIntroIdx = 0;

	static bool mReturnKeyPressed = false;

	enum VillageStates {
		Overview,
		DisplayIntroText,
		Inn,
		Combat,
		NumTypes
	};

	enum OverviewResult {
		Unknown,
		SelectInn,
		SelectCombat
	};

	static FSM<VillageStates> mVillageFSM;
	static OverviewResult mOverviewResult = OverviewResult::Unknown;

	void Reset() {
		mInnText.SetText("");
		mCombatText.SetText("");
		for(size_t i = 0; i < 3; ++i) {
			mLocationIntroText[i].SetText("");
		}
		mLocationIntroTextDelayTimer.Reset();
		mLocationIntroIdx = 0;
		mReturnKeyPressed = false;
		mOverviewResult = OverviewResult::Unknown;
		LevelUtils::GetPlayerInputText().Clear();
	}
}

bool isLocationIntroDone() {
	bool result = true;
	for (size_t i = 0; i < 3; ++i) {
		result = result && mLocationIntroText[i].IsDone();
	}
	return result;
}

void SetUpVillageFSM() {
	mVillageFSM.RegisterState(VillageStates::Overview,
		[&](void*) {
			mOverviewResult = OverviewResult::Unknown;
		}, 
		nullptr, nullptr
	);

	mVillageFSM.RegisterState(VillageStates::DisplayIntroText,
		[&](void*) {
			switch (mOverviewResult) {
				case OverviewResult::SelectInn:
					mLocationIntroText[0].SetText(SentenceBuilder("You walk towards the smell of {c0,1,0|(0)} soup and fried {c0,1,0|(1)s}", WordGroup::Vegetables, WordGroup::Vegetables).Build());
					mLocationIntroText[1].SetText(SentenceBuilder("A {c1,0,0|cheery} innkeeper smiles as you approach").Build());
					mLocationIntroText[2].SetText(SentenceBuilder("A mug of spiced {c0.8,1,0.2|(0)} is placed before you", WordGroup::Beverages).Build());
					break;
				case OverviewResult::SelectCombat:
					mLocationIntroText[0].SetText(SentenceBuilder("With pep in your step, you walk towards the edge of town").Build());
					mLocationIntroText[1].SetText(SentenceBuilder("What could possibly go wrong?").Build());
					mLocationIntroText[2].SetText("");
					break;
				default:
					break;
			};
		},
		[&](void*) {
			CanvasUtils::PlayerTextInput& playerInput = LevelUtils::GetPlayerInputText();
			if (!playerInput.IsModificationEnabled()) {
				playerInput.EnableModification(true);
			}
			playerInput.Clear();
		},
		[&](void*, float dt) {
			if (isLocationIntroDone()) {
				for (size_t i = 0; i < 3; ++i) {
					mLocationIntroText[i].Render();
				}
				CanvasUtils::PlayerTextInput& playerInput = LevelUtils::GetPlayerInputText();
				playerInput.SetText(CONTINUE_STRING);
				playerInput.EnableModification(false);
				return;
			}
			else if(mReturnKeyPressed) {
				mReturnKeyPressed = false;
				for (size_t i = 0; i < 3; ++i) {
					mLocationIntroText[i].Complete();
				}
				CanvasUtils::PlayerTextInput& playerInput = LevelUtils::GetPlayerInputText();
				playerInput.SetText(CONTINUE_STRING);
				playerInput.EnableModification(false);
				return;
			}

			for (size_t i = 0; i <= mLocationIntroIdx; ++i) {
				mLocationIntroText[i].UpdateAndRender(dt);
			}

			if (mLocationIntroText[mLocationIntroIdx].IsDone() && mLocationIntroTextDelayTimer.Done(dt)) {
				mLocationIntroTextDelayTimer.Reset();
				++mLocationIntroIdx;
			}
		}
	);

	mVillageFSM.RegisterState(VillageStates::Inn,
		[&](void*) {
			::Reset();

			EventBroadcaster::get().broadcast<LoadLevelEvent>(LoadLevelEvent("village"));
		},
		nullptr, nullptr
	);

	mVillageFSM.RegisterState(VillageStates::Combat,
		[&](void*) {
			::Reset();

			std::unique_ptr<LevelTransitionInfo> info = std::make_unique<LevelTransitionInfo>();
			info->mPrevLevel = "village";
			info->mEnemies.push_back(ActorType::Slime);

			EventBroadcaster::get().broadcast<LoadLevelEvent>(LoadLevelEvent("combat", std::move(info)));
		},
		nullptr, nullptr
	);

	mVillageFSM.AddEdge(VillageStates::Overview, VillageStates::DisplayIntroText, [&](void*) {
		return mOverviewResult != OverviewResult::Unknown;
	});

	mVillageFSM.AddEdge(VillageStates::DisplayIntroText, VillageStates::Inn, [&](void*) {
		return mOverviewResult == OverviewResult::SelectInn && isLocationIntroDone() && mReturnKeyPressed;
	});

	mVillageFSM.AddEdge(VillageStates::DisplayIntroText, VillageStates::Combat, [&](void*) {
		return mOverviewResult == OverviewResult::SelectCombat && isLocationIntroDone() && mReturnKeyPressed;
	});

	mVillageFSM.Init(VillageStates::Overview, nullptr);
}

void LevelVillage::InitImpl() {
	SetUpVillageFSM();

	LevelUtils::InitLevelBorders();
	LevelUtils::InitPlayerInput(this, [&](std::string str) {
		if (mVillageFSM.GetState() == VillageStates::Overview) {
			str == Utils::ToLower(str);
			if (str == "1") {
				mOverviewResult = OverviewResult::SelectInn;
			}
			else if (str == "2") {
				mOverviewResult = OverviewResult::SelectCombat;
			}
		}
		else if (mVillageFSM.GetState() == VillageStates::DisplayIntroText) {
			mReturnKeyPressed = true;
		}
	});

	// borders
	mTextBorders.SetCharLine(0, 4, SCREEN_CHARS_WIDTH - 2, 4, '-');

	// locations
	mInnText.mPosX = 2;
	mInnText.mPosY = 6;
	mInnText.SetText("[{c0.8,0.4,0.2|1}] Inn");

	mCombatText.mPosX = 2;
	mCombatText.mPosY = 5;
	mCombatText.SetText("[{c1,0.2,0.2|2}] Combat");

	// location introduction text
	for (size_t i = 0; i < 3; ++i) {
		mLocationIntroText[i].mPosY = 13.0f - float(i * 2);
		mLocationIntroText[i].SetDefaultDelay(0.04f);
		mLocationIntroText[i].CenterX(true);
	}
	mLocationIntroIdx = 0;
	mLocationIntroTextDelayTimer.Reset();

	mReturnKeyPressed = false;
}

void LevelVillage::UpdateImpl(float dt) {
	mVillageFSM.Update(dt);

	LevelUtils::UpdateLevelBorders();
	LevelUtils::UpdatePlayerInput(dt);

	mTextBorders.Render();

	mInnText.UpdateAndRender(dt);
	mCombatText.UpdateAndRender(dt);
}

void LevelVillage::DestroyImpl() {
	LevelUtils::DestroyPlayerInput(this);
}
