// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "scriptEnumsAsciiRPG.h"
#include "scriptNode.h"
#include <string>
#include <vector>
#include <unordered_map>

class Blackboard;

class LevelScript {
public:
	void Add(std::unique_ptr<ScriptNode> node);

	void Tick(float dt, Blackboard& blackboard);
	bool IsComplete() const;

private:
	std::vector<std::unique_ptr<ScriptNode> > mScript;
	size_t mScriptIndex = 0;
};

class LevelScriptFactory {
public:
	void RegisterScript(LevelScriptName levelScriptName, const std::string& filename);
	std::unique_ptr<LevelScript> CreateScript(LevelScriptName levelScriptName);

private:
	std::unique_ptr<BattleScriptNode> CreateBattleScriptNode(const std::vector<std::string>& tokens, size_t& idx);
	std::unique_ptr<BlackboardScriptNode> CreateBlackboardScriptNode(const std::vector<std::string>& tokens, size_t& idx);
	std::unique_ptr<AddRemovePartyMemberScriptNode> CreateAddRemovePartyMemberScriptNode(bool isAdd, const std::vector<std::string>& tokens, size_t& idx);

	std::unordered_map<LevelScriptName, std::string> mLevelScriptToFilenameMap;
};
