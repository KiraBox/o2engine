// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "canvasUtils.h"
#include "common.h"
#include "encounter.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "fsm.h"
#include "gameMode.h"
#include "gameModeAsciiRPG.h"
#include "level.h"
#include "levelUtils.h"
#include "sharedConstants.h"
#include "utils.h"

namespace {
	// borders
	static const float mOptionsHeight = 8.0f;
	static CanvasUtils::CanvasRect mOptionsBorders(1, 4 * CHAR_HEIGHT, SCREEN_CHARS_WIDTH - 1, 5);

	// dialog
	static CanvasUtils::Text mBattleCombatText;
	static std::vector<std::string> mBattleDialogText;

	// selection text
	const int mSelectionRows = 4;
	static CanvasUtils::Text mSelectionTextLHS[mSelectionRows];
	static CanvasUtils::Text mSelectionTextRHS[mSelectionRows];

	// stats text
	const int mStatRows = 4;
	static CanvasUtils::Text mPartyStatsText[mStatRows];
	static CanvasUtils::Text mEnemyStatsText[mStatRows];

	// turn order text
	static CanvasUtils::Text mTurnOrderText;

	enum class CombatStates {
		Intro,
		CombatTurn,
		CombatIdle,
		CombatResult,
		Lose,
		Win,
		NumTypes
	};

	static FSM<CombatStates> mCombatFSM;
	static Actor* mCurrentTurnActor = nullptr;

	enum class SelectionStates {
		BaseOptions,
		Idle,
		SelectTarget,
		NumTypes
	};

	enum class BaseOptionsResult {
		Unknown,
		Attack
	};

	static FSM<SelectionStates> mSelectionFSM;
	static BaseOptionsResult mBaseOptionsResult = BaseOptionsResult::Unknown;

	static std::vector<Actor*> mSelectionTargetActors;
	static Actor* mTargetActor = nullptr;

	// input
	static bool mCombatReturnKeyPressed = false;

	static std::string mPrevLevel;

	void Reset() {
		mBattleCombatText.SetText("");
		mBattleDialogText.clear();
		for(size_t i = 0; i < mSelectionRows; ++i) {
			mSelectionTextLHS[i].SetText("");
			mSelectionTextRHS[i].SetText("");
		}
		for(size_t i = 0; i < mStatRows; ++i) {
			mPartyStatsText[i].SetText("");
			mEnemyStatsText[i].SetText("");
		}
		mTurnOrderText.SetText("");
		mSelectionTargetActors.clear();
		mTargetActor = nullptr;
		mCombatReturnKeyPressed = false;
		mPrevLevel.clear();
		LevelUtils::GetPlayerInputText().Clear();
	}
}

void WriteStats(const Actor& actor, CanvasUtils::Text& text) {
	float delta = static_cast<float>(actor.GetHealth()) / static_cast<float>(actor.GetMaxHealth());

	float redVal = 1.0f;
	float greenVal = 1.0f;
	float blueVal = 1.0f;

	if(actor.GetHealth() < actor.GetMaxHealth()) {
		redVal = 1.0f - delta;
		greenVal = delta;
		blueVal = 0.0f;
	}

	std::string health = Utils::Format("{c%f,%f,%f|%i}", redVal, greenVal, blueVal, actor.GetHealth());

	std::string name = actor.GetNameAbbrv();
	name = Utils::ToUpper(name);

	if(!actor.mCustomName.empty()) {
		size_t idx = actor.mCustomName.find_last_of(' ');
		if(idx != std::string::npos && ++idx < actor.mCustomName.size()) {
			name.pop_back();
			name += Utils::Format("{%s|%c}", COLOR_GRAY.c_str(), toupper(actor.mCustomName[idx]));
		}
	}

	text.SetText(Utils::Format("[%s] HP%s", name.c_str(), health.c_str()));
}

void UpdateStats(Encounter* encounter) {
	auto writeStatsHelper = [](const std::vector<Actor*>& actors, CanvasUtils::Text* text, size_t numText){
		for(size_t i = 0; i < actors.size() && i < numText; ++i) {
			WriteStats(*actors[i], text[i]);
		}
	};

	writeStatsHelper(encounter->GetPartyMembers(), mPartyStatsText, mStatRows);
	writeStatsHelper(encounter->GetEnemies(), mEnemyStatsText, mStatRows);
}

void UpdateTurnOrder(const Encounter& encounter) {
	std::vector<Actor*> turnOrder;
	encounter.GetTurnOrder(turnOrder);

	std::string str;
	for(Actor* actor : turnOrder) {
		const std::string* nameColor = &COLOR_WHITE;
		if(str.empty()) {
			nameColor = &COLOR_YELLOW;
		}
		str += Utils::Format("{%s|%s}", nameColor->c_str(), Utils::ToUpper(actor->mCustomName).c_str());
		str += Utils::Format("{%s| -> }", COLOR_GRAY_DARK.c_str());
	}
	mTurnOrderText.SetText(str);
}

void ClearSelectionText() {
	for (size_t i = 0; i < mSelectionRows; ++i) {
		mSelectionTextLHS[i].SetText("");
		mSelectionTextRHS[i].SetText("");
	}
}

void SetUpCombatFSM(Encounter* _encounter, LevelUtils::TextQueue& textQueue) {
	mCombatFSM.RegisterState(CombatStates::Intro,
		[&](void* data) {
			Encounter* encounter = static_cast<Encounter*>(data);
			textQueue.PushText("It's time for battle!");
			textQueue.Advance();
			UpdateTurnOrder(*encounter);
		},
		[&](void*) {
			mCombatReturnKeyPressed = false;
			LevelUtils::GetPlayerInputText().Clear();
			LevelUtils::GetPlayerInputText().EnableModification(true);
			textQueue.Advance();
		},
		[&](void*, float dt) {
			if(mCombatReturnKeyPressed && !mBattleCombatText.IsDone()) {
				mCombatReturnKeyPressed = false;
				textQueue.Advance();
			}
			if(mBattleCombatText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
				LevelUtils::GetPlayerInputText().EnableModification(false);
			}
		}
		);

	mCombatFSM.RegisterState(CombatStates::CombatTurn,
		[&](void* data) {
			Encounter* encounter = static_cast<Encounter*>(data);
			UpdateStats(encounter);

			// ai turn
			if(mCurrentTurnActor == nullptr) {
				mCombatFSM.ForceState(CombatStates::Lose);
			}
			else if (mCurrentTurnActor->mBrain) {
				mCurrentTurnActor = encounter->Tick();
				textQueue.Advance();
				mCombatFSM.ForceState(CombatStates::CombatIdle);
			}
			else {
				mSelectionFSM.ForceState(SelectionStates::BaseOptions);
			}
		},
		nullptr,
		[&](void* data, float dt) {
			// player turn
			if (mSelectionFSM.GetState() == SelectionStates::Idle) {
				ActorActionInfo action;
				action.mActor = mCurrentTurnActor;
				action.mTargetActor = mTargetActor;

				switch (mBaseOptionsResult) {
					case BaseOptionsResult::Attack:
						action.mActionType = ActorActionType::Attack;
						action.mData.mAmount = action.mActor->GetBasicAttackAmount(Utils::GLOBAL_RANDOM);
						break;
					default:
						O2ALOG(false, "unsupported player action type");
				}

				Encounter* encounter = static_cast<Encounter*>(data);
				mCurrentTurnActor = encounter->TickPlayer(action);
				textQueue.Advance();

				mCombatFSM.ForceState(CombatStates::CombatIdle);
			}
		}
	);

	mCombatFSM.RegisterState(CombatStates::CombatIdle,
		[&](void* data) {
			LevelUtils::GetPlayerInputText().EnableModification(false);
		}, 
		[&](void* data) {
			mCombatReturnKeyPressed = false;
			LevelUtils::GetPlayerInputText().SetText("");
			LevelUtils::GetPlayerInputText().EnableModification(true);

			Encounter* encounter = static_cast<Encounter*>(data);
			UpdateTurnOrder(*encounter);
		},
		[&](void* data, float dt) {
			if(mCombatReturnKeyPressed && !mBattleCombatText.IsDone()) {
				mCombatReturnKeyPressed = false;
				textQueue.Advance();
			}
			if(mBattleCombatText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
			}
		}
	);

	mCombatFSM.RegisterState(CombatStates::CombatResult,
		[&](void* data) {
			LevelUtils::GetPlayerInputText().EnableModification(false);

			for(size_t i = 0; i < mBattleDialogText.size(); ++i) {
				textQueue.PushText(mBattleDialogText[i]);
			}
			mBattleDialogText.clear();

			textQueue.Advance();

			Encounter* encounter = static_cast<Encounter*>(data);
			UpdateStats(encounter);
		},
		[&](void* data) {
			mCombatReturnKeyPressed = false;
			LevelUtils::GetPlayerInputText().SetText("");
			LevelUtils::GetPlayerInputText().EnableModification(true);
		},
		[&](void* data, float dt) {
			if(mCombatReturnKeyPressed && !textQueue.IsDone()) {
				mCombatReturnKeyPressed = false;
				textQueue.Advance();
			}
			if(mBattleCombatText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
			}
		}
	);

	mCombatFSM.RegisterState(CombatStates::Lose,
		[&](void* data) {
			UpdateStats(static_cast<Encounter*>(data));
			textQueue.PushText("{c1,0,0;d0.1|G A M E   O V E R}");
			textQueue.Advance();
		},
		nullptr,
		[&](void* data, float dt) {
			if(mCombatReturnKeyPressed && !mBattleCombatText.IsDone()) {
				mCombatReturnKeyPressed = false;
				textQueue.Advance();
			}
			if(mBattleCombatText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
				if(mCombatReturnKeyPressed) {
					EventBroadcaster::get().broadcast(LoadLevelEvent("title"));
					::Reset();
				}
			}
		}
	);

	mCombatFSM.RegisterState(CombatStates::Win, 
		[&](void* data) {
			UpdateStats(static_cast<Encounter*>(data));
			textQueue.PushText("{c0,1,0;d0.1|V I C T O R Y}");
			textQueue.Advance();
		}, 
		nullptr, 
		[&](void* data, float dt) {
			if(mCombatReturnKeyPressed && !mBattleCombatText.IsDone()) {
				mCombatReturnKeyPressed = false;
				textQueue.Advance();
			}
			if(mBattleCombatText.IsDone()) {
				LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
				if(mCombatReturnKeyPressed) {
					EventBroadcaster::get().broadcast(LoadLevelEvent(mPrevLevel));
					::Reset();
				}
			}
		}
	);

	mCombatFSM.AddEdge(CombatStates::Intro, CombatStates::CombatTurn, [&](void*) {
		return mCombatReturnKeyPressed && textQueue.IsDone();
	});
	mCombatFSM.AddEdge(CombatStates::CombatIdle, CombatStates::Lose, [&](void* data) {
		return mCombatReturnKeyPressed && textQueue.IsDone() && static_cast<Encounter*>(data)->GetEncounterCompletionState() == EncounterCompletionState::Loss;
	});
	mCombatFSM.AddEdge(CombatStates::CombatIdle, CombatStates::Win, [&](void* data) {
		return mCombatReturnKeyPressed && textQueue.IsDone() && static_cast<Encounter*>(data)->GetEncounterCompletionState() == EncounterCompletionState::Victory;
	});
	mCombatFSM.AddEdge(CombatStates::CombatIdle, CombatStates::CombatResult, [&](void* data) {
		return mCombatReturnKeyPressed && textQueue.IsDone() && !mBattleDialogText.empty();
	});
	mCombatFSM.AddEdge(CombatStates::CombatIdle, CombatStates::CombatTurn, [&](void* data) {
		return mCombatReturnKeyPressed && textQueue.IsDone();
	});
	mCombatFSM.AddEdge(CombatStates::CombatResult, CombatStates::CombatTurn, [&](void* data) {
		return mCombatReturnKeyPressed && textQueue.IsDone();
	});

	mCombatFSM.Init(CombatStates::Intro, _encounter);
}

void SetUpSelectionFSM(Encounter* encounter) {
	mBaseOptionsResult = BaseOptionsResult::Unknown;

	mSelectionFSM.RegisterState(SelectionStates::BaseOptions, 
		[&](void*) {
			mBaseOptionsResult = BaseOptionsResult::Unknown;
			mSelectionTextLHS[0].SetText(Utils::Format("[{%s|1}] Attack", COLOR_GREEN.c_str()));
		},
		nullptr, nullptr
	);

	mSelectionFSM.RegisterState(SelectionStates::Idle,
		[&](void*) {
			ClearSelectionText();
		},
		nullptr, nullptr
	);

	mSelectionFSM.RegisterState(SelectionStates::SelectTarget,
		[&](void* data) {
			ClearSelectionText();

			mSelectionTargetActors.clear();
			mTargetActor = nullptr;

			Encounter* encounter = static_cast<Encounter*>(data);
			int count = 0;

			const std::vector<Actor*>& enemies = encounter->GetEnemies();
			O2ALOG(enemies.size() <= mSelectionRows, "The player needs more targets!");
			for (size_t i = 0; i < enemies.size() && i < mSelectionRows; ++i) {
				const Actor* enemy = enemies[i];
				if(enemy->GetHealth() > 0) {
					mSelectionTextLHS[i].SetText(Utils::Format("[{%s|%i}] {%s|%s}", COLOR_GREEN.c_str(), count + 1, "c1,0.8,0.8", Utils::Capitalize(enemies[i]->mCustomName).c_str()));
				}
				else {
					mSelectionTextLHS[i].SetText(Utils::Format("[{%s|X}] {%s|%s}", COLOR_RED.c_str(), COLOR_RED.c_str(), Utils::Capitalize(enemies[i]->mCustomName).c_str()));
				}
				++count;

				mSelectionTargetActors.push_back(enemies[i]);
			}

			const std::vector<Actor*>& partyMembers = encounter->GetPartyMembers();
			O2ALOG(partyMembers.size() <= mSelectionRows, "The player needs more targets!");
			for (size_t i = 0; i < partyMembers.size() && i < mSelectionRows; ++i) {
				mSelectionTextRHS[i].SetText(Utils::Format("[{%s|%i}] {%s|%s}", COLOR_BLUE.c_str(), count + 1, "c0.8,0.8,1", Utils::Capitalize(partyMembers[i]->mCustomName).c_str()));
				++count;

				mSelectionTargetActors.push_back(partyMembers[i]);
			}
		},
		nullptr, nullptr
	);

	mSelectionFSM.AddEdge(SelectionStates::BaseOptions, SelectionStates::SelectTarget, [&](void*) {
		return mBaseOptionsResult == BaseOptionsResult::Attack;
	});
	mSelectionFSM.AddEdge(SelectionStates::SelectTarget, SelectionStates::Idle, [&](void*) {
		return mTargetActor != nullptr;
	});

	mSelectionFSM.Init(SelectionStates::BaseOptions, encounter);
}

LevelCombat::LevelCombat(GameMode& gameMode)
	: Level(gameMode) {

}

void LevelCombat::InitImpl() {
	EventBroadcaster::get().addEventListener<DisplayAsciiTextureEvent>(this, [&](const DisplayAsciiTextureEvent& _event) {
		if(const Utils::AsciiImage* image = static_cast<GameModeAsciiRPG&>(mGameMode).GetAsciiImage(_event.mName)) {
			LevelUtils::DisplayImage(*image);
		}
	});

	LevelUtils::InitLevelBorders();
	LevelUtils::InitPlayerInput(this, [&](std::string str) {
		if (mCombatFSM.GetState() == CombatStates::Intro) {
			mCombatReturnKeyPressed = true;
		}
		else if(mCombatFSM.GetState() == CombatStates::CombatTurn) {
			if(mSelectionFSM.GetState() == SelectionStates::BaseOptions) {
				str = Utils::ToLower(str);
				if(str == "1") {
					mBaseOptionsResult = BaseOptionsResult::Attack;
				}
			}
			else if(mSelectionFSM.GetState() == SelectionStates::SelectTarget) {
				int num = -1;
				if(Utils::ToNumber(str, num) && num > 0 && num <= static_cast<int>(mSelectionTargetActors.size())) {
					if(mSelectionTargetActors[num - 1]->GetHealth() > 0) {
						mTargetActor = mSelectionTargetActors[num - 1];
					}
				}
			}
		}
		else if(mCombatFSM.GetState() == CombatStates::CombatIdle || mCombatFSM.GetState() == CombatStates::CombatResult) {
			mCombatReturnKeyPressed = true;
		}
		else if(mCombatFSM.GetState() == CombatStates::Lose) {
			if(mTextQueue.IsDone()) {
				mCombatReturnKeyPressed = true;
			}
		}
		else if(mCombatFSM.GetState() == CombatStates::Win) {
			if(mTextQueue.IsDone()) {
				mCombatReturnKeyPressed = true;
			}
		}
	});

	// borders
	mOptionsBorders.SetCharLine(0, 4, SCREEN_CHARS_WIDTH - 2, 4, '-');

	// dialog text
	mBattleCombatText.mPosX = SCREEN_CHARS_WIDTH / 2;
	mBattleCombatText.mPosY = mOptionsHeight + 1.0f;
	mBattleCombatText.SetDefaultDelay(0.04f);
	mBattleCombatText.CenterX(true);

	// selection text
	for (size_t i = 0; i < mSelectionRows; ++i) {
		mSelectionTextLHS[i].mPosX = 2;
		mSelectionTextLHS[i].mPosY = mOptionsHeight - 1.0f - (1.0f * i);
		mSelectionTextLHS[i].SetMaxLength(18);

		mSelectionTextRHS[i].mPosX = 21;
		mSelectionTextRHS[i].mPosY = mOptionsHeight - 1.0f - (1.0f * i);
		mSelectionTextRHS[i].SetMaxLength(18);
	}

	// stats text
	for(size_t i = 0; i < mStatRows; ++i) {
		mPartyStatsText[i].mPosX = 40;
		mPartyStatsText[i].mPosY = mOptionsHeight - 1.0f - (1.0f * i);
		mPartyStatsText[i].SetMaxLength(18);

		mEnemyStatsText[i].mPosX = 60;
		mEnemyStatsText[i].mPosY = mOptionsHeight - 1.0f - (1.0f * i);
		mEnemyStatsText[i].SetMaxLength(18);
	}

	// turn order text
	mTurnOrderText.mPosX = 2;
	mTurnOrderText.SetMaxLength(76, false);
	mTurnOrderText.mPosY = 3;

	// encounter initialization
	if (const LevelTransitionInfo* levelTransitionInfo = mGameMode.GetLevelMan().GetLevelTransitionInfo()) {
		mEncounter = std::make_unique<Encounter>();

		std::vector<Actor*> party = mGameMode.GetParty();
		for(Actor* partyMember : party) {
			mEncounter->AddPartyMember(partyMember);
		}

		for (ActorType actorType : levelTransitionInfo->mEnemies) {
			std::unique_ptr<Actor> enemy = mGameMode.GetActorFactory().CreateActor(actorType);
			mEncounter->AddEnemy(enemy.get());
			mCreatedActors.push_back(std::move(enemy));
		}

		mPrevLevel = levelTransitionInfo->mPrevLevel;
	}
	else {
		O2ALOG(levelTransitionInfo != nullptr, "Entering a combat level with no transitional information.");
	}

	mEncounter->RegisterOnEncounterCombatText([&](const std::string& str) {
		mTextQueue.PushText(str);
	});

	mEncounter->RegisterOnEncounterDialog([&](const std::string& str) {
		mBattleDialogText.push_back(str);
	});

	mCurrentTurnActor = mEncounter->Start();

	SetUpCombatFSM(mEncounter.get(), mTextQueue);
	SetUpSelectionFSM(mEncounter.get());

	EventBroadcaster::get().broadcast(DisplayAsciiTextureEvent("test"));
}

void LevelCombat::UpdateImpl(float dt) {
	LevelUtils::UpdateLevelBorders();
	LevelUtils::UpdateImage();
	LevelUtils::UpdatePlayerInput(dt);

	mOptionsBorders.Render();
	mTextQueue.UpdateAndRender(mBattleCombatText, dt);

	for(size_t i = 0; i < mStatRows; ++i) {
		mPartyStatsText[i].UpdateAndRender(dt);
		mEnemyStatsText[i].UpdateAndRender(dt);
	}

	for (size_t i = 0; i < mSelectionRows; ++i) {
		mSelectionTextLHS[i].UpdateAndRender(dt);
		mSelectionTextRHS[i].UpdateAndRender(dt);
	}

	mTurnOrderText.UpdateAndRender(dt);

	mCombatFSM.Update(dt);
	mSelectionFSM.Update(dt);
}

void LevelCombat::DestroyImpl() {
	EventBroadcaster::get().removeEventListener<DisplayAsciiTextureEvent>(this);

	LevelUtils::DestroyPlayerInput(this);
}
