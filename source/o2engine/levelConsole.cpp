// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "event.h"
#include "eventQueue.h"
#include "gameMode.h"
#include <iostream>
#include "level.h"
#include "soundType.h"

void LevelConsole::InitImpl() {
	mGameMode.GetUI_DEPRECATED().LoadCanvas("title");

	//EventQueue<LoadSound>::get().pushEvent(LoadSound("data/sounds/th06_10.wav", "music", SoundType::Music));
	//EventQueue<PlaySoundEvent>::get().pushEvent(PlaySoundEvent("music", true));
}

void LevelConsole::UpdateImpl(float dt) {

}

void LevelConsole::DestroyImpl() {
	mGameMode.GetUI_DEPRECATED().UnloadCanvas("title");

	//EventQueue<UnloadSound>::get().pushEvent(UnloadSound("music"));
}
