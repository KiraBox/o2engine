// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

enum class SoundType {
	Music = 0,
	Effect,
	NumTypes
};
