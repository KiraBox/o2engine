// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "textureAsciiConverter.h"
#include "utils.h"

#include <algorithm>

void TextureAsciiConverter::FillAsciiPalette(const Utils::Image& image, unsigned int numCharsWidth, unsigned int numCharsHeight, unsigned int charWidth, unsigned int charHeight) {
	if(image.mWidth < numCharsWidth * charWidth || image.mHeight < numCharsHeight * charHeight) {
		return;
	}

	const unsigned int numEntries = numCharsWidth * numCharsHeight;
	for(unsigned int i = 0; i < numEntries; ++i) {
		AsciiPaletteEntry entry;
		entry.mWidth = charWidth;
		entry.mHeight = charHeight;
		entry.mMask.resize(charWidth * charHeight, false);
		entry.mCode = i;

		mAsciiPalette.push_back(entry);
	}

	auto analyzeChar = [&](unsigned int x, unsigned int y) {
		AsciiPaletteEntry& entry = mAsciiPalette[x + numCharsWidth * y];

		unsigned int topIdx = static_cast<unsigned int>(y * charHeight);
		unsigned int leftIdx = static_cast<unsigned int>(x * charWidth);
		unsigned int botIdx = static_cast<unsigned int>((y + 1) * charHeight);
		unsigned int rightIdx = static_cast<unsigned int>((x + 1) * charWidth);

		for(unsigned int x = leftIdx; x < rightIdx; ++x) {
			for(unsigned int y = topIdx; y < botIdx; ++y) {
				const Utils::Image::PixelInfo& info = image.mPixelInfo[x + image.mWidth * y];
				if(IsNotBlack(info.mColor)) {
					entry.mMask[(x - leftIdx) + charWidth * (y - topIdx)] = true;
				}
			}
		}
	};

	for(unsigned int x = 0; x < numCharsWidth; ++x) {
		for(unsigned int y = 0; y < numCharsHeight; ++y) {
			analyzeChar(x, y);
		}
	}
}

bool TextureAsciiConverter::Convert(const Utils::Image& image, unsigned int width, unsigned int height, Utils::AsciiImage& outImage) const {
	if(width == 0 || height == 0) {
		return false;
	}

	if(image.mWidth < width || image.mHeight < height) {
		// not enough information to create an image
		return false;
	}

	// resize and clear the previous image structure
	outImage.mWidth = width;
	outImage.mHeight = height;
	outImage.mCharInfo.resize(width * height, {});

	// calculate the dimensions of a character cell
	const float cellWidth = float(image.mWidth) / float(width);
	const float cellHeight = float(image.mHeight) / float(height);
	const float invCellWidth = 1.0f / cellWidth;
	const float invCellHeight = 1.0f / cellHeight;

	auto findChar = [&](unsigned int x, unsigned int y) {
		Utils::AsciiImage::CharInfo info;

		unsigned int topIdx = static_cast<unsigned int>(y * cellHeight);
		unsigned int leftIdx = static_cast<unsigned int>(x * cellWidth);
		unsigned int botIdx = static_cast<unsigned int>((y + 1) * cellHeight);
		unsigned int rightIdx = static_cast<unsigned int>((x + 1) * cellWidth);

		Utils::Color colorAccum;
		unsigned int numColoredPixels = 0;

		// average the color
		{
			for(unsigned int x = leftIdx; x < rightIdx; ++x) {
				for(unsigned int y = topIdx; y < botIdx; ++y) {
					const Utils::Image::PixelInfo& info = image.mPixelInfo[x + image.mWidth * y];

					colorAccum.mR += info.mColor.mR;
					colorAccum.mG += info.mColor.mG;
					colorAccum.mB += info.mColor.mB;

					if(IsNotBlack(info.mColor)) {
						++numColoredPixels;
					}
				}
			}

			if(numColoredPixels > 0) {
				colorAccum.mR /= numColoredPixels;
				colorAccum.mG /= numColoredPixels;
				colorAccum.mB /= numColoredPixels;
			}

			info.mColor = colorAccum;
		}

		int closestMatchCode = -1;
		int closestMatchPixels = -1;

		// find a close-matching character
		{
			for(const AsciiPaletteEntry& entry : mAsciiPalette) {
				int numMatchingPixels = 0;

				for(unsigned int x = leftIdx; x < rightIdx; ++x) {
					for(unsigned int y = topIdx; y < botIdx; ++y) {
						const Utils::Image::PixelInfo& info = image.mPixelInfo[x + image.mWidth * y];

						float xVal = static_cast<float>(x - leftIdx) * invCellWidth;
						float yVal = static_cast<float>(y - topIdx) * invCellHeight;

						const bool isNotBlack = IsNotBlack(info.mColor);
						const bool isPixelSet = IsPixelSetInPalette(entry, xVal, yVal);
						if((isNotBlack && isPixelSet) || (!isNotBlack && !isPixelSet)) {
							++numMatchingPixels;
						}
					}
				}

				if(numMatchingPixels > closestMatchPixels) {
					closestMatchPixels = numMatchingPixels;
					closestMatchCode = entry.mCode;
				}
			}
		}

		O2ASSERT(closestMatchCode > -1);
		if(closestMatchCode > -1) {
			info.mCode = closestMatchCode;
		}

		return info;
	};

	// for each character to calculate
	for(unsigned int x = 0; x < width; ++x) {
		for(unsigned int y = 0; y < height; ++y) {
			Utils::AsciiImage::CharInfo& cellInfo = outImage.mCharInfo[x + width * y];
			cellInfo = findChar(x, y);
		}
	}

	return true;
}

const bool TextureAsciiConverter::IsPixelSetInPalette(const AsciiPaletteEntry& entry, float x, float y) const {
	unsigned int pixelX = static_cast<unsigned int>(entry.mWidth * x);
	unsigned int pixelY = static_cast<unsigned int>(entry.mHeight * y);
	return entry.mMask[pixelX + entry.mWidth * pixelY];
}
