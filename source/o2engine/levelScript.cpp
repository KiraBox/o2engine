// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "actor.h"
#include "common.h"
#include "fileSystem.h"
#include "levelScript.h"
#include "tokenizer.h"
#include "utils.h"

void LevelScript::Add(std::unique_ptr<ScriptNode> node) {
	mScript.push_back(std::move(node));
}

void LevelScript::Tick(float dt, Blackboard& blackboard) {
	if(mScriptIndex >= mScript.size()) {
		return;
	}

	mScript[mScriptIndex]->Tick(dt, blackboard);
	if(mScript[mScriptIndex]->IsComplete()) {
		++mScriptIndex;
	}
}

bool LevelScript::IsComplete() const {
	return mScriptIndex >= mScript.size();
}

void LevelScriptFactory::RegisterScript(LevelScriptName levelScriptName, const std::string& filename) {
	O2ASSERT(mLevelScriptToFilenameMap.find(levelScriptName) == mLevelScriptToFilenameMap.end());
	if(mLevelScriptToFilenameMap.find(levelScriptName) == mLevelScriptToFilenameMap.end()) {
		mLevelScriptToFilenameMap[levelScriptName] = filename;
	}
}

std::unique_ptr<LevelScript> LevelScriptFactory::CreateScript(LevelScriptName levelScriptName) {
	auto it = mLevelScriptToFilenameMap.find(levelScriptName);
	if(it != mLevelScriptToFilenameMap.end()) {
		FileSystem::FileObject scriptFile;
		if(FileSystem::ReadTextFile(it->second, "rt", scriptFile)) {
			Tokenizer tok;
			tok.Tokenize(scriptFile.mStringStream.str(), ";\n\r\t");

			std::unique_ptr<LevelScript> levelScript = std::make_unique<LevelScript>();

			for(size_t i = 0; i < tok.GetTokens().size() - 1; ++i) {
				const std::string& command = tok.GetTokens()[i];
				if(command == "COMMENT") {
					// ignore comments
					++i;
				}
				else if(command == "DIALOGUE") {
					levelScript->Add(std::make_unique<DialogueScriptNode>(tok.GetTokens()[i+1]));
					++i;
				}
				else if(command == "BATTLE") {
					levelScript->Add(CreateBattleScriptNode(tok.GetTokens(), i));
				}
				else if(command == "ITEM") {
					O2LOG("script ITM unimplemented");
				}
				else if(command == "BLACKBOARD") {
					levelScript->Add(CreateBlackboardScriptNode(tok.GetTokens(), i));
				}
				else if(command == "ADDPARTY") {
					levelScript->Add(CreateAddRemovePartyMemberScriptNode(true, tok.GetTokens(), i));
				}
				else if(command == "REMPARTY") {
					levelScript->Add(CreateAddRemovePartyMemberScriptNode(false, tok.GetTokens(), i));
				}
				else {
					O2LOG("Unrecognized script command: %s", command.c_str());
				}
			}

			return std::move(levelScript);
		}
	}

	O2ALOG(false, "Level script not found.");
	return nullptr;
}

std::unique_ptr<BattleScriptNode> LevelScriptFactory::CreateBattleScriptNode(const std::vector<std::string>& tokens, size_t& idx) {
	std::unique_ptr<BattleScriptNode> node = std::make_unique<BattleScriptNode>();

	int numEnemies = atoi(tokens[++idx].c_str());
	for(int i = 0; i < numEnemies; ++i) {
		std::string actorType = Utils::ToLower(tokens[++idx]);

		auto it = NAME_TO_ACTOR_TYPE_MAP.find(actorType);
		if(it != NAME_TO_ACTOR_TYPE_MAP.end()) {
			node->mEnemies.push_back(it->second);
		}
		else {
			O2FAIL();
		}
	}

	return std::move(node);
}

std::unique_ptr<BlackboardScriptNode> LevelScriptFactory::CreateBlackboardScriptNode(const std::vector<std::string>& tokens, size_t& idx) {
	std::unique_ptr<BlackboardScriptNode> node = std::make_unique<BlackboardScriptNode>();

	node->mTargetKey = tokens[++idx];

	std::string opType = Utils::ToLower(tokens[++idx]);
	if(opType == "assign") {
		node->mOpType = BlackboardScriptNode::Assign;
	}
	else {
		O2FAIL();
	}

	node->mValue = atoi(tokens[++idx].c_str());

	return std::move(node);
}

std::unique_ptr<AddRemovePartyMemberScriptNode> LevelScriptFactory::CreateAddRemovePartyMemberScriptNode(bool isAdd, const std::vector<std::string>& tokens, size_t& idx) {
	std::unique_ptr<AddRemovePartyMemberScriptNode> node = std::make_unique<AddRemovePartyMemberScriptNode>();

	node->mIsAdd = isAdd;

	std::string actorType = Utils::ToLower(tokens[++idx]);
	auto it = NAME_TO_ACTOR_TYPE_MAP.find(actorType);
	if(it != NAME_TO_ACTOR_TYPE_MAP.end()) {
		node->mActorType = it->second;
	}
	else {
		O2FAIL();
	}

	return std::move(node);
}
