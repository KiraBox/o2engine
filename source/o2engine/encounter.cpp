// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "actor.h"
#include "brain.h"
#include "common.h"
#include "dialogBrain.h"
#include "encounter.h"
#include "utils.h"

namespace {
	const int NUM_ACTIVE_TURNS = 10;
}

void Encounter::AddPartyMember(Actor* actor) {
	if (actor) {
		mPartyMembers.push_back(actor);
	}
}

void Encounter::AddEnemy(Actor* actor) {
	if (actor) {
		mEnemies.push_back(actor);
	}
}

Actor* Encounter::Start() {
	O2ASSERT(!mPartyMembers.empty() && !mEnemies.empty());

	// give enemies custom names. Slime A, Slime B, etc..
	std::vector<int> typeNumCount;
	typeNumCount.resize(static_cast<size_t>(ActorType::NumTypes), 0);

	for (auto& enemy : mEnemies) {
		std::string enemyName;
		const ActorType actorType = enemy->mActorType;
		enemyName = ACTOR_TYPE_TO_NAME_MAP[actorType];
		enemyName += " ";
		enemyName += ('A' + typeNumCount[static_cast<size_t>(actorType)]++);
		enemy->mCustomName = enemyName;
	}

	UpdateActionQueue();

	return GetActor();
}

Actor* Encounter::Tick() {
	Actor* actor = GetActor();
	O2ALOG(actor->mBrain, "We should never reach this function without a brain");

	std::vector<Actor*> partyMembers;
	std::vector<Actor*> enemies;

	if(IsEnemy(actor)) {
		for(Actor* partyMember : mPartyMembers) {
			if(partyMember->GetHealth() > 0) {
				enemies.push_back(partyMember);
			}
		}
		partyMembers.assign(mEnemies.begin(), mEnemies.end());
	}
	else {
		for(Actor* enemy : mEnemies) {
			if(enemy->GetHealth() > 0) {
				enemies.push_back(enemy);
			}
		}
		partyMembers.assign(mPartyMembers.begin(), mPartyMembers.end());
	}

	if(Brain* brain = actor->mBrain) {
		ActorActionInfo action = brain->ChooseAction(actor, enemies, partyMembers);
		DoActorAction(action);
	}

	return GetActor();
}

Actor* Encounter::TickPlayer(const ActorActionInfo& action) {
	DoActorAction(action);

	return GetActor();
}

void Encounter::DoActorAction(const ActorActionInfo& action) {
	if(mEncounterCompletionState != EncounterCompletionState::Unknown) {
		return;
	}

	if(mOnEncounterCombatText) {
		mOnEncounterCombatText(action.mActor->GetCombatText(action));
	}

	if(action.mActionType == ActorActionType::Attack) {
		Actor& targetActor = *action.mTargetActor;
		bool isFriendlyFire = AreAllies(action.mActor, action.mTargetActor);

		targetActor.Damage(action.mData.mAmount);
		if(targetActor.mDialogBrain && mOnEncounterDialog) {
			std::vector<std::string> dialog = targetActor.mDialogBrain->GetHurtText(isFriendlyFire);
			for(auto& text : dialog) {
				mOnEncounterDialog(text);
			}
		}
		if(targetActor.GetHealth() <= 0) {
			if(mOnEncounterDialog) {
				if(targetActor.mDialogBrain) {
					std::vector<std::string> dialog = targetActor.mDialogBrain->GetDefeatedText(isFriendlyFire);
					for(auto& text : dialog) {
						mOnEncounterDialog(text);
					}
				}
				mOnEncounterDialog(Utils::Format("%s was defeated!", Utils::Capitalize(targetActor.mCustomName).c_str()));
			}
			RemoveActor(targetActor);
		}
	}

	bool isLoss = true;
	for(Actor* partyMember : mPartyMembers) {
		if(partyMember && partyMember->GetHealth() > 0) {
			isLoss = false;
			break;
		}
	}

	if(isLoss) {
		mEncounterCompletionState = EncounterCompletionState::Loss;
		return;
	}

	bool isVictory = true;
	for(Actor* enemy : mEnemies) {
		if(enemy && enemy->GetHealth() > 0) {
			isVictory = false;
			break;
		}
	}

	if(isVictory) {
		mEncounterCompletionState = EncounterCompletionState::Victory;
	}

	Advance();
}

void Encounter::UpdateActionQueue() {
	auto addActorTurn = [&](Actor* actor) {
		if (actor && actor->GetHealth() > 0) {
			int initiative = 0;
			for (int i = 0; i < NUM_ACTIVE_TURNS; ++i) {
				initiative += actor->mSpeed;
				mActionQueue.push_back({ actor, initiative });
			}
		}
	};

	if (mActionQueue.empty()) {
		for (Actor* actor : mPartyMembers) {
			addActorTurn(actor);
		}
		for (Actor* actor : mEnemies) {
			addActorTurn(actor);
		}

		SortByInitiative();
	}
}

void Encounter::SortByInitiative() {
	mActionQueue.sort([&](const ActorInitiativeInfo& lhs, const ActorInitiativeInfo& rhs) {
		return lhs.mInitiative < rhs.mInitiative;
	});
}

bool Encounter::IsEnemy(const Actor* actor) const {
	if (actor) {
		for (size_t i = 0; i < mEnemies.size(); ++i) {
			if (actor == mEnemies[i]) {
				return true;
			}
		}
	}
	return false;
}

bool Encounter::AreAllies(const Actor* actorA, const Actor* actorB) const {
	bool isEnemyA = IsEnemy(actorA);
	bool isEnemyB = IsEnemy(actorB);
	return (isEnemyA && isEnemyB) || (!isEnemyA && !isEnemyB);
}

Actor* Encounter::GetActor() const {
	if (mActionQueue.empty()) {
		return nullptr;
	}
	return mActionQueue.front().mActor;
}

void Encounter::SetRandom(Utils::Random& random) {
	mRandom = &random;
}

const std::vector<Actor*>& Encounter::GetEnemies() const {
	return mEnemies;
}

const std::vector<Actor*>& Encounter::GetPartyMembers() const {
	return mPartyMembers;
}

void Encounter::GetTurnOrder(std::vector<Actor*>& outTurnOrder) const {
	outTurnOrder.resize(mActionQueue.size());

	int idx = 0;
	for(const ActorInitiativeInfo& info : mActionQueue) {
		outTurnOrder[idx++] = info.mActor;
	}
}

EncounterCompletionState Encounter::GetEncounterCompletionState() const {
	return mEncounterCompletionState;
}

void Encounter::Advance() {
	if (mActionQueue.empty()) {
		return;
	}

	ActorInitiativeInfo info = mActionQueue.front();
	mActionQueue.pop_front();

	for (auto& it : mActionQueue) {
		it.mInitiative -= info.mInitiative;
	}

	// find the last initiative for the actor that took the turn
	int lastInitiative = -1;
	for (auto& rit = mActionQueue.rbegin(); rit != mActionQueue.rend(); ++rit) {
		if (info.mActor->mActorId == rit->mActor->mActorId) {
			lastInitiative = rit->mInitiative;
			break;
		}
	}

	O2ASSERT(lastInitiative > -1);
	if (lastInitiative > -1) {
		mActionQueue.push_back({ info.mActor, lastInitiative + info.mActor->mSpeed });
	}

	SortByInitiative();
}

void Encounter::RemoveActor(const Actor& actor) {
	if(mActionQueue.empty()) {
		return;
	}

	for(auto it = mActionQueue.begin(); it != mActionQueue.end();) {
		if(it->mActor->mActorId == actor.mActorId) {
			it = mActionQueue.erase(it);
		}
		else {
			++it;
		}
	}
}

void Encounter::RegisterOnEncounterCombatText(std::function<void(const std::string & str)> callback) {
	mOnEncounterCombatText = callback;
}

void Encounter::RegisterOnEncounterDialog(std::function<void(const std::string & str)> callback) {
	mOnEncounterDialog = callback;
}
