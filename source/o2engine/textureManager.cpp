// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "event.h"
#include "eventBroadcaster.h"
#include "GL/glew.h"
#include <map>
#include "SDL.h"
#include "SDLUtils.h"
#include "textureManager.h"
#include "utils.h"

namespace {
	struct TextureData {
		GLuint mId = 0;
		SDL_Surface* mSurface = nullptr;
	};

	typedef std::map<std::string, TextureData> TextureMap;
	TextureMap mTextures;
}

void TextureManager::LoadBMPImpl(const std::string& name, const std::string& filepath) {
	if (!filepath.empty()) {
		TextureData textureData;
		textureData.mSurface = SDL_LoadBMP(filepath.c_str());
		if (SDLUtils::CheckError(textureData.mSurface != nullptr) == false) {
			auto elem = mTextures.find(name);
			O2ALOG(elem == mTextures.end(), "Loading duplicate texture.");
			auto res = mTextures.insert_or_assign(name, textureData);
			if (res.second) {
				const SDL_Surface& surface = *res.first->second.mSurface;
				GLuint& id = res.first->second.mId;

				glGenTextures(1, &id);
				glBindTexture(GL_TEXTURE_2D, id);

				// when texture area is small, bilinear filter the closest mipmap
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

				// when texture area is large, bilinear filter the original
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				// the texture wraps over at the edges
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				// create the texture
				O2ALOG(surface.format->format == SDL_PIXELFORMAT_BGR24, "Unsupported pixel format!");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, surface.w, surface.h, 0, GL_BGR, GL_UNSIGNED_BYTE, surface.pixels);
			}
		}
	}
}

void TextureManager::ClearImpl() {
	for (auto& elem : mTextures) {
		SDL_FreeSurface(elem.second.mSurface);
	}
	mTextures.clear();
}

bool TextureManager::UseTextureImpl(const std::string& name, TextureInfo& outInfo) const {
	auto elem = mTextures.find(name);
	if (elem != mTextures.end()) {
		glBindTexture(GL_TEXTURE_2D, elem->second.mId);
		outInfo.mWidth = elem->second.mSurface->w;
		outInfo.mHeight = elem->second.mSurface->h;
		return true;
	}
	else {
		O2LOG("Texture '%s' not found.", name.c_str());
		return false;
	}
}

void TextureManager::Init() {
	EventBroadcaster::get().addEventListener<LoadTextureEvent>(this, [&](const LoadTextureEvent& _event) {
		LoadBMP(_event.mName, _event.mRelativePath);
	});
}

void TextureManager::Destroy() {
	EventBroadcaster::get().removeEventListener<LoadTextureEvent>(this);
	Clear();
}

void TextureManager::LoadBMP(const std::string& name, const std::string& filepath) {
	LoadBMPImpl(name, filepath);
}

void TextureManager::Clear() {
	ClearImpl();
}

bool TextureManager::UseTexture(const std::string& name, TextureInfo& outInfo) const {
	return UseTextureImpl(name, outInfo);
}

bool TextureManager::GetTexture(const std::string& name, Utils::Image& outImage) const {
	auto elem = mTextures.find(name);
	if(elem != mTextures.end()) {
		if(elem->second.mSurface->format->format != SDL_PIXELFORMAT_BGR24) {
			return false;
		}

		outImage.mWidth = elem->second.mSurface->w;
		outImage.mHeight = elem->second.mSurface->h;
		
		outImage.mPixelInfo.resize(outImage.mWidth * outImage.mHeight);

		for(unsigned int w = 0; w < outImage.mWidth; ++w) {
			for(unsigned int h = 0; h < outImage.mHeight; ++h) {
				const size_t index = w + (outImage.mWidth * h);
				const unsigned int pixelIndex = h * elem->second.mSurface->pitch + w * elem->second.mSurface->format->BytesPerPixel;
				const uint8_t& blue = static_cast<uint8_t*>(elem->second.mSurface->pixels)[pixelIndex];
				const uint8_t& green = static_cast<uint8_t*>(elem->second.mSurface->pixels)[pixelIndex + 1];
				const uint8_t& red = static_cast<uint8_t*>(elem->second.mSurface->pixels)[pixelIndex + 2];

				outImage.mPixelInfo[index].mColor.mB = static_cast<float>(blue) / 255.0f;				
				outImage.mPixelInfo[index].mColor.mG = static_cast<float>(green) / 255.0f;
				outImage.mPixelInfo[index].mColor.mR = static_cast<float>(red) / 255.0f;
			}
		}
	}

	return true;
}
