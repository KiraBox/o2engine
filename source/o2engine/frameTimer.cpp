// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "frameTimer.h"

#if defined(_WIN32)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <timeapi.h>

#pragma comment(lib, "winmm.lib")	// timeGetTime

namespace {
	LARGE_INTEGER mFrequency;
	LARGE_INTEGER mPrevTicks;
	LARGE_INTEGER mCurrTicks;
	DWORD mPrevTime;
	DWORD mCurrTime;
}

void FrameTimer::UpdateImpl() {
	static const bool isHiResAvailable = QueryPerformanceFrequency(&mFrequency);
	
	{
		static bool isInitialized = false;
		if (!isInitialized) {
			if (isHiResAvailable) {
				QueryPerformanceCounter(&mPrevTicks);
			}
			else {
				mPrevTime = timeGetTime();
			}
			isInitialized = true;
		}
	}

	if (isHiResAvailable) {
		static const double invFrequency = 1.0 / static_cast<double>(mFrequency.QuadPart);

		QueryPerformanceCounter(&mCurrTicks);
		mDeltaTime = static_cast<float>(static_cast<double>(mCurrTicks.QuadPart - mPrevTicks.QuadPart) * invFrequency);
		mPrevTicks = mCurrTicks;
	}
	else {
		mCurrTime = timeGetTime();
		mDeltaTime = static_cast<float>(mCurrTime - mPrevTime) * 0.001f;
		mPrevTime = mCurrTime;
	}
}

#endif

// todo: add unimplemented errors for linux and mac os, potentially just use ctime

void FrameTimer::Update() {
	UpdateImpl();

	mDeltaTimeSinceStart += mDeltaTime;

	if (mDeltaTime > 0.f) {
		mFrameRate = 1.f / mDeltaTime;
	}
}

float FrameTimer::GetDeltaTime() const {
	return mDeltaTime;
}

float FrameTimer::GetDeltaTimeSinceStart() const {
	return mDeltaTimeSinceStart;
}

float FrameTimer::GetFrameRate() const {
	return mFrameRate;
}
