// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <string>

namespace Utils {
	struct Image;
}

class TextureManager {
public:
	struct TextureInfo {
		int mWidth;
		int mHeight;
	};

	void Init();
	void Destroy();

	void LoadBMP(const std::string& name, const std::string& filepath);
	void Clear();

	bool UseTexture(const std::string& name, TextureInfo& outInfo) const;
	bool GetTexture(const std::string& name, Utils::Image& outImage) const;

private:
	void LoadBMPImpl(const std::string& name, const std::string& filepath);
	void ClearImpl();

	bool UseTextureImpl(const std::string& name, TextureInfo& outInfo) const;
};
