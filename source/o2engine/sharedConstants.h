// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

// width-height in pixels
constexpr int CHAR_WIDTH = 9;
constexpr int CHAR_HEIGHT = 16;

constexpr int NUM_CHARS_WIDTH_ATLAS = 32;
constexpr int NUM_CHARS_HEIGHT_ATLAS = 8;

constexpr int SCREEN_CHARS_WIDTH = 80;
constexpr int SCREEN_CHARS_HEIGHT = 25;

constexpr int SCREEN_WIDTH = SCREEN_CHARS_WIDTH * CHAR_WIDTH;
constexpr int SCREEN_HEIGHT = SCREEN_CHARS_HEIGHT * CHAR_HEIGHT;

typedef class GameModeAsciiRPG O2_GAME_MODE;

const std::string COLOR_BLUE = "c0,0,1";
const std::string COLOR_BLUE_LIGHT = "c0.25,0.65,0.9";
const std::string COLOR_TURQUOISE = "c0,0.7,0.7";
const std::string COLOR_GRAY = "c0.5,0.5,0.5";
const std::string COLOR_GRAY_DARK = "c0.3,0.3,0.3";
const std::string COLOR_GREEN = "c0,1,0";
const std::string COLOR_RED = "c1,0,0";
const std::string COLOR_RED_DARK = "c0.6,0.1,0.1";
const std::string COLOR_WHITE = "c1,1,1";
const std::string COLOR_YELLOW = "c1,1,0";
const std::string COLOR_PURPLE = "c0.5,0,0.5";

const std::string CONTINUE_STRING = "Press enter to continue ...";

const int DIALGOUE_MAX_CHARS_WIDTH = 70;
