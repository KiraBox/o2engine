// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actor.h"
#include "brain.h"
#include "dialogBrain.h"
#include "levelManager.h"
#include "levelScript.h"
#include "userInterface.h"

class GameMode {
public:
	void Init();
	void Update(float dt);
	void Destroy();

	UserInterface& GetUI_DEPRECATED();

	Actor& GetPlayer();
	std::vector<Actor*> GetParty();
	void AddPartyMember(ActorType type);
	void RemovePartyMember(ActorType type);

	const ActorFactory& GetActorFactory() const;
	const LevelManager& GetLevelMan() const;

private:
	virtual void InitImpl() = 0;
	virtual void UpdateImpl(float dt) = 0;
	virtual void DestroyImpl() = 0;

protected:
	std::unique_ptr<LevelManager> mLevelManager;
	std::unique_ptr<UserInterface> mUI;

	Actor mPlayer;
	std::vector<std::unique_ptr<Actor>> mParty;

	ActorFactory mActorFactory;
	BrainFactory mBrainFactory;
	DialogBrainFactory mDialogBrainFactory;
};
