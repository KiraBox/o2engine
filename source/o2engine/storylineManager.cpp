// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "storylineManager.h"

StorylineManager::StorylineManager(LevelScriptFactory& scriptFactory, Blackboard& blackboard)
	: mScriptFactory(scriptFactory)
	, mBlackboard(blackboard) {

}

void StorylineManager::AddScript(LevelScriptName levelScript, ConditionDelegate condition) {
	O2ASSERT(mConditionDelegates[levelScript] == nullptr);
	mConditionDelegates[levelScript] = condition;
}

void StorylineManager::Tick(float dt) {
	if(mIsPaused) {
		return;
	}

	if(!mActiveScript) {
		for(size_t i = 0; i < LevelScriptName::Count; ++i) {
			if(mConditionDelegates[i](mBlackboard)) {
				if(mActiveScript) {
					O2ALOG(false, "Several game scripts have overlapping entry points.");
				}
				else {
					mActiveScript = mScriptFactory.CreateScript(static_cast<LevelScriptName>(i));
				}
			}
		}
	}

	if(mActiveScript) {
		mActiveScript->Tick(dt, mBlackboard);
		if(mActiveScript->IsComplete()) {
			mActiveScript = nullptr;
		}
	}
}

void StorylineManager::Pause(bool state) {
	mIsPaused = state;
}

bool StorylineManager::IsActiveScript() const {
	return mActiveScript != nullptr;
}
