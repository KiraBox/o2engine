// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <vector>

namespace Utils {
	struct AsciiImage;
	struct Image;
}

class TextureAsciiConverter {
public:
	void FillAsciiPalette(const Utils::Image& image, unsigned int numCharsWidth, unsigned int numCharsHeight, unsigned int charWidth, unsigned int charHeight);
	bool Convert(const Utils::Image& image, unsigned int width, unsigned int height, Utils::AsciiImage& outImage) const;

private:
	struct AsciiPaletteEntry {
		unsigned int mWidth = 0;
		unsigned int mHeight = 0;
		std::vector<bool> mMask;
		unsigned int mCode = 0;
	};

	const bool IsPixelSetInPalette(const AsciiPaletteEntry& entry, float x, float y) const;

	std::vector<AsciiPaletteEntry> mAsciiPalette;
};
