// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#define UNUSED(x) (void)x;

#if !defined(_DEBUG)

#define O2LOG(...)
#define O2FAIL()
#define O2ASSERT(x)
#define O2ALOG(x, ...)

#else

#include <cassert>
#include <stdarg.h>

#define O2LOG(...) printf(__VA_ARGS__); printf("\n");
#define O2FAIL() O2ASSERT(false);
#define O2ASSERT(x) assert(x);
#define O2ALOG(x, ...) if(!(x)) { O2LOG(__VA_ARGS__); O2ASSERT(false); }

#endif
