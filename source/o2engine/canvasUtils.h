// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <functional>
#include <string>
#include <vector>

enum class EKeyboard : unsigned int;
namespace Utils { struct AsciiImage; }

namespace CanvasUtils {

void initAtlas();

struct TextEffectInfo {
	unsigned char mCharacter;

	// color effect
	float mColor[3] = { 255.f, 255.f, 255.f };

	// typewriter text
	float mDelaySeconds = 0.f;
};

class Text {
public:
	void Render();
	void Update(float dt);
	void UpdateAndRender(float dt);

	void SetText(const std::string& text);
	const std::string& GetText() const;

	void Complete();
	bool IsDone() const;

	void SetDefaultDelay(float delaySeconds);

	void DisableFormatting();
	void EnableFormatting();

	void SetMaxLength(int maxLength, bool carry = true);
	int GetMaxLength() const;

	void CenterX(bool state);
	void CenterY(bool state);

public:
	float mPosX = 0.f;
	float mPosY = 0.f;

protected:
	std::string mOriginalText;
	std::vector<std::vector<TextEffectInfo>> mText;
	size_t mNumCharsInText;

	bool mFormattingEnabled = true;

	float mDelayCounter = 0.f;
	float mDefaultDelaySeconds = 0.f;
	size_t mDelayIdx = 0;
	size_t mDelayLineIdx = 0;
	size_t mDelaySubLineIdx = 0;

	int mMaxLength = -1;
	bool mShouldCarry = true;

	bool mCenterX = false;
	bool mCenterY = false;
};

class PlayerTextInput {
public:
	PlayerTextInput(Text& text, unsigned int width);

	void Update(float dt);

	void Append(unsigned char c);
	void Remove();
	void Clear();

	void OnKeyPressed(EKeyboard key);
	void OnKeyReleased(EKeyboard key);

	bool IsShiftHeld() const;

	void RegisterOnReturn(std::function<void(std::string)> callback);

	void SetText(const std::string& str);

	bool IsModificationEnabled() const;
	void EnableModification(bool state);

private:
	unsigned int mWidth;
	Text& mText;

	unsigned char mCursor;
	float mDelaySeconds;
	float mDelayAccum;
	bool mCursorToggle;

	bool mIsShiftHeld[2];
	bool mIsBackspaceHeld;

	float mBackspaceDelaySeconds[2];
	float mBackspaceDelayAccum[2];

	std::function<void(std::string)> mOnReturnCallback;

	bool mModificationEnabled = true;
};

class CanvasRect {
public:
	CanvasRect(float posX, float posY, unsigned int charsWidth, unsigned int charsHeight);

	void Render();

	void SetChar(int idxX, int idxY, unsigned char c);
	void SetCharLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, unsigned char c);
	void SetStringLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, const std::string& str);
	void SetImageCentered(const Utils::AsciiImage& image);

private:
	void _SetCharLine(int startIdxX, int startIdxY, int endIdxX, int endIdxY, std::function<unsigned char()> getCharFunc);

	float mPosX;
	float mPosY;
	unsigned int mWidth;
	unsigned int mHeight;

	std::vector<std::vector<TextEffectInfo>> mCharInfo;
};

}
