// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <functional>
#include <unordered_map>

template <class StateType>
class FSM {
public:
	typedef std::function<void(void*)> StateTransitionFunc;
	typedef std::function<void(void*, float dt)> StateFunc;
	typedef std::function<bool(void*)> EdgeCondition;

	struct State {
		StateTransitionFunc mOnEnter;
		StateTransitionFunc mOnExit;
		StateFunc mUpdate;
	};

	struct Edge {
		StateType mTo;
		EdgeCondition mCondition;
	};

	void RegisterState(StateType type, std::function<void(void*)> onEnter, std::function<void(void*)> onExit, std::function<void(void*, float dt)> onUpdate) {
		mStates[type] = { onEnter, onExit, onUpdate };
	}

	void AddEdge(StateType from, StateType to, std::function<bool(void*)> condition) {
		mEdges[from].push_back({ to, condition });
	}

	void Init(StateType type, void* userData) {
		mUserData = userData;
		mCurrentState = StateType::NumTypes;
		mHasNextState = true;
		mNextState = type;
	}

	void ForceState(StateType type) {
		mHasNextState = true;
		mNextState = type;
		Update(0.0f);
	}

	void Update(float dt) {
		if (mHasNextState) {
			auto& elem = mStates.find(mCurrentState);
			if (elem != mStates.end() && elem->second.mOnExit != nullptr) {
				elem->second.mOnExit(mUserData);
			}

			// advance to the next state
			mCurrentState = mNextState;
			mHasNextState = false;

			elem = mStates.find(mCurrentState);
			if (elem != mStates.end() && elem->second.mOnEnter != nullptr) {
				elem->second.mOnEnter(mUserData);
			}
		}

		{
			// update the current state
			auto& elem = mStates.find(mCurrentState);
			if (elem != mStates.end() && elem->second.mUpdate != nullptr) {
				elem->second.mUpdate(mUserData, dt);
			}
		}

		// check edges
		auto& elem = mEdges.find(mCurrentState);
		if (elem != mEdges.end()) {
			const std::vector<Edge>& edges = elem->second;
			for (size_t i = 0; i < edges.size(); ++i) {
				if (edges[i].mCondition(mUserData)) {
					mNextState = edges[i].mTo;
					mHasNextState = true;
					break;
				}
			}
		}
	}

	StateType GetState() const {
		return mCurrentState;
	}

private:
	std::unordered_map<StateType, State> mStates;
	std::unordered_map<StateType, std::vector<Edge> > mEdges;

	StateType mInitialState;
	StateType mCurrentState;

	bool mHasNextState = false;
	StateType mNextState;

	void* mUserData = nullptr;
};
