// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "soundType.h"
#include <string>
#include <vector>

enum class ActorType;
enum class EKeyboard : unsigned int;
namespace Utils { struct AsciiImage; };

struct PlatformInputEvent {
	bool mKeyDown = false;
	unsigned int mKeycode = 0;
	PlatformInputEvent(bool keyDown, unsigned int keycode) :
		mKeyDown(keyDown), mKeycode(keycode) {}
};

struct KeyPressedEvent {
	EKeyboard mKey;
	KeyPressedEvent(EKeyboard key) :
		mKey(key) {}
};

struct KeyReleasedEvent {
	EKeyboard mKey;
	KeyReleasedEvent(EKeyboard key) :
		mKey(key) {}
};

struct LoadTextureEvent {
	std::string mName;
	std::string mRelativePath;
	LoadTextureEvent(const std::string& name, const std::string relativePath)
		: mName(name)
		, mRelativePath(relativePath) {}
};

struct ConvertTextureEvent {
	std::string mName;
	unsigned int mWidth;
	unsigned int mHeight;
	ConvertTextureEvent(const std::string& name, unsigned int width, unsigned int height)
		: mName(name)
		, mWidth(width)
		, mHeight(height) {}
};

struct DisplayAsciiTextureEvent {
	std::string mName;
	DisplayAsciiTextureEvent(const std::string& name)
		: mName(name) {}
};

struct LevelTransitionInfo {
	LevelTransitionInfo() {}
	LevelTransitionInfo(const LevelTransitionInfo& info) {
		mPrevLevel = info.mPrevLevel;
		mEnemies.assign(info.mEnemies.begin(), info.mEnemies.end());
	}

	std::string mPrevLevel;
	std::vector<ActorType> mEnemies;
};


struct LoadLevelEvent {
	std::string mName;
	std::unique_ptr<LevelTransitionInfo> mLevelTransitionInfo;
	LoadLevelEvent(const std::string& name, std::unique_ptr<LevelTransitionInfo> transitionInfo = nullptr)
		: mName(name)
		, mLevelTransitionInfo(std::move(transitionInfo)) {}
};

struct ExitGameEvent {};

struct Render2DQuadEvent {
	std::string mTextureName;
	int mPosX = 0;
	int mPosY = 0;
	unsigned int mScaleX = 1;
	unsigned int mScaleY = 1;
	bool mCentered = false;
	bool mAtlassed = false;
	float mTopLeftU = 0.0f;
	float mTopLeftV = 0.0f;
	float mBotRightU = 1.0f;
	float mBotRightV = 1.0f;
	float mRGB[3] = { 1.0f, 1.0f, 1.0f };

	Render2DQuadEvent(const std::string& name) :
		mTextureName(name) {}

	Render2DQuadEvent& position(int x, int y) {
		mPosX = x;
		mPosY = y;
		return *this;
	}

	Render2DQuadEvent& scale(unsigned int x, unsigned int y) {
		mScaleX = x;
		mScaleY = y;
		return *this;
	}

	Render2DQuadEvent& centered() {
		mCentered = true;
		return *this;
	}

	Render2DQuadEvent& topLeft(float u, float v) {
		mTopLeftU = u;
		mTopLeftV = v;
		return *this;
	}

	Render2DQuadEvent& botRight(float u, float v) {
		mBotRightU = u;
		mBotRightV = v;
		return *this;
	}

	Render2DQuadEvent& atlassed() {
		mAtlassed = true;
		return *this;
	}

	Render2DQuadEvent& color(float r, float g, float b) {
		mRGB[0] = r;
		mRGB[1] = g;
		mRGB[2] = b;
		return *this;
	}
};

struct LoadSound {
	std::string mFilename;
	std::string mName;
	SoundType mType;
	LoadSound(const std::string& filename, const std::string& name, SoundType type)
		: mFilename(filename), mName(name), mType(type) {}
};

struct UnloadSound {
	std::string mName;
	UnloadSound(const std::string& name)
		: mName(name) {}
};

struct PlaySoundEvent {
	std::string mName;
	bool mLoop;
	PlaySoundEvent(const std::string& name, bool loop)
		: mName(name), mLoop(loop) {}
};

struct PauseAudio {
	bool mState;
	PauseAudio(bool state)
		: mState(state) {}
};

struct ScriptEventAck {
	bool mAccepted;
	ScriptEventAck(bool accepted)
		: mAccepted(accepted) {}
};

struct DialogueScriptEvent {
	std::string mDialogue;
	DialogueScriptEvent(const std::string& dialogue)
		: mDialogue(dialogue) {}
};

struct BattleScriptEvent {
	const std::vector<ActorType>& mEnemies;
	BattleScriptEvent(const std::vector<ActorType>& enemies)
		: mEnemies(enemies) {}
};

struct BlackboardScriptEvent {

};

struct AddRemovePartyMemberScriptEvent {
	bool mIsAdd;
	ActorType mActorType;
	AddRemovePartyMemberScriptEvent(bool isAdd, ActorType type)
		: mIsAdd(isAdd), mActorType(type) {
	}
};
