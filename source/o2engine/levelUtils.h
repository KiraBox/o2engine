// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "canvasUtils.h"
#include "levelUtils.h"
#include <queue>

class Actor;
namespace Utils { struct AsciiImage; }

namespace LevelUtils {

void InitLevelBorders();
void UpdateLevelBorders();
void UpdateImage();

void InitPlayerInput(void* _this, std::function<void(std::string)> onReturnCallback);
void UpdatePlayerInput(float dt);
void DestroyPlayerInput(void* _this);

CanvasUtils::PlayerTextInput& GetPlayerInputText();

void DisplayImage(const Utils::AsciiImage& image);

class TextQueue {
public:
	void PushText(const std::string& text);
	bool PopText(std::string& outStr);

	void Clear();
	void Reset();

	void Advance();
	void UpdateAndRender(CanvasUtils::Text& text, float dt, std::function<void()> postUpdate = nullptr);
	bool IsDone() const;

private:
	std::queue<std::string> mText;
	bool mAdvance = false;
	bool mIsDone = false;
};

}
