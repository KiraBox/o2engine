// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <memory>
#include <vector>

enum class DialogBrainType {
	Min = 0,
	NumTypes
};

class DialogBrain {
public:
	explicit DialogBrain(DialogBrainType brainType) : mBrainType(brainType) {}
	virtual ~DialogBrain() {}

	virtual std::vector<std::string> GetHurtText(bool isFriendlyFire) = 0;
	virtual std::vector<std::string> GetDefeatedText(bool isFriendlyFire) = 0;

	DialogBrainType GetBrainType() const;

private:
	DialogBrainType mBrainType;
};

class MinDialogBrain : public DialogBrain {
public:
	MinDialogBrain() : DialogBrain(DialogBrainType::Min) {}
	virtual ~MinDialogBrain() {}

	virtual std::vector<std::string> GetHurtText(bool isFriendlyFire) override;
	virtual std::vector<std::string> GetDefeatedText(bool isFriendlyFire) override;
};

class DialogBrainFactory {
public:
	void RegisterBrain(std::unique_ptr<DialogBrain> brain);
	DialogBrain* GetBrain(DialogBrainType brainType) const;

private:
	std::vector<std::unique_ptr<DialogBrain>> mRegisteredBrains;
};
