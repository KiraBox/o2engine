// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <sstream>
#include <string>

namespace FileSystem
{

struct FileObject {
	std::stringstream mStringStream;
};

// (r)ead, (w)rite, (a)ppend, (b)inary
bool ReadTextFile(const std::string& filepath, const std::string& mode, FileObject& fileObj);
bool WriteTextFile(const std::string& filepath, const std::string& mode, FileObject& fileObj);

}
