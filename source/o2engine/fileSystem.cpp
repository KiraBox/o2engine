// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "fileSystem.h"
#include <SDL.h>

namespace FileSystem
{

bool ReadTextFile(const std::string& filepath, const std::string& mode, FileObject& fileObj) {
	if(filepath.empty()) {
		return false;
	}

	if(SDL_RWops* rw = SDL_RWFromFile(filepath.c_str(), mode.c_str())) {
		Sint64 filesize = SDL_RWsize(rw);
		char* buffer = new char[static_cast<int>(filesize)];

		Sint64 totalRead = 0;
		Sint64 numRead = 1;
		char* bufferWalk = buffer;

		while(totalRead < filesize && numRead > 0) {
			numRead = SDL_RWread(rw, bufferWalk, sizeof(char), static_cast<int>(filesize - numRead));
			totalRead += numRead;
			bufferWalk += numRead;
		}

		if(totalRead < filesize) {
			delete [] buffer;
			return false;
		}

		fileObj.mStringStream.write(buffer, filesize);
		O2ALOG(fileObj.mStringStream, "Failed to read into string stream.");
		delete [] buffer;

		SDL_RWclose(rw);

		return true;
	}

	return false;
}

bool WriteTextFile(const std::string& filepath, const std::string& mode, FileObject& fileObj) {
	if(filepath.empty()) {
		return false;
	}

	if(SDL_RWops* rw = SDL_RWFromFile(filepath.c_str(), mode.c_str())) {
		if(fileObj.mStringStream) {
			fileObj.mStringStream.seekg(0, fileObj.mStringStream.end);
			int streamSize = static_cast<int>(fileObj.mStringStream.tellg());
			fileObj.mStringStream.seekg(0, fileObj.mStringStream.beg);

			char* buffer = new char[streamSize];
			fileObj.mStringStream.read(buffer, streamSize);

			Sint64 totalWritten = SDL_RWwrite(rw, buffer, sizeof(char), streamSize);

			delete [] buffer;
		}

		SDL_RWclose(rw);

		return true;
	}

	return false;
}

}
