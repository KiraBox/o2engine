// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <map>
#include "soundType.h"

struct SoundData;

class Audio {
public:
	Audio();

	void Init();
	void Update(float dt);
	void Destroy();

	void PlaySound(const std::string& name, bool loop);

private:
	void InitImpl();
	void UpdateImpl(float dt);
	void DestroyImpl();

private:
	std::map<std::string, SoundData*> mLoadedSounds;
	bool mPaused = false;

	unsigned int mGlobalVolume;
	float mVolume[static_cast<size_t>(SoundType::NumTypes)];
};
