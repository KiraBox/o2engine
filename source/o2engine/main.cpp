// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "engine.h"

#if defined(_WIN32)

int main(int argc, char** args) {
	(void)argc;
	(void)args;

	Engine engine;
	engine.Run();
	return 0;
}

#endif
