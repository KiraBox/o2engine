// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "blackboard.h"
#include <functional>
#include "levelScript.h"
#include "scriptEnumsAsciiRPG.h"

class StorylineManager {
public:
	StorylineManager(LevelScriptFactory& scriptFactory, Blackboard& blackboard);

	typedef std::function<bool(const Blackboard& blackboard)> ConditionDelegate;
	void AddScript(LevelScriptName levelScript, ConditionDelegate condition);

	void Tick(float dt);
	void Pause(bool state);

	bool IsActiveScript() const;

private:
	ConditionDelegate mConditionDelegates[LevelScriptName::Count];

	LevelScriptFactory& mScriptFactory;
	Blackboard& mBlackboard;

	std::unique_ptr<LevelScript> mActiveScript;
	bool mIsPaused = true;
};
