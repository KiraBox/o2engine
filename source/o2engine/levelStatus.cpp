// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "common.h"
#include "fsm.h"
#include "gameMode.h"
#include "gameModeAsciiRPG.h"
#include "level.h"
#include "levelUtils.h"
#include "sharedConstants.h"

namespace {
	static CanvasUtils::Text mDisplayedText;
}

void Reset() {
	mDisplayedText.SetText("");
}

void LevelStatus::InitImpl() {
	::Reset();

	LevelUtils::InitLevelBorders();
	LevelUtils::InitPlayerInput(this, [&](std::string str) {
		mReturnKeyPressed = true;
	});

	mDisplayedText.mPosX = 5;
	mDisplayedText.mPosY = 5;
	mDisplayedText.SetDefaultDelay(0.025f);
	mDisplayedText.SetMaxLength(DIALGOUE_MAX_CHARS_WIDTH);
	//mDisplayedText.CenterX(true);

	static_cast<GameModeAsciiRPG&>(mGameMode).GetStoryMan().Pause(false);
}

void LevelStatus::UpdateImpl(float dt) {
	LevelUtils::UpdateLevelBorders();
	LevelUtils::UpdatePlayerInput(dt);

	if(mReturnKeyPressed) {
		if(!mTextQueue.IsDone()) {
			mReturnKeyPressed = false;
			mTextQueue.Advance();
		}
	}
	else if(mDisplayedText.IsDone()) {
		LevelUtils::GetPlayerInputText().SetText(CONTINUE_STRING);
	}

	mTextQueue.UpdateAndRender(mDisplayedText, dt);
}

void LevelStatus::DestroyImpl() {
	LevelUtils::DestroyPlayerInput(this);
}
