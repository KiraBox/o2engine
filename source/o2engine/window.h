// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <string>

class Window {
public:
	void Init();
	void Update();
	void Destroy();

	void SetName(const std::string& name);
	const std::string& GetName() const;

	void SetWidth(unsigned int width);
	unsigned int GetWidth() const;

	void SetHeight(unsigned int height);
	unsigned int GetHeight() const;

	void SetDimensions(unsigned int width, unsigned int height);

	void* GetWindow() const;
	void* GetNativeWindow() const;

private:
	void InitImpl();
	void UpdateImpl();
	void DestroyImpl();

	void ResizeImpl();
	void RenameImpl();

	std::string mName;
	unsigned int mWidth;
	unsigned int mHeight;
};
