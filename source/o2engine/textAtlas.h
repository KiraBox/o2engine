// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <vector>

class TextAtlas {
public:
	struct Cell {
		float mTopLeftU;
		float mTopLeftV;
		float mBotRightU;
		float mBotRightV;
	};

	TextAtlas(const std::string& textureName, unsigned int atlasWidth, unsigned int atlasHeight, unsigned int width, unsigned int height);

	size_t getNumCells() const;
	const Cell& getCell(size_t idx) const;

	const std::string& getTextureName() const {
		return mTextureName;
	}

private:
	const std::string mTextureName;
	std::vector<Cell> mCells;

	unsigned int mAtlasWidth;
	unsigned int mAtlasHeight;
	unsigned int mWidth;
	unsigned int mHeight;
	unsigned int mNumWidth;
	unsigned int mNumHeight;
};

class IBMPCRenderer {
public:
	IBMPCRenderer(const TextAtlas& atlas)
		: mAtlas(atlas) {
	}

	void Draw(size_t code, float x, float y) const;
	void DrawASCII(unsigned char, float x, float y) const;

	void SetColor(float r, float g, float b);
	void SetDefaultColor();

private:
	const TextAtlas& mAtlas;
	const float mDefaultColor[3] = { 255.f, 255.f, 255.f };
	float mColor[3] = { 255.f, 255.f, 255.f };	// rgb
};
