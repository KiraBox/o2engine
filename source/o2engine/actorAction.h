// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class Actor;

enum class ActorActionType {
	None,
	Attack
};

struct ActorActionInfo {
	Actor* mActor;
	Actor* mTargetActor;

	ActorActionType mActionType;
	union {
		int mAmount;
	} mData;
};
