// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include <unordered_map>

class Blackboard {
public:
	void RegisterBooleanKey(unsigned int key, bool value);
	void SetBoolean(unsigned int key, bool value);
	bool GetBoolean(unsigned int key) const;

private:
	std::unordered_map<unsigned int, bool> mBooleanValues;
};
