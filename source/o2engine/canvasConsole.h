// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "canvas.h"
#include "canvasUtils.h"
#include <functional>
#include <vector>
#include <map>

enum class EKeyboard : unsigned int;

class CanvasConsole : public Canvas {
public:
	CanvasConsole() : Canvas() {}
	virtual ~CanvasConsole() {}

	virtual void Init() override;
	virtual void Update(float dt) override;
	virtual void Destroy() override;
};



class Console {
public:
	Console(const std::string& lineHeader, unsigned int height, unsigned width);

	void Init();
	void Update(float dt);
	void Destroy();

	void Render();
	void PushLine(const std::string& str);
	void Append(unsigned char c);
	void Append(const std::string& str);
	void Remove();
	void Clear();
	void ClearLine();

	const std::string& GetLineHeader() const {
		return mLineHeader;
	}

	std::string GetLine() const;

	void OnKeyPressed(EKeyboard key);
	void OnKeyReleased(EKeyboard key);

	bool IsShiftHeld() const {
		return mIsShiftHeld[0] || mIsShiftHeld[1];
	}

	void RegisterCommandNoParams(const std::string& name, std::function<bool()> command);
	void AutoComplete();

private:
	std::vector<CanvasUtils::Text> mRows;
	size_t mRowIdx;

	std::string mLineHeader;
	unsigned int mHeight;
	unsigned int mWidth;

	unsigned char mCursor;
	float mDelaySeconds;
	float mDelayAccum;
	bool mCursorToggle;

	bool mIsShiftHeld[2];
	bool mIsBackspaceHeld;

	float mBackspaceDelaySeconds[2];
	float mBackspaceDelayAccum[2];

	std::vector<std::string> mCommandNames;
	std::map<std::string, std::function<bool()>> mCommandsNoParams;
};
