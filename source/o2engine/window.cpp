// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.

#include "window.h"
#include "SDL.h"
#include "SDLUtils.h"
#include "SDL_syswm.h"

namespace {
	SDL_Window* mWindow = nullptr;
	SDL_Surface* mScreenSurface = nullptr;
}

void Window::InitImpl() {
	mWindow = SDL_CreateWindow(
		mName.c_str(),
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		mWidth, mHeight,
		SDL_WINDOW_OPENGL
	);

	if (mWindow == nullptr) {
		SDLUtils::Error();
		return;
	}

	mScreenSurface = SDL_GetWindowSurface(mWindow);

	if (mScreenSurface == nullptr) {
		SDLUtils::Error();
		return;
	}
}

void Window::UpdateImpl() {

}

void Window::DestroyImpl() {
	if (mWindow) {
		SDL_DestroyWindow(mWindow);
	}

	mWindow = nullptr;
	mScreenSurface = nullptr;
}

void Window::ResizeImpl() {
	if (mWindow) {
		SDL_SetWindowSize(mWindow, mWidth, mHeight);
	}
}

void Window::RenameImpl() {
	if (mWindow) {
		SDL_SetWindowTitle(mWindow, mName.c_str());
	}
}

void Window::Init() {
	InitImpl();
}

void Window::Update() {
	UpdateImpl();
}

void Window::Destroy() {
	DestroyImpl();
}

void Window::SetName(const std::string& name) {
	mName = name;
	RenameImpl();
}

const std::string& Window::GetName() const {
	return mName;
}

void Window::SetWidth(unsigned int width) {
	if (mWidth != width) {
		mWidth = width;
		ResizeImpl();
	}
}

unsigned int Window::GetWidth() const {
	return mWidth;
}

void Window::SetHeight(unsigned int height) {
	if (mHeight != height) {
		mHeight = height;
		ResizeImpl();
	}
}

unsigned int Window::GetHeight() const {
	return mHeight;
}

void Window::SetDimensions(unsigned int width, unsigned int height) {
	if (mWidth != width || mHeight != height) {
		mWidth = width;
		mHeight = height;
		ResizeImpl();
	}
}

void* Window::GetWindow() const {
	return mWindow;
}

void* Window::GetNativeWindow() const {
	if (mWindow) {
		SDL_SysWMinfo windowManInfo;
		SDL_VERSION(&windowManInfo.version);
		if (SDL_GetWindowWMInfo(mWindow, &windowManInfo)) {
			return windowManInfo.info.win.window;
		}
	}

	return nullptr;
}
