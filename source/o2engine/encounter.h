// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

#include "actorAction.h"
#include <functional>
#include <list>
#include <vector>

class Actor;
namespace Utils { class Random; }

enum class EncounterCompletionState {
	Unknown = 0,
	Loss,
	Victory
};

class Encounter {
public:
	void SetRandom(Utils::Random& random);

	void AddPartyMember(Actor* actor);
	void AddEnemy(Actor* actor);

	Actor* Start();
	Actor* Tick();
	Actor* TickPlayer(const ActorActionInfo& action);

	const std::vector<Actor*>& GetEnemies() const;
	const std::vector<Actor*>& GetPartyMembers() const;

	void GetTurnOrder(std::vector<Actor*>& outTurnOrder) const;

	EncounterCompletionState GetEncounterCompletionState() const;

	void RegisterOnEncounterCombatText(std::function<void(const std::string& str)> callback);
	void RegisterOnEncounterDialog(std::function<void(const std::string& str)> callback);

private:
	void UpdateActionQueue();
	void SortByInitiative();
	bool IsEnemy(const Actor* actor) const;
	bool AreAllies(const Actor* actorA, const Actor* actorB) const;
	Actor* GetActor() const;
	void DoActorAction(const ActorActionInfo& action);
	void Advance();
	void RemoveActor(const Actor& actor);

	// actor queue based on initiative
	struct ActorInitiativeInfo {
		Actor* mActor = nullptr;
		int mInitiative = std::numeric_limits<int>::max();
	};

	EncounterCompletionState mEncounterCompletionState = EncounterCompletionState::Unknown;

	Utils::Random* mRandom = nullptr;

	std::vector<Actor*> mPartyMembers;
	std::vector<Actor*> mEnemies;

	std::list<ActorInitiativeInfo> mActionQueue;
	bool mActionQueueDirty = false;

	std::function<void(const std::string& str)> mOnEncounterCombatText;
	std::function<void(const std::string& str)> mOnEncounterDialog;
};
