// Copyright (c) 2019, Ryan Tyler Rae. All rights reserved.
#pragma once

class Canvas {
public:
	virtual void Init() = 0;
	virtual void Update(float dt) = 0;
	virtual void Destroy() = 0;
};
