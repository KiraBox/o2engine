#version 110

uniform sampler2D texture;
uniform vec3 colorRGB;

varying vec2 texCoord;

vec3 when_eq(vec3 x, vec3 y) {
	return vec3(1.0 - abs(sign(x - y)));
}

void main()
{
	gl_FragData[0] = texture2D(texture, texCoord);

	// change the color of the white part of text
	vec3 invColorRGB = vec3(1.0) - colorRGB;
	gl_FragData[0].xyz -= when_eq(vec3(1.0), gl_FragData[0].xyz) * invColorRGB;
}
