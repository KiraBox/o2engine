#version 110

uniform sampler2D texture;
uniform vec3 colorRGB;

varying vec2 texCoord;

void main()
{
	texCoord = gl_MultiTexCoord0.xy;
	gl_Position = ftransform();
}
