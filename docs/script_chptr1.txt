[PROLOGUE]

Long ago, HUMANS and DEMIHUMANS ruled over the world with the power of MAGIC.

There were many types of MAGIC.

WHITE MAGIC. The vibrance of life, and bitterness of death.

BLACK MAGIC. The questionable arts, mysterious and mad. 

BLUE MAGIC. Control over time and submission.

GREEN MAGIC. The breath of nature itself.

And RED MAGIC. Deformation of SOUL and SPIRIT. Forbidden to all.

A powerful MAGE emerged by the name of ZARREF, and he performed many acts of kindness and compassion.

When he lost someone dear to him, he turned to RED MAGIC in an attempt to restore the SOUL.

But once lost, the SOUL cannot be restored.

Frustrated and twisted by his failures, ZARREF committed great crimes against the world.

A great WAR was fought, and at last he was defeated.

But with his final breath, he used RED MAGIC to CURSE himself, splintering his SOUL and scattering it to the winds.

He vowed to return, and once again use MAGIC to restore the SOUL of that whom he had lost.

Ages have passed, and much has been forgotten.

Rumors of evil once again spread across the land ...
